<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRemindersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reminders', function (Blueprint $table) {
            $table->id();
            $table->string('user_id');
            $table->string('account_id');
            $table->string('type')->nullable();
            $table->string('amount');
            $table->string('description')->nullable();
            $table->string('subcategory_id')->nullable();
            $table->string('reminder_date');
            $table->string('reminder_until')->nullable();
            $table->string('reminder_type')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reminders');
    }
}
