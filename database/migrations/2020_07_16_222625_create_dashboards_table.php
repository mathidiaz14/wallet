<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDashboardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dashboards', function (Blueprint $table) {
            $table->id();
            $table->string('user_id');
            $table->string('one')->nullable();
            $table->string('two')->nullable();
            $table->string('tree')->nullable();
            $table->string('four')->nullable();
            $table->string('five')->nullable();
            $table->string('six')->nullable();
            $table->string('seven')->nullable();
            $table->string('eight')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dashboards');
    }
}
