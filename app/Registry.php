<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Registry extends Model
{
    protected $dates = ['date'];
    
    public function account()
    {
        return $this->belongsTo('App\Account');
    }

    public function subcategory()
    {
        return $this->belongsTo('App\Subcategory');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
