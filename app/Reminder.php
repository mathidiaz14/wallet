<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reminder extends Model
{
	protected $dates = ['reminder_date', 'reminder_until'];

    public function account()
    {
        return $this->belongsTo('App\Account');
    }

    public function subcategory()
    {
        return $this->belongsTo('App\Subcategory');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
