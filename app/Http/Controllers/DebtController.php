<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Debt;
use App\Registry;
use App\History;
use App\Account;
use Carbon\Carbon;
use Auth;

class DebtController extends Controller
{

    private $path = "debt.";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view($this->path."index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $debt                   = new Debt();
        $debt->user_id          = Auth::user()->id;
        $debt->who              = $request->who;
        $debt->amount           = $request->amount;
        $debt->amount_returned  = "0";
        $debt->account_id       = $request->account_id;
        $debt->save();

        if($request->registry_check == "on")
        {
            $account = Account::find($request->account_id);

             //Genero el registro del prestamo
            $registry                   = new Registry();
            $registry->account_id       = $request->account_id;
            $registry->amount           = $request->amount;
            $registry->user_id          = Auth::user()->id;
            $registry->subcategory_id   = "debt";
            $registry->way              = "transfer";
            $registry->description      = "Prestamo de ".$request->who;
            $registry->date             = Carbon::now();
            $registry->hour             = Carbon::now()->format('H:i');
            $registry->type             = 'more';
            $registry->save();

            //Resto monto a la cuenta
            $account->amount += $request->amount;
            $account->save();

            //Reviso si hay historial y sino lo genero
            $date       = Carbon::now()->format('d-m-y');
            $history    = $account->histories->where('date', $date)->first();
            
            if($history == null)
            {
                $now                = new History();
                $now->account_id    = $account->id;
                $now->amount        = $account->amount;
                $now->date          = $date;
                $now->save();

            }else
            {
                $history->amount = $account->amount;
                $history->save();
            }
        }

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $debt                   = Debt::find($id);
        $debt->who              = $request->who;
        $debt->amount           = $request->amount;
        $debt->amount_returned  = $request->amount_returned;
        $debt->save();

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $debt = Debt::find($id);
        $debt->delete();

        return back();
    }

    public function add(Request $request)
    {
        $debt = Debt::find($request->debt);

        if($request->type == "less")
            $debt->amount += $request->number;
        else
            $debt->amount_returned += $request->number;

        if($debt->amount <= $debt->amount_returned)
            $debt->status = "disabled";

        $debt->save();

        if($request->registry_check == "on")
        {
            $account = Account::find($request->account_id);

            //Genero el registro del prestamo
            $registry                   = new Registry();
            $registry->account_id       = $request->account_id;
            $registry->amount           = $request->number;
            $registry->user_id          = Auth::user()->id;
            $registry->subcategory_id   = "debt";
            $registry->way              = "transfer";
            $registry->description      = "Devolución a ".$debt->who;
            $registry->date             = Carbon::now();
            $registry->hour             = Carbon::now()->format('H:i');
            $registry->type             = 'less';
            $registry->save();

            //Resto monto a la cuenta
            $account->amount -= $request->number;
            $account->save();

            //Reviso si hay historial y sino lo genero
            $date       = Carbon::now()->format('d-m-y');
            $history    = $account->histories->where('date', $date)->first();
            
            if($history == null)
            {
                $now                = new History();
                $now->account_id    = $account->id;
                $now->amount        = $account->amount;
                $now->date          = $date;
                $now->save();

            }else
            {
                $history->amount = $account->amount;
                $history->save();
            }
        }
        
        return back();
    }
}
