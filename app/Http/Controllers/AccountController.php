<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Account;
use App\Shared;
use App\Registry;
use App\History;
use Carbon\Carbon;
use Auth;

class AccountController extends Controller
{
    private $path = "account.";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view($this->path.'index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->path.'create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $account            = new Account();

        $account->title     = $request->title;
        $account->amount    = $request->amount;
        $account->color     = $request->color;
        $account->exclude   = $request->exclude;
        $account->code      = Code(20);
        $account->save();

        $account->users()->attach(Auth::user()->id);
        
        $account->users()->updateExistingPivot(
                                Auth::user()->id, 
                                array(
                                    'role' => 'admin', 
                                    'status' => 'enabled'
                                )
                            );

        $history = new History();
        $history->account_id    = $account->id;
        $history->amount        = $account->amount;
        $history->date          = Carbon::now()->format('d-m-y');
        $history->save();

        return redirect('account');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $account    = Account::find($id);
        $registries = Registry::where('account_id', $account->id)
                                ->orderBy('date', 'desc')
                                ->orderBy('hour', 'desc')
                                ->paginate(30);

        return view($this->path."show", compact('account', 'registries'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $account = Account::find($id);

        return view($this->path."edit", compact('account'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $account            = Account::find($id);

        $account->title     = $request->title;
        $account->amount    = $request->amount;
        $account->color     = $request->color;
        $account->exclude   = $request->exclude;
        $account->save();

        return redirect('account');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $account = Account::find($id);

        $account->users()->detach();

        $account->delete();

        return back();
    }

    public function exit(Request $request, $id)
    {
        Auth::user()->accounts()->detach($id);

        return back();
    }

    public function share(Request $request)
    {
        $code = code(10);
        $shared = new Shared();

        $shared->email      = $request->email;
        $shared->account    = $request->account;
        $shared->code       = $code;
        $shared->save();

        $body = [
            "code"  => $code,
            "user"  => Auth::user()->name
        ];

        $subject = "Invitación a Wallet by MathiasDíaz";

        email('emails.share', $subject, $body, $request->email);

        Session(["success" => "La invitación fue enviada correctamente."]);

        return back();
    }

    public function delete_user(Request $request)
    {
        $user = User::find($request->user);
        $account = Account::find($request->account);

        $account->users()->detach($user);

        Session(["success" => "Se elimino el usuario ".$user->name." de la cuenta ".$account->title]);
        return back();
    }

    public function delete_shared(Request $request)
    {
        $shared = Shared::find($request->id);
        $shared->delete();

        Session(["success" => "Se elimino la invitación del usuario ".$shared->email]);
        return back();
    }

    public function get_invitation($id)
    {
        $shared = Shared::where('code', $id)->first();
        $now    = Carbon::now();

        if($shared == null)
            return view('invitation.null');
        elseif ($now->diffInHours($shared->created_at) >= 48)
            return view('invitation.expired');
        

        $user = User::where('email', $shared->email)->first();

        if( $user != null )
        {
            $account = Account::find($shared->account);

            $account->users()->attach($user->id);
        
            $account->users()->updateExistingPivot(
                                    $user->id, 
                                    array(
                                        'role' => 'share', 
                                        'status' => 'enabled'
                                    )
                                );  

            $shared->delete(); 

            return redirect('home');
        }else
        {
            return view('invitation.index', compact('shared'));
        }
    }

    public function transfer(Request $request)
    {
        $from = Account::find($request->from);
        $to = Account::find($request->to);

        //Genero el registro del descuento
        $registry                   = new Registry();
        $registry->account_id       = $from->id;
        $registry->amount           = $request->amount;
        $registry->user_id          = Auth::user()->id;
        $registry->subcategory_id   = "transfer";
        $registry->way              = "transfer";
        $registry->description      = "Transferencia hacia ".$from->title;
        $registry->date             = Carbon::now();
        $registry->hour             = Carbon::now()->format('H:i');
        $registry->type             = 'less';
        $registry->save();

        //Resto monto a la cuenta
        $from->amount -= $request->amount;
        $from->save();

        //Reviso si hay historial y sino lo genero
        $date       = Carbon::now()->format('d-m-y');
        $history    = $from->histories->where('date', $date)->first();
        
        if($history == null)
        {
            $now                = new History();
            $now->account_id    = $from->id;
            $now->amount        = $from->amount;
            $now->date          = $date;
            $now->save();

        }else
        {
            $history->amount = $from->amount;
            $history->save();
        }

        //Genero el registro del aumento
        $registry                   = new Registry();
        $registry->account_id       = $to->id;
        $registry->amount           = $request->amount;
        $registry->user_id          = Auth::user()->id;
        $registry->subcategory_id   = "transfer";
        $registry->way              = "transfer";
        $registry->description      = "Transferencia desde ".$to->title;
        $registry->date             = Carbon::now();
        $registry->hour             = Carbon::now()->format('H:i');
        $registry->type             = 'more';
        $registry->save();

        //Resto monto a la cuenta
        $to->amount += $request->amount;
        $to->save();

        //Reviso si hay historial y sino lo genero
        $date       = Carbon::now()->format('d-m-y');
        $history    = $to->histories->where('date', $date)->first();
        
        if($history == null)
        {
            $now                = new History();
            $now->account_id    = $to->id;
            $now->amount        = $to->amount;
            $now->date          = $date;
            $now->save();

        }else
        {
            $history->amount = $to->amount;
            $history->save();
        }

        Session(["success" => "Se realizo la transferencia desde la cuenta ".$from->title." a ".$to->title]);

        return back();
    }

    public function web($code)
    {
        $account = Account::where('code', $code)->first();

        if($account != null) 
            return view($this->path."web", compact('account'));

        abort('404');
    }
}
