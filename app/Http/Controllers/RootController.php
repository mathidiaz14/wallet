<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Hash;

class RootController extends Controller
{
	private $path = "root.";

    public function index()
    {
    	$users = User::paginate();

    	return view($this->path."index", compact('users'));
    }

    public function user_destroy($id)
    {
        $user = User::find($id);

        $user->accounts()->detach();
        $user->delete();

        Session(['success' => 'El usuario se elimino correctamente']);
        return back();
    }

    public function user_password(Request $request, $id)
    {
    	$user = User::find($id);

    	$user->password = Hash::make($request->password);
    	$user->save();

        $body = [
            "password"  => $request->password,
        ];

        $subject = "Nueva contraseña para Wallet by MathiasDíaz";

        email('emails.change_password', $subject, $body, $user->email);

        Session(['success' => 'La contraseña se modifico correctamente']);
    	return back();
    }
}
