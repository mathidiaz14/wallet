<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Objective;
use Auth;

class ObjectiveController extends Controller
{
    private $path = "objective.";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view($this->path."index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $objective = new Objective();

        $objective->user_id         = Auth::user()->id;
        $objective->title           = $request->title;
        $objective->color           = $request->color;
        $objective->amount          = $request->amount;
        $objective->objective       = $request->objective;
        $objective->date            = $request->date;
        $objective->save();

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $objective = Objective::find($id);

        return view($this->path."show", compact('objective'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $objective = Objective::find($id);

        $objective->title           = $request->title;
        $objective->color           = $request->color;
        $objective->amount          = $request->amount;
        $objective->objective       = $request->objective;
        $objective->date            = $request->date;
        $objective->save();

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $objective = Objective::find($id);
        $objective->delete();

        return back();
    }

    public function add(Request $request)
    {
        $objective = Objective::find($request->objective);

        if($request->type == "less")
            $objective->amount -= $request->number;
        else
            $objective->amount += $request->number;

        $objective->save();

        return back();
    }
}
