<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Hash;
use Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the users
     *
     * @param  \App\Models\User  $model
     * @return \Illuminate\View\View
     */
    public function index(User $model)
    {
        return view('users.index', ['users' => $model->paginate(15)]);
    }

    public function destroy($id)
    {
        $user = User::find($id);
        
        $user->accounts()->detach();
        $user->accounts()->delete();
        $user->registries()->delete();
        $user->categories()->delete();
        $user->reminders()->delete();
        $user->objectives()->delete();
        $user->debts()->delete();
        
        $user->delete();

        Auth::logout();

        return back();
    }
}
