<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Reminder;
use App\Registry;
use App\Notification;
use App\History;
use Carbon\Carbon;
use Auth;

class ReminderController extends Controller
{
    private $path = "reminder.";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view($this->path."index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $reminder                   = new Reminder();
        $reminder->account_id       = $request->account;
        $reminder->amount           = $request->amount;
        $reminder->subcategory_id   = $request->subcategory;
        $reminder->user_id          = Auth::user()->id;
        $reminder->description      = $request->description;
        $reminder->reminder_date    = $request->reminder_date;
        $reminder->reminder_type    = $request->reminder_type;
        $reminder->reminder_until   = $request->reminder_until;
        $reminder->save();

        if($reminder->subcategory != null)
        {
            $reminder->type = $reminder->subcategory->category->type;
            $reminder->category_id = $reminder->subcategory->category->id;
        }
        else
        {
            $reminder->type = 'less';
        }
        
        $reminder->save();

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $reminder                   = Reminder::find($id);
        $reminder->account_id       = $request->account;
        $reminder->amount           = $request->amount;
        $reminder->subcategory_id   = $request->subcategory;
        $reminder->description      = $request->description;
        $reminder->reminder_date    = $request->reminder_date;
        $reminder->reminder_type    = $request->reminder_type;
        $reminder->reminder_until   = $request->reminder_until;
        $reminder->save();

        if($reminder->subcategory != null)
        {
            $reminder->type = $reminder->subcategory->category->type;
            $reminder->category_id = $reminder->subcategory->category->id;
        }
        else
        {
            $reminder->type = 'less';
        }
        
        $reminder->save();

        return back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $reminder = Reminder::find($id);
        $reminder->delete();

        return back();
    }

    public function check(Request $request, $id)
    {
        $reminder                   = Reminder::find($id);
        $reminder->delete();

        $registry                   = new Registry();
        $registry->account_id       = $request->account;
        $registry->amount           = $request->amount;
        $registry->subcategory_id   = $request->subcategory_id;
        $registry->user_id          = Auth::user()->id;
        $registry->description      = $request->description;
        $registry->way              = $request->way;
        $registry->date             = $request->date;
        $registry->hour             = $request->hour;
        $registry->save();

        if($registry->subcategory != null)
        {
            $registry->type = $registry->subcategory->category->type;
            $registry->category_id = $registry->subcategory->category->id;
        }
        else
        {
            $registry->type = 'less';
        }
        
        $registry->save();

        $account = $registry->account;

        if($registry->type == "less")
            $account->amount -= $registry->amount;
        else
            $account->amount += $registry->amount;

        $account->save();

        $date       = Carbon::now()->format('d-m-y');
        $history    = $account->histories->where('date', $date)->first();
        
        if($history == null)
        {
            $now                = new History();
            $now->account_id    = $account->id;
            $now->amount        = $account->amount;
            $now->date          = $date;
            $now->save();

        }else
        {
            $history->amount = $account->amount;
            $history->save();
        }

        return back();
    }
}
