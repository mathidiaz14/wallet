<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Twilio\Rest\Client;
use Carbon\Carbon;
use Storage;
use File;
use Auth;
use Mail;
use Hash;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $start          = Carbon::now()->firstOfMonth();
        $end            = Carbon::now();
        $account_select = null;

        return view('home', compact('start', 'end', 'account_select'));
    }

    public function test_sms()
    {
        
        // Your Account SID and Auth Token from twilio.com/console
        $sid = env('TWILIO_SID');
        $token = env('TWILIO_TOKEN');
        $client = new Client( $sid, $token );


        // Use the client to do fun stuff like send text messages!
        $client->messages->create(
            // the number you'd like to send the message to
            '+59895570044',
            [
                // A Twilio phone number you purchased at twilio.com/console
                'from' => env('TWILIO_FROM'),
                // the body of the text message you'd like to send
                'body' => 'Prueba desde Finanzas'
            ]
        );

        return "Se envio el mensaje";
    }

    public function test_email()
    {
        $content = "Prueba de envio";

        $para      = 'diaz.mathias.14@gmail.com';
        $titulo    = 'Prueba de envio desde MathiasDíaz.uy';
        
        $cabeceras  = 'MIME-Version: 1.0' . "\r\n" .
            'Content-type: text/html; charset=iso-8859-1' . "\r\n" .
            'From: wallet@mathiasdiaz.uy' . "\r\n" .
            'Reply-To: wallet@mathiasdiaz.uy' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();
        
        $mensaje   = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional //EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>

            <html xmlns='http://www.w3.org/1999/xhtml' xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:v='urn:schemas-microsoft-com:vml'>
            <head>
                <!--[if gte mso 9]><xml><o:OfficeDocumentSettings><o:AllowPNG/><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings></xml><![endif]-->
                <meta content='text/html; charset=utf-8' http-equiv='Content-Type'/>
                <meta content='width=device-width' name='viewport'/>
                <!--[if !mso]><!-->
                <meta content='IE=edge' http-equiv='X-UA-Compatible'/>
                <!--<![endif]-->
                <title></title>
                <!--[if !mso]><!-->
                <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'/>
                <style>
                    @font-face {
                        font-family: Poppins;
                        src: url('http://glik.uy/fonts/Poppins-Bold.ttf');
                    }

                    *{
                        margin:0;
                        font-family: Poppins;
                    }

                    .btn
                    {
                        text-decoration: none;
                        background: #235693;
                        padding: 10px;
                        border-radius: 5px;
                        color:white;
                    }
                </style>
            </head>
            <body style='background: #009ed3'>
                <table class='full-width-container' border='0' cellpadding='0' cellspacing='0' height='100%' width='100%' style='width: 100%; height: 100%; padding: 30px 0 30px 0;'>
                        <tr>
                            <td align='center' valign='top'>
                                <!-- / 700px container -->
                                <table class='container' border='0' cellpadding='0' cellspacing='0' width='700' bgcolor='#ffffff' style='width: 700px;'>
                                    <tr>
                                        <td align='center' valign='top'>
                                            <!-- / Header -->
                                            <table class='container header' border='0' cellpadding='0' cellspacing='0' width='620' style='width: 620px;'>
                                                <tr>
                                                    <td style='padding: 30px 0 30px 0; border-bottom: solid 1px #eeeeee;' align='left'>
                                                        <a href='#' style='font-size: 30px; text-decoration: none; color: #fff;'>
                                                            <img src='http://wallet.mathiasdiaz.uy/icon.png' width='50px' alt=''>
                                                        </a>
                                                    </td>
                                                </tr>
                                            </table>
                                            <!-- /// Header -->

                                            <!-- / Hero subheader -->
                                            <table class='container hero-subheader' border='0' cellpadding='0' cellspacing='0' width='620' style='width: 620px;'>
                                                <tr>
                                                    <td class='hero-subheader__title' style='font-size: 43px; font-weight: bold; padding: 30px 0 15px 0;' align='center'>Prueba del administrador de Glik</td>
                                                </tr>

                                                <tr>
                                                    <td class='hero-subheader__content' style=' line-height: 27px; color: #969696; ' align='center'>
                                                        <p>Prueba de envio</p>
                                                    </td>
                                                </tr>
                                            </table>
                                            <!-- /// Hero subheader -->

                                            <!-- / Divider -->
                                            <table class='container' border='0' cellpadding='0' cellspacing='0' width='100%' align='center'>
                                                <tr>
                                                    <td align='center'>
                                                        <table class='container' border='0' cellpadding='0' cellspacing='0' width='620' align='center' style='border-bottom: solid 1px #eeeeee; width: 620px;'>
                                                            <tr>
                                                                <td align='center'>&nbsp;</td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <!-- /// Divider -->

                                            <!-- / Footer -->
                                            <table class='container' border='0' cellpadding='0' cellspacing='0' width='100%' align='center'>
                                                <tr>
                                                    <td align='center'>
                                                        <table class='container' border='0' cellpadding='0' cellspacing='0' width='620' align='center' style='border-top: 1px solid #eeeeee; width: 620px;'>
                                                            <tr>
                                                                <td style='text-align: center; padding: 50px 0 10px 0;'>
                                                                    <p style='font-size: 25px; text-decoration: none; color: #d5d5d5;'><b>Wallet</b> <small>by MathiasDíaz.uy</small></p>
                                                                </td>
                                                            </tr>

                                                            <tr>
                                                                <td> <p style='color: #d5d5d5; text-align: center; font-size: 15px; padding: 10px 0 60px 0; line-height: 22px;'>Este mensaje se genero automaticamente, <br />por favor no responda</td></p>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <!-- /// Footer -->
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
            </body>
            </html>";

        mail($para, $titulo, $mensaje, $cabeceras);

        return "se envio el email correctamente";
    }

    public function my_get()
    {
        $user = Auth::user();
        return view('my', compact('user'));
    }

    public function my_set(Request $request)
    {
        $file                   = $request->file('avatar');
        
        $user                   = Auth::user();
        $user->name             = $request->name;
        $user->email            = $request->email;

        if($file != null)
        {
            $nameFile   = "avatar_".$user->id;
            Storage::disk('public')->put($nameFile.".".$file->getClientOriginalExtension(), File::get($file));
            $user->avatar       = "storage/".$nameFile.".".$file->getClientOriginalExtension();
        }

        if($request->password != null) 
        {
            if($request->password == $request->password_confirmation)
            {
                $user->password = Hash::make($request->password);
                Session(['success' => "Se modificaron los datos correctamente"]);
            }
            else
            {
                Session(['error' => "El usuario y la contraseña no coinciden"]);
            }
        }

        $user->save();

        return back();
    }

    public function dashboard(Request $request)
    {
        $dashboard = Auth::user()->dashboard;

        $dashboard[$request->number] = $request->card;
        $dashboard->save();

        return back();
    }

    public function dashboard_delete(Request $request)
    {
        $dashboard = Auth::user()->dashboard;

        $dashboard[$request->number] = null;
        $dashboard->save();

        return back();    
    }

    public function filter(Request $request)
    {
        $start          = Carbon::create($request->start.' 00:00:00');
        $end            = Carbon::create($request->end.' 23:59:59');
        $account_select = $request->account_select;
        
        return view('home', compact('start', 'end', 'account_select'));
    }

    public function dashboard_account($id)
    {
        $start          = Carbon::now()->firstOfMonth();
        $end            = Carbon::now();
        $account_select = $id;

        return view('home', compact('start', 'end', 'account_select'));
    }

    public function delete_avatar()
    {
        $user = Auth::user();

        if($user->avatar != null)
        {
            if(file_exists($user->avatar))
                unlink($user->avatar);

            $user->avatar = null;
            $user->save();

            Session(["success" => "El avarar se elimino correctamente."]);
        }

        return back();
    }
}
