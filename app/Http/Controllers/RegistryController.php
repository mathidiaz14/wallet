<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \Illuminate\Support\Collection;
use Illuminate\Pagination\Paginator;
use Illuminate\Container\Container;
use Illuminate\Pagination\LengthAwarePaginator;
use Carbon\Carbon;
use App\Registry;
use App\History;
use Auth;
use Arr;


class RegistryController extends Controller
{
    private $path = "registry.";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $registries = collect(); 

        foreach(Auth::user()->accounts as $account)
        {    
            foreach($account->registries as $registry) 
                $registries->add($registry);
        }

        $registries = $registries->sortByDesc('date');
        $registries = $this->paginate($registries, 50);

        return view($this->path."index", compact('registries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $registry                   = new Registry();
        $registry->account_id       = $request->account;
        $registry->amount           = $request->amount;
        $registry->subcategory_id   = $request->subcategory;
        $registry->user_id          = Auth::user()->id;
        $registry->description      = $request->description;
        $registry->way              = $request->way;
        $registry->date             = $request->date;
        $registry->hour             = $request->hour;
        $registry->save();

        if($registry->subcategory_id != null)
        {
            $registry->type = $registry->subcategory->category->type;
            $registry->category_id = $registry->subcategory->category->id;
        }
        else
        {
            $registry->type = 'less';
        }
        
        $registry->save();

        $account = $registry->account;

        if($registry->type == "less")
            $account->amount -= $registry->amount;
        else
            $account->amount += $registry->amount;

        $account->save();

        $date       = $registry->date->format('d-m-y');
        $history    = $account->histories->where('date', $date)->first();
        
        if($history == null)
        {
            $now                = new History();
            $now->account_id    = $account->id;
            $now->amount        = $account->amount;
            $now->date          = $date;
            $now->save();

        }else
        {
            $history->amount = $account->amount;
            $history->save();
        }

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $registry = Registry::find($id);

        return view($this->path."show", compact('registry'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->destroy($id);

        $registry                   = new Registry();
        $registry->account_id       = $request->account;
        $registry->amount           = $request->amount;
        $registry->subcategory_id   = $request->subcategory;
        $registry->user_id          = Auth::user()->id;
        $registry->description      = $request->description;
        $registry->way              = $request->way;
        $registry->date             = $request->date;
        $registry->hour             = $request->hour;
        $registry->save();

        if($registry->subcategory_id != null)
        {
            $registry->type = $registry->subcategory->category->type;
            $registry->category_id = $registry->subcategory->category->id;
        }
        else
        {
            $registry->type = 'less';
        }
        
        $registry->save();

        $account = $registry->account;

        if($registry->type == "less")
            $account->amount -= $registry->amount;
        else
            $account->amount += $registry->amount;

        $account->save();

        $date       = $registry->date->format('d-m-y');
        $history    = $account->histories->where('date', $date)->first();
        
        if($history == null)
        {
            $now                = new History();
            $now->account_id    = $account->id;
            $now->amount        = $account->amount;
            $now->date          = $date;
            $now->save();

        }else
        {
            $history->amount = $account->amount;
            $history->save();
        }

        $histories  = $registry->account->histories
                        ->where('date', '>', $registry->date->format('d-m-y'));

        foreach ($histories as $history) 
        {
            if($registry->type == "less")
                $history->amount -= $registry->amount;
            else
                $history->amount += $registry->amount;
            
            $history->save();
        }

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $registry   = Registry::find($id);
        $account    = $registry->account;
        $history    = $registry->account->histories
                                ->where('date', $registry->date->format('d-m-y'))
                                ->first();

        $histories  = $registry->account->histories
                                ->where('date', '>', $registry->date->format('d-m-y'));
        
        if($registry->type == "less")
        {
            if($history != null)
            {
                $history->amount += $registry->amount;
                $history->save();
            }

            $account->amount += $registry->amount;
        }else
        {
            if($history != null)
            {
                $history->amount -= $registry->amount;
                $history->save();
            }
            
            $account->amount -= $registry->amount;
        }

        foreach ($histories as $history) {
            if($registry->type == "less")
                $history->amount += $registry->amount;
            else
                $history->amount -= $registry->amount;
            
            $history->save();
        }

        $account->save();
        $registry->delete();

        return back();
    }

    public function search(Request $request)
    {
        $reg = Registry::where('description', 'like', '%'.$request->search.'%')
                        ->orwhere('amount', $request->search)
                        ->orderBy('date', 'desc')
                        ->orderBy('hour', 'asc')
                        ->get();

        $registries = collect();

        foreach ($reg as $registry)
        {
            $flag = false;

            foreach(Auth::user()->accounts as $account)
            {
                if($registry->account_id == $account->id)
                    $flag = true;
            }

            if($flag)
                $registries->add($registry);
        }

        return view($this->path."search", compact('registries'));
    }

    public static function paginate(Collection $results, $pageSize = 20)
    {
        $page = Paginator::resolveCurrentPage('page');
        
        $total = $results->count();

        return self::paginator($results->forPage($page, $pageSize), $total, $pageSize, $page, [
            'path' => Paginator::resolveCurrentPath(),
            'pageName' => 'page',
        ]);

    }

    protected static function paginator($items, $total, $perPage, $currentPage, $options)
    {
        return Container::getInstance()->makeWith(LengthAwarePaginator::class, compact(
            'items', 'total', 'perPage', 'currentPage', 'options'
        ));
    }
}
