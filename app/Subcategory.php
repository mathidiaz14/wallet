<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subcategory extends Model
{
	protected $fillable = ['category_id','title','description'];
	
    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function registries()
    {
        return $this->hasMany('App\Registry');
    }

    public function reminders()
    {
        return $this->hasMany('App\Reminder');
    }
}
