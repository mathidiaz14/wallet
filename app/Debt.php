<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Debt extends Model
{
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function account()
    {
        return $this->belongsTo('App\Account');
    }
}
