<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

	protected $fillable = ['user_id','title','description','color','type'];
	
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function subcategories()
    {
        return $this->hasMany('App\Subcategory');
    }

    public function registries()
    {
        return $this->hasMany('App\Registry');
    }
}
