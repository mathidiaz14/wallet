<?php

function email($template, $subject, $body, $to)
{
    Mail::send($template, $body, function($message) use ($to, $subject)
    {
        $message->to($to)->subject($subject);
        $message->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'));
    });

    return true;
}

function code($length = 10) 
{ 
    return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length); 
} 

function Setting($key)
{
	$setting = Auth::user()->setting;

	return $setting[$key];
}

function dashboard_check($id)
{
    $dashboard = Auth::user()->dashboard;
    $flag = true;

    if($dashboard->one == $id)
        $flag = false;
    elseif($dashboard->two == $id)
        $flag = false;
    elseif($dashboard->tree == $id)
        $flag = false;
    elseif($dashboard->four == $id)
        $flag = false;
    elseif($dashboard->five == $id)
        $flag = false;
    elseif($dashboard->six == $id)
        $flag = false;
    elseif($dashboard->seven == $id)
        $flag = false;
    elseif($dashboard->eight == $id)
        $flag = false;

    return $flag;
}