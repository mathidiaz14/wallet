<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $dates = ['created_at'];
    
    public function users()
    {
        return $this->belongsToMany('App\Models\User')->withPivot('role', 'status');
    }

    public function registries()
    {
        return $this->hasMany('App\Registry')->orderBy('date', 'DESC')->orderBy('hour', 'DESC');
    }

    public function histories()
    {
    	return $this->hasMany('App\History');
    }

    public function reminders()
    {
        return $this->hasMany('App\Reminder');
    }

    public function debts()
    {
        return $this->hasMany('App\Debt');
    }
}
