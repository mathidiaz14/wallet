<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Objective extends Model
{
	protected $dates = ['date'];
	
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
