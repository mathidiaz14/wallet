<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Account;
use App\History;
use Carbon\Carbon;

class AccountHistory extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'account:history';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $date       = Carbon::now()->format('d-m-y');
        $accounts   = Account::all();

        foreach($accounts as $account)
        {
            $history    = $account->histories->where('date', $date)->first();
            
            if($history == null)
            {
                $now                = new History();
                $now->account_id    = $account->id;
                $now->amount        = $account->amount;
                $now->date          = $date;
                $now->save();

            }else
            {
                $history->amount = $account->amount;
                $history->save();
            }
        }

        $this->info("Se completo el historial de cuentas");
    }
}
