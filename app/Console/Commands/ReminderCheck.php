<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Reminder;
use App\Notification;

class ReminderCheck extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reminder:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Comando que chequea los recordatorios diariamente';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $reminders  = Reminder::all();
        $count      = 0;
        
        $bar = $this->output->createProgressBar(count($reminders));

        foreach ($reminders as $reminder) 
        {
            $bar->advance();

            if(($reminder->reminder_date->isToday()) && ($reminder->status != 'disabled'))
            {
                $reminder->status = "disabled";
                $reminder->save();

                $notification               = new Notification();
                $notification->title        = "Recordatorio";
                $notification->description  = "Hay un recordatorio de ".$reminder->subcategory->title." vencido";
                $notification->type         = "reminder";
                $notification->status       = "enabled";
                $notification->link         = url('reminder');
                $notification->user_id      = $reminder->user_id;
                $notification->save();

                $body = [];

                $subject = "Recordatorio de Wallet by MathiasDíaz";

                email('emails.reminder', $subject, $body, $reminder->user->email);

                $count ++;

                if($reminder->reminder_type != null)
                {
                    if(($reminder->reminder_until == null) or ($reminder->reminder_until->isFuture()))
                    {   
                        $replicate = $reminder->replicate();

                        $date = \Carbon\Carbon::today();

                        switch ($replicate->reminder_type) {
                            case "day":
                                $date = \Carbon\Carbon::tomorrow();
                                break;
                            case "week":
                                $date = \Carbon\Carbon::today()->addWeek();;
                                break;
                            case "month":
                                $date = \Carbon\Carbon::today()->addMonth();
                                break;
                            case "year":
                                $date = \Carbon\Carbon::today()->addYear();
                                break;
                            case "first_monday":
                                $date = \Carbon\Carbon::create('first monday of next month');
                                break;
                            case "first_thuesday":
                                $date = \Carbon\Carbon::create('first tuesday of next month');
                                break;
                            case "first_wednesday":
                                $date = \Carbon\Carbon::create('first wednesday of next month');
                                break;
                            case "first_thursday":
                                $date = \Carbon\Carbon::create('first thursday of next month');
                                break;
                            case "first_friday":
                                $date = \Carbon\Carbon::create('first friday of next month');
                                break;
                            case "first_saturday":
                                $date = \Carbon\Carbon::create('first saturday of next month');
                                break;
                            case "first_sunday":
                                $date = \Carbon\Carbon::create('first sunday of next month');
                                break;
                            default:
                                # code...
                                break;
                        }
                        
                        $replicate->reminder_date = $date;
                        $replicate->save();
                    }   
                }
            }
        }

        $bar->finish();

        $this->newLine();
        $this->info("-------------------------------");
        $this->info("El proceso se completo correctamente, se envian ".$count." recordatorios");
        $this->info("-------------------------------");
    }
}
