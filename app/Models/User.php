<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name', 'email', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $dates = ['deleted_at', 'last_login'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function accounts()
    {
        return $this->belongsToMany('App\Account')->withPivot('role', 'status');
    }

    public function categories()
    {
        return $this->hasMany('App\Category');
    }

    public function registries()
    {
        return $this->hasMany('App\Registry');
    }

    public function debts()
    {
        return $this->hasMany('App\Debt');
    }

    public function objectives()
    {
        return $this->hasMany('App\Objective');
    }

    public function reminders()
    {
        return $this->hasMany('App\Reminder');
    }

    public function notifications()
    {
        return $this->hasMany('App\Notification');
    }

    public function setting()
    {
        return $this->hasOne('App\Setting');
    }

    public function dashboard()
    {
        return $this->hasOne('App\Dashboard');
    }

}
