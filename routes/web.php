<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\RootController;
use App\Http\Controllers\AccountController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\SubcategoryController;
use App\Http\Controllers\DebtController;
use App\Http\Controllers\RegistryController;
use App\Http\Controllers\ObjectiveController;
use App\Http\Controllers\ReminderController;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\NotificationController;

Auth::routes();

Route::get('/', function () {
    return redirect('home');
});

Route::get('/home'                      , [HomeController::class, 'index'])->name('home');

Route::get('root'                       , [RootController::class, 'index'])->middleware('root');
Route::post('root/user/destroy/{ID}'    , [RootController::class, 'user_destroy'])->middleware('root');
Route::post('root/password/{ID}'        , [RootController::class, 'user_password'])->middleware('root');

Route::resource('account'               , AccountController::class)->middleware('auth');
Route::post('account/share'             , [AccountController::class, 'share'])->middleware('auth');
Route::post('account/transfer'          , [AccountController::class, 'transfer'])->middleware('auth');
Route::post('account/exit/{id}'         , [AccountController::class, 'exit'])->middleware('auth');
Route::post('delete/user/account'       , [AccountController::class, 'delete_user'])->middleware('auth');
Route::post('delete/user/shared'        , [AccountController::class, 'delete_shared'])->middleware('auth');
Route::get('account/web/{code}'         , [AccountController::class, 'web']);

Route::get('invitation/{CODE}'          , [AccountController::class, 'get_invitation']);
Route::post('invitation'                , [AccountController::class, 'set_invitation']);

Route::resource('user'                  , UserController::class)->middleware('auth');
Route::resource('category'              , CategoryController::class)->middleware('auth');
Route::resource('subcategory'           , SubcategoryController::class)->middleware('auth');
Route::resource('setting'               , SettingController::class)->middleware('auth');
Route::resource('notification'          , NotificationController::class)->middleware('auth');

Route::resource('debt'                  , DebtController::class)->middleware('auth');
Route::post('debt/add'                  , [DebtController::class, 'add'])->middleware('auth');

Route::resource('registry'              , RegistryController::class)->middleware('auth');
Route::post('search'                    , [RegistryController::class, 'search'])->middleware('auth');

Route::resource('objective'             , ObjectiveController::class)->middleware('auth');
Route::post('objective/add'             , [ObjectiveController::class, 'add'])->middleware('auth');

Route::resource('reminder'              , ReminderController::class)->middleware('auth');
Route::post('reminder/check/{ID}'       , [ReminderController::class, 'check'])->middleware('auth');

Route::post('filter'                    , [HomeController::class, 'filter'])->middleware('auth');

Route::get('my'                         , [HomeController::class, 'my_get'])->middleware('auth');
Route::post('my'                        , [HomeController::class, 'my_set'])->middleware('auth');
Route::get('delete/avatar'              , [HomeController::class, 'delete_avatar'])->middleware('auth');

Route::post('dashboard/card'            , [HomeController::class, 'dashboard'])->middleware('auth');
Route::post('dashboard/card/delete'     , [HomeController::class, 'dashboard_delete'])->middleware('auth');
Route::get('dashboard/card/{id}'        , [HomeController::class, 'dashboard_account'])->middleware('auth');

Route::get('test_sms'                   , [HomeController::class, 'test_sms']);
Route::get('test_email'                 , [HomeController::class, 'test_email']);

Route::get('test', function()
{
    email('emails.test', "Prueba", [], "diaz.mathias.14@gmail.com");
    dd("Se envio el correo");
    return view('emails.test');
});

Route::get('refresh-csrf', function(){
    return csrf_token();
});

