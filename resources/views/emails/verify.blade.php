<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional //EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>

<html xmlns='http://www.w3.org/1999/xhtml' xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:v='urn:schemas-microsoft-com:vml'>
<head>
	<!--[if gte mso 9]><xml><o:OfficeDocumentSettings><o:AllowPNG/><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings></xml><![endif]-->
	<meta content='text/html; charset=utf-8' http-equiv='Content-Type'/>
	<meta content='width=device-width' name='viewport'/>
	<!--[if !mso]><!-->
	<meta content='IE=edge' http-equiv='X-UA-Compatible'/>
	<!--<![endif]-->
	<title></title>
	<!--[if !mso]><!-->
	<link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'/>
	<style>
		@font-face {
			font-family: Poppins;
			src: url('http://glik.uy/fonts/Poppins-Bold.ttf');
		}

		*{
			margin:0;
			font-family: Poppins;
		}

		.btn
		{
			text-decoration: none;
			background: #235693;
			padding: 10px;
			border-radius: 5px;
			color:white;
			margin:20px;
		}
	</style>
</head>
<body>
	<table class='full-width-container' border='0' cellpadding='0' cellspacing='0' height='100%' width='100%' bgcolor='#00B36F' style='width: 100%; height: 100%; padding: 30px 0 30px 0;'>
			<tr>
				<td align='center' valign='top'>
					<!-- / 700px container -->
					<table class='container' border='0' cellpadding='0' cellspacing='0' width='700' bgcolor='#ffffff' style='width: 700px;'>
						<tr>
							<td align='center' valign='top'>
								<!-- / Header -->
								<table class='container header' border='0' cellpadding='0' cellspacing='0' width='620' style='width: 620px;'>
									<tr>
										<td style='padding: 30px 0 30px 0; border-bottom: solid 1px #eeeeee;' align='left'>
											<a href='#' style='font-size: 30px; text-decoration: none; color: #000000;'>
												<img src='http://glik.uy/images/logo_horizontal.png' width='150px' alt=''>
											</a>
										</td>
									</tr>
								</table>
								<!-- /// Header -->

								<!-- / Hero subheader -->
								<table class='container hero-subheader' border='0' cellpadding='0' cellspacing='0' width='620' style='width: 620px;'>
									<tr>
										<td class='hero-subheader__title' style='font-size: 43px; font-weight: bold; padding: 80px 0 15px 0;' align='center'>Bienvenido/a</td>
									</tr>

									<tr>
										<td class='hero-subheader__content' style='font-size: 20px; line-height: 27px; color: #969696; padding: 0 0px 90px 0;' align='center'>
											<p>Estamos muy contentos de que formes parte de nuestra plataforma, ahora solo necesitamos que confirmes tu correo electrónico haciendo click en el siguiente botón.</p>
											<br /><br /><br />
											<a class='btn' href='{{ url('verify', $confirmation_code) }}'>Verificar correo</a>
											<br /><br /><br />
											<small style='font-size: 10px;'>Si el enlace no funciona, copie y pegue la siguiente url en el navegador: {{ url('verify', $confirmation_code) }}</small>
											<p>Muchas gracias por elegirnos</p>
										</td>
									</tr>
								</table>
								<!-- /// Hero subheader -->

								<!-- / Divider -->
								<table class='container' border='0' cellpadding='0' cellspacing='0' width='100%' align='center'>
									<tr>
										<td align='center'>
											<table class='container' border='0' cellpadding='0' cellspacing='0' width='620' align='center' style='border-bottom: solid 1px #eeeeee; width: 620px;'>
												<tr>
													<td align='center'>&nbsp;</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
								<!-- /// Divider -->

								<!-- / Footer -->
								<table class='container' border='0' cellpadding='0' cellspacing='0' width='100%' align='center'>
									<tr>
										<td align='center'>
											<table class='container' border='0' cellpadding='0' cellspacing='0' width='620' align='center' style='border-top: 1px solid #eeeeee; width: 620px;'>
												<tr>
													<td style='text-align: center; padding: 50px 0 10px 0;'>
														<a href='#' style='font-size: 28px; text-decoration: none; color: #d5d5d5;'>Glik.uy</a>
													</td>
												</tr>

												<tr>
													<td align='middle'>
														<table width='60' height='2' border='0' cellpadding='0' cellspacing='0' style='width: 60px; height: 2px;'>
															<tr>
																<td align='middle' width='200'>
																	<a href='http://facebook.com/glikuy' target='_blank'>
																		<img src='http://glik.uy/images/facebook.png' width='40px' alt='' />
																	</a>
																</td>
																<td>
																	<a href='http://instagram.com/glikuy' target='_blank'>
																		<img src='http://glik.uy/images/instagram.png' width='40px' alt='' />
																	</a>
																</td>
															</tr>
														</table>
													</td>
												</tr>

												<tr>
													<td style='color: #d5d5d5; text-align: center; font-size: 15px; padding: 10px 0 60px 0; line-height: 22px;'>Este mensaje se genero automaticamente, <br />por favor no responda</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
								<!-- /// Footer -->
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
</body>
</html>