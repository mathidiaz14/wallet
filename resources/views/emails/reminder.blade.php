@extends('emails.master')

@section('content')
	<table class='container hero-subheader' border='0' cellpadding='0' cellspacing='0' width='620' style='width: 620px;'>
        <tr>
            <td class='hero-subheader__title' style='font-size: 43px; font-weight: bold; padding: 30px 0 15px 0;' align='center'>Recordatorio de Wallet</td>
        </tr>

        <tr>
            <td class='hero-subheader__content' style=' line-height: 27px; color: #969696; ' align='center'>
                <p>Te recordamos que hoy vence un recordatorio que creaste en la aplicación, <a href="{{url('reminder')}}">Ingresa aqui</a> para revisar los recordatorios</p>
            </td>
        </tr>
    </table>
@endsection