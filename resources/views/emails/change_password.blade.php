@extends('emails.master')

@section('content')
	<table class='container hero-subheader' border='0' cellpadding='0' cellspacing='0' width='620' style='width: 620px;'>
	    <tr>
	        <td class='hero-subheader__title' style='font-size: 43px; font-weight: bold; padding: 30px 0 15px 0;' align='center'>Cambio de contraseña</td>
	    </tr>

	    <tr>
	        <td class='hero-subheader__content' style=' line-height: 27px; color: #969696; ' align='center'>
	            <p>Un administrador cambio la contraseña de tu cuenta, la contraseña para que ingreses nuevamente es: <b>{{$password}}</b></p>
	        </td>
	    </tr>
	</table>
@endsection