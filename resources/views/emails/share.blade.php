@extends('emails.master')

@section('content')
	<!-- / Hero subheader -->
	<table class='container hero-subheader' border='0' cellpadding='0' cellspacing='0' width='620' style='width: 620px;'>
		<tr>
			<td class='hero-subheader__title' style='font-size: 43px; font-weight: bold; padding: 40px 0 15px 0;' align='center'>Bienvenido/a</td>
		</tr>

		<tr>
			<td class='hero-subheader__content' style='font-size: 20px; line-height: 27px; color: #969696;' align='center'>
				<p>{{$user}} te envio una invitación para que ingreses al sistema, haz click en el siguiente enlace para activar una cuenta</p>
				<br /><br /><br />
				<a class='btn' style='color:white;' href="{{ url('invitation', $code) }}">Activar cuenta</a>
				<br /><br /><br />
				<small style='font-size: 10px;'>Si el enlace no funciona, copie y pegue la siguiente url en el navegador: <br>{{ url('invitation', $code) }}</small>
			</td>
		</tr>
	</table>
	<!-- /// Hero subheader -->
@endsection
