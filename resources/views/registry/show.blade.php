<!-- Button trigger modal -->
<button type="button" class="btn btn-success" data-toggle="modal" data-target="#showRegistry_{{$registry->id}}">
    <i class="fa fa-eye"></i>
</button>

<!-- Modal -->
<div class="modal fade" id="showRegistry_{{$registry->id}}" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editModalLabel">Detalle del registro</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-left">
				<div class="col">
					<div class="table table-responsive">
						<table class="table table-striped">
							<tbody>
								<tr>
									<td>
										Monto
									</td>
									<td>
										<b> 
											@if($registry->type == "less")
						                      	<i class="fa fa-minus" style="color:red;"></i>
						                    @else
						                      	<i class="fa fa-plus" style="color:green;"></i>
						                    @endif
											{{$registry->amount}}
										</b>
									</td>
								</tr>
								<tr>
									<td>
										Cuenta
									</td>
									<td>
										@if($registry->account != null)
											<a href="{{url('account', $registry->account->id)}}" style="background-color: {{$registry->account->color}}; color:white;
							                      padding: 5px 30px; 
							                      border-radius: 5px;">
						                      	{{$registry->account->title}}
						                    </a>
						                @endif
									</td>
								</tr>
								<tr>
									<td>
										Usuario
									</td>
									<td>
										<a href="{{url('user', $registry->user_id)}}">
											<b>{{$registry->user->name}}</b>
										</a>
									</td>
								</tr>
								<tr>
									<td>
										Categoria
									</td>
									<td>
										@if($registry->subcategory_id == "transfer")
										  <span style="background-color:gray;" class="subcategory-bg">
										    Transferencia
										  </span>
										@elseif($registry->subcategory_id == "debt")
										  <span style="background-color:#f1e66a;" class="subcategory-bg">
										    Prestamo
										  </span>
										@elseif($registry->subcategory_id == null)
										  <span style="background-color:gray;" class="subcategory-bg">
										    Sin categoria
										  </span>
										@else
											<span style="background-color:
						                      {{$registry->category->color}};" class="subcategory-bg">
												{{$registry->category->title}}
											</span>
										@endif
									</td>
								</tr>
								<tr>
									<td>
										Subcategoria
									</td>
									<td>
										@include('registry.subcategory_show')
									</td>
								</tr>
								<tr>
									<td>
										Observación
									</td>
									<td>
										<b>{{$registry->description}}</b>
									</td>
								</tr>
								<tr>
									<td>
										Forma de pago
									</td>
									<td>
										<b>
											@switch($registry->way)
											    @case('debt')
											        Debito
											        @break
											    @case('cash')
											        Efectivo
											        @break
											    @case('credit')
											        Credito
											        @break
											    @case('transfer')
											        Transferencia
											        @break
											@endswitch
										</b>
									</td>
								</tr>
								<tr>
									<td>
										Fecha
									</td>
									<td>
										<b>{{$registry->date->format('d/m/Y')}}</b>
									</td>
								</tr>
								<tr>
									<td>
										Hora
									</td>
									<td>
										<b>{{$registry->hour}}</b>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
            </div>
            <div class="modal-footer">
            	<div class="col text-left">
	            	<button type="button" class="btn btn-secondary" data-dismiss="modal">
	                	<i class="fa fa-times"></i>
	                    Cerrar
	                </button>
            	</div>
            </div>
        </div>
    </div>
</div>