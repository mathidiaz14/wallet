<input type="hidden" name="subcategory" id="subcategory">
<br>
<button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#categoryModal" id="category_select">
	<i class="fa fa-external-link-alt"></i>
	Seleccionar Categoria
</button>

 <!-- Modal -->
<div class="modal fade" id="categoryModal" tabindex="-1" role="dialog" aria-labelledby="categoryModalLabel" aria-hidden="true" >
    <div class="modal-dialog" role="document">
      	<div class="modal-content">
        	<div class="modal-header">
          		<h5 class="modal-title">Categoria</h5>
	         	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	            	<span aria-hidden="true">&times;</span>
	          	</button>
        	</div>
        	<div class="modal-body ">
	          	<div class="row row_category">
	          		@foreach(Auth::user()->categories as $category)
		              	<div class="col-6">
		              		<a class="btn btn_category btn-block" attr-id="{{$category->id}}" style="background-color: {{$category->color}}; color:white; margin-bottom: 10px; cursor:pointer;">
			              		{{$category->title}}
			              	</a>
		              	</div>
		          	@endforeach
	          	</div>

	          	@foreach(Auth::user()->categories as $category)
					<div class="row row_subcategory" id="category_{{$category->id}}" style="display: none;">
		              	@foreach($category->subcategories as $subcategory)
							<div class="col-6">
								<a class="btn btn_subcategory btn-block" 
								attr-color="@if($subcategory->color == null){{$category->color}}@else{{$subcategory->color}}@endif" 
								attr-name="{{$subcategory->title}}"
								attr-id="{{$subcategory->id}}" 
								attr-type="{{$subcategory->category->type}}"
								style="background-color:@if($subcategory->color == null){{$category->color}}@else{{$subcategory->color}}@endif; color:white;  margin-bottom: 10px; cursor:pointer;">
				              		{{$subcategory->title}}
				              	</a>	
							</div>
		              	@endforeach

		              	<div class="col-6">
		              		<a class="btn btn-block btn-secondary btn_back" style= "margin-bottom: 10px; cursor:pointer; color:white;">
		              			<i class="fa fa-chevron-left"></i>
		              			Atras
		              		</a>
		              	</div> 
	              	</div>
	          	@endforeach

          		<hr>
	            <div class="row">
	              	<div class="col">
		                <button type="button" class="btn btn-secondary" data-dismiss="modal">
							<i class="fa fa-times"></i>
							Cerrar
		                </button>
	              	</div>
	         	</div>
        	</div>
      	</div>
    </div>
</div>

<script>
	$('.btn_subcategory').click(function()
	{
		$('#categoryModal').modal('hide');
		$('#category_select').removeClass('btn-primary');
		$('#category_select').css('background-color' , $(this).attr('attr-color'));
		$('#category_select').css('color' , 'white');
		$('#category_select').html($(this).attr('attr-name'));
		$('#subcategory').val($(this).attr('attr-id'));

		if ($(this).attr('attr-type') == 'less') 
			$('#basic-addon1').html('<i class="fa fa-minus"></i>');
		else
			$('#basic-addon1').html('<i class="fa fa-plus"></i>');

	});

	$('.btn_category').click(function()
	{
		$('.row_subcategory').hide();
		$('.row_category').hide();
		$('#category_'+$(this).attr('attr-id')).show();
	});

	$('.btn_back').click(function()
	{
		$('.row_subcategory').hide();
		$('.row_category').show();
	});

</script>