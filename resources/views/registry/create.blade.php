<!-- Button trigger modal -->
<button type="button" class="btn btn-primary btn_add_registry" data-toggle="modal" data-target="#addRegistryModal" id="addRegistryModalBtn">
	<span class="d-none d-xl-block">
		<i class="fa fa-plus"></i>			
		Agregar registro
	</span>

	<span class="d-xl-none">
		<i class="fa fa-plus"></i>
	</span>
</button>

  <!-- Modal -->
<div class="modal fade" id="addRegistryModal" tabindex="-1" role="dialog" aria-labelledby="addRegistryModalLabel" aria-hidden="true" >
    <div class="modal-dialog" role="document">
      	<div class="modal-content">
	        <div class="modal-header bg-gradient-primary">
	          	<h5 class="modal-title text-white">
	          		<i class="fas fa-fw fa-receipt"></i>
	          		Agregar registro
	          	</h5>
	         	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	            	<span aria-hidden="true">&times;</span>
	          	</button>
	        </div>
	        <div class="modal-body ">
	        	@if(Auth::user()->accounts->count() > 0)
		          	<form class="form-horizontal" action="{{url('registry')}}" method="post">
		            @csrf
			            <div class="row">
				            <div class="col-6">
								<label>Monto</label>
								<div class="input-group mb-3">
									<div class="input-group-prepend">
										<span class="input-group-text" id="basic-addon1">
											<i class="fa fa-minus"></i>
										</span>
									</div>
									<input type="number" class="form-control" aria-describedby="basic-addon1" name="amount" id="amount_registry" required>
								</div>
				            </div>
			            	<div class="col-6">
				                <div class="form-group">
				                    <label>Cuenta</label>
				                    <select name="account" id="account" class="form-control">
				                      	@foreach(Auth::user()->accounts as $account)
				                      		@if(Setting('default_account') == $account->id)
												<option value="{{$account->id}}" selected="">{{$account->title}}</option>
											@else
												<option value="{{$account->id}}">{{$account->title}}</option>
											@endif
				                      	@endforeach
				                    </select>
				                </div>
				            </div>
				            <div class="col-6">
				                <div class="form-group">
				                    <label>Categoria</label>
				                    @if((Setting('remember_subcategory') == null) or (Auth::user()->registries->count() == 0))
					                    <input type="hidden" name="subcategory" id="subcategory">
										<br>
										<button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#categoryModal" id="category_select">
											<i class="fa fa-external-link-alt"></i>
											Seleccionar Categoria
										</button>
									@else
										@php
											$last = Auth::user()->registries->first();
										@endphp
										<input type="hidden" name="subcategory" id="subcategory" value="{{$last->subcategory->id}}">
										<br>
										<button type="button" class="btn btn-block" data-toggle="modal" data-target="#categoryModal" id="category_select" style="background-color: @if($last->subcategory->color == null) {{$last->subcategory->category->color}}@else {{$last->subcategory->color}}@endif; color:white;">
											{{$last->subcategory->title}}
										</button>
									@endif
				                </div>
				            </div>
				            <div class="col-6">
				                <div class="form-group">
				                    <label>Forma de pago</label>
				                    <select name="way" id="way" class="form-control">
				                  		<option value="debt">Debito</option>
				                  		<option value="cash">Efectivo</option>
				                  		<option value="credit">Credito</option>
				                  		<option value="transfer">Transferencia</option>
				                    </select>
				                </div>
				            </div>
				            <div class="col-12">
				                <div class="form-group">
				                    <label>Observación</label>
				                	<input type="text" name="description" id="description" class="form-control">
				                </div>
				            </div>
				            <div class="col-6">
				                <div class="form-group">
				                    <label>Fecha</label>
				                    <input type="date" name="date" id="date" value="" class="form-control">
				                </div>
				            </div>
				            <div class="col-6">
				                <div class="form-group">
				                    <label>Hora</label>
				                    <input type="time" name="hour" id="hour" value="" class="form-control">
				                </div>
				            </div>
			            </div>
			            <hr>
			            <div class="row">
			              	<div class="col">
				                <button type="button" class="btn btn-secondary" data-dismiss="modal">
									<i class="fa fa-times"></i>
									Cerrar
				                </button>
			              	</div>
			              	<div class="col text-right">
				                <button class="btn btn-primary">
									<i class="fa fa-save"></i>
									Guardar
				                </button>
			              	</div>  
			            </div>
		          	</form>
	            @else
		            <div class="col text-center">
		            	<h4 style="color:red;">No tiene ninguna cuenta, <a href="{{url('account')}}">ingrese aqui</a> para crear una.</h4>
		            </div>
	            @endif
	        </div>
      </div>
    </div>
</div>


 <!-- Modal -->
<div class="modal fade" id="categoryModal" tabindex="-1" role="dialog" aria-labelledby="categoryModalLabel" aria-hidden="true" >
    <div class="modal-dialog" role="document">
      	<div class="modal-content">
        	<div class="modal-header">
          		<h5 class="modal-title">Categoria</h5>
	         	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	            	<span aria-hidden="true">&times;</span>
	          	</button>
        	</div>
        	<div class="modal-body ">
	          	<div class="row row_category">
	          		@foreach(Auth::user()->categories as $category)
		              	<div class="col-6">
		              		<a class="btn btn_category btn-block" attr-id="{{$category->id}}" style="background-color: {{$category->color}}; color:white; margin-bottom: 10px; cursor:pointer;">
			              		{{$category->title}}
			              	</a>
		              	</div>
		          	@endforeach
	          	</div>

	          	@foreach(Auth::user()->categories as $category)
					<div class="row row_subcategory" id="category_{{$category->id}}" style="display: none;">
		              	@foreach($category->subcategories as $subcategory)
							<div class="col-6">
								<a class="btn btn_subcategory btn-block" 
								attr-color="@if($subcategory->color == null){{$category->color}}@else{{$subcategory->color}}@endif" 
								attr-name="{{$subcategory->title}}"
								attr-id="{{$subcategory->id}}" 
								attr-type="{{$subcategory->category->type}}"
								style="background-color:@if($subcategory->color == null){{$category->color}}@else{{$subcategory->color}}@endif; color:white;  margin-bottom: 10px; cursor:pointer;">
				              		{{$subcategory->title}}
				              	</a>	
							</div>
		              	@endforeach

		              	<div class="col-6">
		              		<a class="btn btn-block btn-secondary btn_back" style= "margin-bottom: 10px; cursor:pointer; color:white;">
		              			<i class="fa fa-chevron-left"></i>
		              			Atras
		              		</a>
		              	</div> 
	              	</div>
	          	@endforeach

          		<hr>
	            <div class="row">
	              	<div class="col">
		                <button type="button" class="btn btn-secondary" data-dismiss="modal">
							<i class="fa fa-times"></i>
							Cerrar
		                </button>
	              	</div>
	         	</div>
        	</div>
      	</div>
    </div>
</div>

<script>	
    $(document).ready(function()
    {
		$('#addRegistryModal').on('shown.bs.modal', function () 
		{
			$('#amount_registry').trigger('focus');
		});

		$('.btn_add_registry').click(function()
		{
			console.log('Click en btn_add_registry');
			$('#addRegistryModal #date').val("");
			$('#addRegistryModal #hour').val("");

			$('#addRegistryModal #date').val("{{Carbon\Carbon::now()->format('Y-m-d')}}");
			$('#addRegistryModal #hour').val("{{Carbon\Carbon::now()->format('H:i')}}");
		});

		$('#categoryModal').on('show.bs.modal', function()
		{
			$('#addRegistryModal').modal('hide');
		});

		$('.btn_subcategory').click(function()
		{
			$('#categoryModal').modal('hide');
			$('#category_select').removeClass('btn-primary');
			$('#category_select').css('background-color' , $(this).attr('attr-color'));
			$('#category_select').css('color' , 'white');
			$('#category_select').html($(this).attr('attr-name'));
			$('#subcategory').val($(this).attr('attr-id'));

			if ($(this).attr('attr-type') == 'less') 
				$('#basic-addon1').html('<i class="fa fa-minus"></i>');
			else
				$('#basic-addon1').html('<i class="fa fa-plus"></i>');
			
			$('#addRegistryModal').modal('show');

		});

		$('.btn_category').click(function()
		{
			$('.row_subcategory').hide();
			$('.row_category').hide();
			$('#category_'+$(this).attr('attr-id')).show();
		});

		$('.btn_back').click(function()
		{
			$('.row_subcategory').hide();
			$('.row_category').show();
		});
    });
</script>