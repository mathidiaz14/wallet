@extends('layouts.dashboard', ['menu' => 'registry'])

@section('content')
<div class="row">
  <div class="col-sm-12">
    <div class="card shadow">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Registros</h6>
      </div>
      <div class="card-body">
        <div class="table table-responsive">
          <table class="table table-striped">
            <tbody>
              <tr class="text-center">
                <th>Fecha</th>
                <th>Monto</th>
                <th>Categoria</th>
                <th>Descripción</th>
                <th>Usuario</th>
                <th>Cuenta</th>
                <th></th>
                <th></th>
                <th></th>
              </tr>
              @foreach($registries as $registry)
                <tr class="text-center" attr-account="{{$registry->account_id}}" attr-subcategory="{{$registry->subcategory_id}}" attr-show="true">
                  <td>{{$registry->date->format('d/m/Y')}} {{$registry->hour}}</td>
                  <td>
                    @if($registry->type == "less")
                      <b style="color:red;">
                        -{{$registry->amount}}
                      </b>
                    @else
                      <b style="color:green;">
                        +{{$registry->amount}}
                      </b>
                    @endif
                  </td>
                  <td>
                    @include('registry.subcategory_show')
                  </td>
                  <td>{{$registry->description}}</td>
                  <td>{{$registry->user->name}}</td>
                  <td>
                    @if($registry->account != null)
                      <a href="{{url('account', $registry->account->id)}}" style="
                        background-color: {{$registry->account->color}}; 
                        color:white;
                        padding: 5px 30px; 
                        border-radius: 5px;">
                        {{$registry->account->title}}
                      </a>
                    @endif
                  </td>
                  <td>
                    @include('registry.show', ['registry' => $registry])
                  </td>
                  <td>
                    @include('registry.edit', ['registry' => $registry])
                  </td>
                  <td>
                    @include('helper.delete', ['id' => $registry->id, 'route' => url('registry', $registry->id)])
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>  
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
  
@endsection