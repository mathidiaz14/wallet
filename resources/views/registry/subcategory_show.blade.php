@if($registry->subcategory_id == "transfer")
  <span style="background-color:gray;" class="subcategory-bg">
    Transferencia
  </span>
@elseif($registry->subcategory_id == "debt")
  <span style="background-color:#f1e66a;" class="subcategory-bg">
    Prestamo
  </span>
@elseif($registry->subcategory_id == null)
  <span style="background-color:gray;" class="subcategory-bg">
    Sin categoria
  </span>
@else
  <span style="background-color:
    @if($registry->subcategory->color == null)
      {{$registry->category->color}}
    @else
      {{$registry->subcategory->color}} 
    @endif;" class="subcategory-bg">
    {{$registry->subcategory->title}}
  </span>
@endif