@extends('layouts.dashboard', ['menu' => 'registry'])

@section('content')
<div class="row">
  <div class="col-sm-12 col-md-4 col-lg-3">
    
    <div class="card shadow">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Filtros</h6>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-sm-12">
            <p><b>Cuentas</b></p>
            @foreach(Auth::user()->accounts as $account)
                <input type="checkbox" id="account_{{$account->id}}" attr-id="{{$account->id}}" class="check_account"/> 
                <label for="account_{{$account->id}}">{{$account->title}}</label> <br>
            @endforeach
          </div>
          <div class="col-sm-12">
          <hr>
            <p><b>Categorias</b></p>
            @foreach(Auth::user()->categories as $category)  
              <small><b>{{$category->title}}</b></small>
              <br>
              @foreach($category->subcategories as $subcategory)
                <input type="checkbox" id="subcategory_{{$subcategory->id}}" attr-id="{{$subcategory->id}}" class="check_subcategory" />  
                <label for="subcategory_{{$subcategory->id}}">{{$subcategory->title}}</label>
                <br>
              @endforeach
            @endforeach
          </div>
          <div class="col-sm-12">
            <hr>
            <p><b>Periodo</b></p>
            <select name="period" id="" class="form-control">
              <option value="ever" selected="">Desde el comienzo</option>
              <option value="year">Ultimo año</option>
              <option value="6month">Ultimos 6 meses</option>
              <option value="3month">Ultimos 3 meses</option>
              <option value="month">Ultimo mes</option>
              <option value="week">Ultima semana</option>
            </select>
          </div>
        </div>
      </div>
      <div class="card-footer">
        <div class="col text-right">
          <a class="btn btn-primary btn-clear text-white">
            <i class="fa fa-eraser"></i>
            Limpiar
          </a>
        </div>
      </div>
    </div>
    <br>
  </div>
  <div class="col-sm-12 col-md-8 col-lg-9">
    <div class="card shadow">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Registros</h6>
      </div>
      <div class="card-body">
        <div class="table table-responsive">
          <table class="table table-striped">
            <tbody>
              <tr class="text-center">
                <th>Fecha</th>
                <th>Monto</th>
                <th>Subcategoria</th>
                <th>Usuario</th>
                <th>Cuenta</th>
                <th></th>
                <th></th>
                <th></th>
              </tr>
              @foreach($registries as $registry)
                <tr class="text-center" attr-account="{{$registry->account_id}}" attr-subcategory="{{$registry->subcategory_id}}" attr-show="true">
                  <td>{{$registry->date->format('d/m/Y')}} {{$registry->hour}}</td>
                  <td>
                    @if($registry->type == "less")
                      <b style="color:red;">
                        -{{$registry->amount}}
                      </b>
                    @else
                      <b style="color:green;">
                        +{{$registry->amount}}
                      </b>
                    @endif
                  </td>
                  <td>
                    @include('registry.subcategory_show')
                  </td>
                  <td>{{$registry->user->name}}</td>
                  <td>
                    @if($registry->account != null)
                      <a href="{{url('account', $registry->account->id)}}" style="
                        background-color: {{$registry->account->color}}; 
                        color:white;
                        padding: 5px 30px; 
                        border-radius: 5px;">
                        {{$registry->account->title}}
                      </a>
                    @endif
                  </td>
                  <td>
                    @include('registry.show', ['registry' => $registry])
                  </td>
                  <td>
                    @include('registry.edit', ['registry' => $registry])
                  </td>
                  <td>
                    @include('helper.delete', ['id' => $registry->id, 'route' => url('registry', $registry->id)])
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>  
        </div>
        <div class="col-12">
          @include('helper.links', ['links' => $registries])
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
  <script>
    $(document).ready(function()
    {
      $('.check_account').click(function()
      {
        var id = $(this).attr('attr-id');

        if($(this).is(':checked'))
        {
          var show = $('tr[attr-show="true"]');
          
          show.each(function(index)
          {
            if($(this).attr('attr-account') != id)
            {
              $(this).attr('attr-show', 'false');
              $(this).hide();
            }
          });
        
        }else
        {
          var show = $('tr[attr-show="false"]');
          
          show.each(function(index)
          {
            if($(this).attr('attr-account') != id)
            {
              $(this).attr('attr-show', 'true');
              $(this).show();
            }
          });
        }
      });

      $('.check_subcategory').click(function()
      {
        var id = $(this).attr('attr-id');

        if($(this).is(':checked'))
        {
          var show = $('tr[attr-show="true"]');
          
          show.each(function(index)
          {
            if($(this).attr('attr-subcategory') != id)
            {
              $(this).attr('attr-show', 'false');
              $(this).hide();
            }
          });
        
        }else
        {
          var show = $('tr[attr-show="false"]');
          
          show.each(function(index)
          {
            if($(this).attr('attr-subcategory') != id)
            {
              $(this).attr('attr-show', 'true');
              $(this).show();
            }
          });
        }
      });

      $('.btn-clear').click(function()
      {
        $('.check_account').prop('checked', false);
        $('.check_subcategory').prop('checked', false);
        $('tr').attr('attr-show' ,"true");
        $('tr').show();
      });
    });
  </script>
@endsection