@if(($registry->subcategory_id == "transfer") or ($registry->subcategory_id == "debt"))
	<a class="btn btn-info disabled">
		<i class="fa fa-edit"></i>
	</a>
@else
	<button type="button" class="btn btn-info" data-toggle="modal" data-target="#editRegistryModal_{{$registry->id}}">
		<i class="fa fa-edit"></i>				
	</button>

	  <!-- Modal -->
	<div class="modal fade" id="editRegistryModal_{{$registry->id}}" tabindex="-1" role="dialog" aria-labelledby="editRegistryModalLabel_{{$registry->id}}" aria-hidden="true" >
	    <div class="modal-dialog" role="document">
	      	<div class="modal-content">
		        <div class="modal-header bg-gradient-info">
		          	<h5 class="modal-title text-white">
		          		<i class="fas fa-fw fa-receipt"></i>
		          		Editar registro
		          	</h5>
		         	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		            	<span aria-hidden="true">&times;</span>
		          	</button>
		        </div>
		        <div class="modal-body text-left">
	        	  	<form class="form-horizontal" action="{{url('registry', $registry->id)}}" method="post">
		            @csrf
		            @method('PATCH')
			            <div class="row">
				            <div class="col-6">
								<label>Monto</label>
								<input type="number" class="form-control" aria-describedby="basic-addon1" name="amount" id="amount_registry" value="{{$registry->amount}}" required>
				            </div>
			            	<div class="col-6">
				                <div class="form-group">
				                    <label>Cuenta</label>
				                    <select name="account" id="" class="form-control">
				                      	@foreach(Auth::user()->accounts as $account)
											@if($account->id == $registry->account_id)
												<option value="{{$account->id}}" selected="">{{$account->title}}</option>
											@else
												<option value="{{$account->id}}">{{$account->title}}</option>
											@endif
				                      	@endforeach
				                    </select>
				                </div>
				            </div>
				            <div class="col-6">
				                <div class="form-group">
				                    <label>Categoria</label>
				                    <select name="subcategory" id="" class="form-control">
										<option value="null">Sin categoria</option>
				                    	@foreach(Auth::user()->categories as $category)
											<optgroup label="{{$category->title}}">
												@foreach($category->subcategories as $subcategory)
													@if($subcategory->id == $registry->subcategory_id)
														<option value="{{$subcategory->id}}" selected="">{{$subcategory->title}}</option>
													@else
														<option value="{{$subcategory->id}}">{{$subcategory->title}}</option>
													@endif
												@endforeach
											</optgroup>
				                    	@endforeach
				                    </select>
				                </div>
				            </div>
				            <div class="col-6">
				                <div class="form-group">
				                    <label>Forma de pago</label>
				                    <select name="way" id="" class="form-control">
				                  		@if($registry->way == "debt")
				                  			<option value="debt" selected="">Debito</option>
				                  		@else
											<option value="debt">Debito</option>
				                  		@endif

				                  		@if($registry->way == "cash")
				                  			<option value="cash" selected="">Efectivo</option>
				                  		@else
											<option value="cash">Efectivo</option>
				                  		@endif

				                  		@if($registry->way == "credit")
				                  			<option value="credit" selected="">Credito</option>
				                  		@else
											<option value="credit">Credito</option>
				                  		@endif


				                  		@if($registry->way == "transfer")
				                  			<option value="transfer" selected="">Transferencia</option>
				                  		@else
											<option value="transfer">Transferencia</option>
				                  		@endif
				                    </select>
				                </div>
				            </div>
				            <div class="col-12">
				                <div class="form-group">
				                    <label>Observación</label>
				                    <input type="text" name="description" class="form-control" value="{{$registry->description}}">
				                </div>
				            </div>
				            <div class="col-6">
				                <div class="form-group">
				                    <label>Fecha</label>
				                    <input type="date" name="date" value="{{$registry->date->format('Y-m-d')}}" class="form-control" readonly>
				                </div>
				            </div>
				            <div class="col-6">
				                <div class="form-group">
				                    <label>Hora</label>
				                    <input type="time" name="hour" value="{{$registry->hour}}" class="form-control" readonly>
				                </div>
				            </div>
			            </div>
			            <hr>
			            <div class="row">
			              	<div class="col">
				                <button type="button" class="btn btn-secondary" data-dismiss="modal">
									<i class="fa fa-times"></i>
									Cerrar
				                </button>
			              	</div>
			              	<div class="col text-right">
				                <button class="btn btn-info">
									<i class="fa fa-save"></i>
									Guardar
				                </button>
			              	</div>  
			            </div>
		          	</form>
		        </div>
	      </div>
	    </div>
	</div>

	<script>	
	    $(document).ready(function()
	    {
			$('#editRegistryModal_{{$registry->id}}').on('shown.bs.modal', function () {
				$('#amount_registry').trigger('focus')
			});
	    });
	</script>
@endif