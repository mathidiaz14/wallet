@extends('layouts.dashboard', ['menu' => ''])

@section('content')
<div class="row">
  <div class="col-sm">
    <div class="card shadow">
      <div class="card-header py-3">
        <div class="row">
          <div class="col">
            <h6 class="m-0 font-weight-bold text-primary">Mi usuario</h6>
          </div>
          <div class="col text-right">
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-12">
            @include('helper.alert')
            <form action="{{url('my')}}" method="POST" class="form-horizontal" enctype="multipart/form-data">
              @csrf
              <div class="row">
                <div class="col-xs-12 col-md-3">
                  <div class="col-xs-12">
                    @if($user->avatar == null)
                      <img src="{{asset('images/avatar.png')}}" alt="" width="100%">
                    @else
                      <img src="{{asset($user->avatar)}}" alt="" width="100%">
                      <br><br>
                      <a href="{{url('delete/avatar')}}" class="btn btn-danger btn-block">
                        <i class="fa fa-trash"></i>
                        Eliminar avatar
                      </a>
                    @endif
                  </div>  
                  <div class="col-xs-12">
                    <br>
                    <input type="file" name="avatar">
                  </div>
                </div>
                <div class="col-xs-12 col-md-7 offset-md-2">
                  <div class="form-group">
                    <label for="">Nombre</label>
                    <input type="text" name="name" value="{{$user->name}}" class="form-control">
                  </div>
                  <div class="form-group">
                    <label for="">Email</label>
                    <input type="text" name="email" value="{{$user->email}}" class="form-control" readonly>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="form-group col-12 col-md-6">
                      <label for="">Contraseña</label>
                      <input type="password" name="password" class="form-control">
                    </div>
                    <div class="form-group col-12 col-md-6">
                      <label for="">Repetir contraseña</label>
                      <input type="password" name="password_confirmation" class="form-control">
                    </div>
                  </div>
                  <hr>
                  <div class="form-group text-right">
                    <button class="btn btn-info">
                      <i class="fa fa-save"></i>
                      Guardar
                    </button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div class="card-footer text-left">
        <!-- Button trigger modal -->
        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal">
          <i class="fa fa-times"></i>
          Eliminar mi cuenta
        </button>

        <!-- Modal -->
        <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true" >
          <div class="modal-dialog" role="document">
              <div class="modal-content">
              <div class="modal-header bg-gradient-danger">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="modal-body text-center">
                  <p><i class="fa fa-exclamation-triangle fa-4x"></i></p>
                    <h4>¿Realmente desea eliminar su cuenta?</h4>
                    <p>Recuerde que si elimina su cuenta, se eliminaran tambien todos los datos asociados a la misma (registros, cuentas, etc.)</p>
                    <br>
                    <div class="row">
                      <div class="col">
                    <button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">
                      NO
                    </button>
                      </div>
                      <div class="col">
                        <form action="{{url('user', $user->id)}}" method="POST">
                            @csrf
                            <input type='hidden' name='_method' value='DELETE'>
                            <button class="btn btn-danger btn-block">
                                SI
                            </button>
                        </form>
                      </div>
                    </div>
                </div>
              </div>
          </div>
        </div>        
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
  
@endsection