<!-- Button trigger modal -->
<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal_{{$id}}">
	<i class="fa fa-trash"></i>
</button>

<!-- Modal -->
<div class="modal fade" id="deleteModal_{{$id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true" >
	<div class="modal-dialog" role="document">
  		<div class="modal-content">
			<div class="modal-header bg-gradient-danger">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
    		</div>
    		<div class="modal-body text-center">
    			<p><i class="fa fa-exclamation-triangle fa-4x"></i></p>
      			<h4>¿Desea eliminar el item?</h4>
      			<br>
      			<p style="color:red;">Al eliminar la subcategoria, se cambiaran todos los registros de la subcategoria a nulo</p>
      			<br>
      			<div class="row">
      				<div class="col">
						<button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">
							NO
						</button>
      				</div>
      				<div class="col">
      					<form action="{{ url('subcategory', $id) }}" method="POST">
                    @csrf
                    <input type='hidden' name='_method' value='DELETE'>
                    <button class="btn btn-danger btn-block">
                        SI
                    </button>
                </form>
      				</div>
      			</div>
    		</div>
  		</div>
	</div>
</div>