<label class="switch">
  	<input type="checkbox" name="{{$name}}" id="{{$name}}" @if($active == "on") checked @endif>
  	<span class="slider round"></span>
</label>