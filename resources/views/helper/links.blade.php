@if($links->lastPage() > 1)
    <div class="row">
      <div class="col text-center">
        <a href="{{$links->previousPageUrl()}}" class="btn btn-dark">
          <i class="fa fa-chevron-left"></i>
        </a>
        
        @for($i=1;$i<=$links->lastPage();$i++)
          @if($links->currentPage() == $i)
            <a href="{{$links->url($i)}}" class="btn btn-info">
              {{$i}}
            </a>
          @else
            @if(($i == 1) or ($i == 2) or ($i == 3) or ($i == $links->lastPage() - 2) or ($i == $links->lastPage() - 1) or ($i == $links->lastPage()) or ($i == $links->currentPage() + 1) or ($i == $links->currentPage() - 1))
              <a href="{{$links->url($i)}}" class="btn btn-default">
                {{$i}}
              </a> 
            @endif
          @endif
        @endfor

        <a href="{{$links->nextPageUrl()}}" class="btn btn-dark">
          <i class="fa fa-chevron-right"></i>
        </a>
      </div>
    </div>
@endif