@if(session()->has('error'))
	<div class="alert alert-danger" role="alert">
	  	{{session('error')}}
		{{session()->forget('error')}}
	</div>
@elseif(session()->has('success'))
	<div class="alert alert-success" role="alert">
	  	{{session('success')}}
		{{session()->forget('success')}}
	</div>
@endif