
<li class="nav-item dropdown no-arrow mx-1">
  <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <i class="fas fa-bell fa-fw"></i>
    <!-- Counter - Alerts -->
    @if(Auth::user()->notifications->count() > 0)
      <span class="badge badge-danger badge-counter">
        {{Auth::user()->notifications->count()}}
      </span>
    @endif
  </a>
  <!-- Dropdown - Alerts -->
  <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">
    <h6 class="dropdown-header">
      Alertas
    </h6>
    @if(Auth::user()->notifications->count() > 0)
      @foreach(Auth::user()->notifications->sortByDesc('created_at') as $notification)
        <a class="dropdown-item d-flex align-items-center" href="{{url('notification', $notification->id)}}">
          <div class="mr-3">
            <div class="icon-circle bg-primary">
              <i class="fas fa-file-alt text-white"></i>
            </div>
          </div>
          <div>
            <div class="small text-gray-500">{{$notification->created_at->format('d/m/Y H:i')}}</div>
            <span class="font-weight-bold">{{$notification->title}}</span>
            <br>
            <span class="">{{$notification->description}}</span>
          </div>
        </a>
      @endforeach
    @else
      <a class="dropdown-item d-flex align-items-center">
        <span class="font-weight-bold">No tiene notificaciones</span>
      </a>
    @endif
  </div>
</li>