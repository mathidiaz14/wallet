<a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{url('/')}}">
    <div class="sidebar-brand-icon rotate-n-15">
      <img src="{{asset('logo.png')}}" width="50px" alt="">
    </div>
    <div class="sidebar-brand-text mx-3">Finanzas personales</div>
  </a>

  <!-- Divider -->
  <hr class="sidebar-divider my-0">
  
  <li class="nav-item">
    <a class="nav-link">
      Bienvenido/a <br> <b>{{Auth::user()->name}}</b>
    </a>
  </li>

  <!-- Divider -->
  <hr class="sidebar-divider">
  
  <!-- Nav Item - Dashboard -->
  <li class="nav-item @if($menu == 'dashboard') active @endif ">
    <a class="nav-link" href="{{url('/')}}">
      <i class="fas fa-fw fa-tachometer-alt"></i>
      <span>Dashboard</span></a>
  </li>

  <li class="nav-item @if($menu == 'account') active @endif">
    <a class="nav-link" href="{{url('account')}}">
      <i class="fas fa-fw fa-wallet"></i>
      <span>Cuentas</span></a>
  </li>

  <li class="nav-item @if($menu == 'registry') active @endif">
    <a class="nav-link" href="{{url('registry')}}">
      <i class="fas fa-fw fa-receipt"></i>
      <span>Registros</span></a>
  </li>

  <li class="nav-item @if($menu == 'category') active @endif">
    <a class="nav-link" href="{{url('category')}}">
      <i class="fas fa-fw fa-boxes"></i>
      <span>Categorias</span></a>
  </li>
  
  <li class="nav-item @if($menu == 'objective') active @endif">
    <a class="nav-link" href="{{url('objective')}}">
      <i class="fas fa-fw fa-bullseye"></i>
      <span>Objetivos</span></a>
  </li>

  <li class="nav-item @if($menu == 'debt') active @endif">
    <a class="nav-link" href="{{url('debt')}}">
      <i class="fas fa-fw fa-hand-holding-usd"></i>
      <span>Deudas</span></a>
  </li>

  <li class="nav-item @if($menu == 'reminder') active @endif">
    <a class="nav-link" href="{{url('reminder')}}">
      <i class="fas fa-fw fa-stopwatch"></i>
      <span>Recordatorios</span></a>
  </li>

  @if(Auth::user()->root == "on")
    <li class="nav-item @if($menu == 'root') active @endif">
      <a class="nav-link" href="{{url('root')}}">
        <i class="fas fa-fw fa-chess"></i>
        <span>ROOT</span></a>
    </li>
  @endif

  <!-- Divider -->
  <hr class="sidebar-divider d-none d-md-block">

  <!-- Sidebar Toggler (Sidebar) -->
  <div class="text-center d-none d-md-inline">
    <button class="rounded-circle border-0" id="sidebarToggle"></button>
  </div>