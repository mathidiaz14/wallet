<li class="nav-item dropdown no-arrow">
  <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <span class="mr-2 d-none d-lg-inline text-gray-600 small">
      <i class="fa fa-chevron-down"></i>
      {{Auth::user()->name}}
    </span>
    @if(Auth::user()->avatar == null)
      <img class="img-profile rounded-circle" src="{{asset('images/avatar.png')}}">
    @else
      <img class="img-profile rounded-circle" src="{{asset(Auth::user()->avatar)}}">
    @endif
  </a>
  <!-- Dropdown - User Information -->
  <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
    <a class="dropdown-item" href="{{url('my')}}">
      <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
      Perfil
    </a>
    <a class="dropdown-item" href="{{url('setting')}}">
      <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
      Configuración
    </a>
    <div class="dropdown-divider"></div>
    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
      <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
      Cerrar sesión
    </a>
  </div>
</li>