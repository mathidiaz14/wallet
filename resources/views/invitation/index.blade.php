@extends('layouts.auth')

@section('content')
<div class="row">
  <div class="col-lg-6 d-none d-lg-block bg-register-image"></div>
  <div class="col-lg-6">
    <div class="p-5">
        <br><br>
        <div class="text-center">
            <h1 class="h1 text-gray-900 mb-4">Bienvenido</h1>
            <h4 class=" mb-4">Te invitaron a unirte a la plataforma, completa los datos y continua</h4>
        </div>
        <br>
        <form class="user" method="POST" action="{{ route('register') }}">
            @csrf
            <input type="hidden" name="invitation" value="{{$shared->id}}">
            <div class="form-group">
                <input id="email" type="email" class="form-control form-control-user" name="email" value="{{$shared->email}}" readonly="">
            </div>

            <div class="form-group">
                <input id="name" type="text" class="form-control form-control-user" name="name" value="{{ old('name') }}" placeholder="Nombre" required autofocus>
            </div>
            
            <div class="form-group">
                <input id="password" type="password" class="form-control form-control-user" name="password" required placeholder="Contraseña">

                @if ($errors->has('password'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
            
            <div class="form-group">
                <input id="password_confirmation" type="password" class="form-control form-control-user" name="password_confirmation" required placeholder="Repetir contraseña">
            </div>

            <button type="submit" class="btn btn-primary btn-user btn-block">
                Continuar
            </button>
        </form>
        <hr>
    </div>
  </div>
</div>

@endsection