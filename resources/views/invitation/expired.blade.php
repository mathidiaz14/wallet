@extends('layouts.auth')

@section('content')
<div class="row">
  <div class="col-lg-6 d-none d-lg-block bg-register-image"></div>
  <div class="col-lg-6">
    <div class="p-5">
        <br><br>
        <div class="text-center">
            <h1 class="h1 text-gray-900 mb-4">Lo lamentamos, pero la invitación ya expiro</h1>
            <h1 class="h4 text-gray-900 mb-4">Es necesario que te envien otra invitación para poder ingresar.</h1>
        </div>
        <hr>
        <div class="text-center">
        	<a href="{{url('home')}}" class="btn btn-primary">
	        	Ir al inicio
	        </a>
        </div>
    </div>
  </div>
</div>

@endsection