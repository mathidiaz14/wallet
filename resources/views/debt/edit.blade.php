<!-- Button trigger modal -->
  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editModal_{{$id}}">
    <i class="fa fa-edit"></i>
  </button>

  <!-- Modal -->
  <div class="modal fade" id="editModal_{{$id}}" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true" >
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="addModalLabel">Editar deuda</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body text-left">
          <form class="form-horizontal" action="{{url('debt', $debt->id)}}" method="post">
            @csrf
            @method('PATCH')
            <div class="col-sm-12">
                <div class="form-group">
                    <label>¿A quien?</label>
                    <input type="text" class="form-control" value="{{$debt->who}}" name="who" id="who" required="">
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                    <label>Monto</label>
                    <input type="number" class="form-control" value="{{$debt->amount}}" name="amount" required="">
                </div>
            </div>

            <div class="col-sm-12">
                <div class="form-group">
                    <label>Monto devuelto</label>
                    <input type="number" class="form-control" value="{{$debt->amount_returned}}" name="amount_returned" required="">
                </div>
            </div>
            <hr>
            <div class="row">
              <div class="col">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                  <i class="fa fa-chevron-left"></i>
                  Atras
                </button>
              </div>
              <div class="col text-right">
                <button class="btn btn-primary">
                  <i class="fa fa-save"></i>
                  Guardar
                </button>
              </div>  
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>