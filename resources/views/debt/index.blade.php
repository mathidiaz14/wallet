@extends('layouts.dashboard', ['menu' => 'debt'])

@section('css')
  <style>

    /* Style the tab */
    .tab {
      overflow: hidden;
      background-color: #f8f9fc;
      border-radius: 5px;
    }

    /* Style the buttons inside the tab */
    .tab button {
      background-color: inherit;
      float: left;
      border: none;
      outline: none;
      cursor: pointer;
      padding: 14px 16px;
      transition: 0.3s;
      font-size: 17px;
    }

    /* Change background color of buttons on hover */
    .tab button:hover {
      background-color: #ddd;
    }

    /* Create an active/current tablink class */
    .tab button.active {
      background-color: #4e73df;
      color:white;
    }

    /* Style the tab content */
    .tabcontent {
      display: none;
      padding: 6px 12px;
      border-top: none;
    }
  </style>
@endsection

@section('content')
<div class="row">
  <div class="col-sm">
    <div class="card shadow">
      <div class="card-header py-3">
        <div class="row">
          <div class="col">
            <h6 class="m-0 font-weight-bold text-primary">Deudas</h6>
          </div>
          <div class="col text-right">
            @include('debt.create')  
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="tab">
          <button class="tablinks active" onclick="openCity(event, 'enabled')">Pendientes</button>
          <button class="tablinks" onclick="openCity(event, 'disabled')">Pagas</button>
        </div>
        <br>
        <div id="enabled" class="tabcontent" style="display: block;">
          <div class="table table-responsive">
            <table class="table table-striped">
              <thead>
                  <tr class="text-center">
                    <th>A quien?</th>
                    <th>Monto prestado</th>
                    <th>Monto devuelto</th>
                    <th>Porcentaje devuelto</th>
                    <th>Estado</th>
                    <th>Devolver dinero</th>
                    <th>Editar</th>
                    <th>Eliminar</th>
                  </tr>
              </thead>
              <tbody>
               @foreach(Auth::user()->debts->where('status', 'enabled') as $debt)
                <tr class="text-center">
                  <td>
                    <b>{{$debt->who}}</b>      
                  </td>
                  <td>
                    <b>$ {{$debt->amount}}</b>
                  </td>
                  <td>
                    <b>$ {{$debt->amount_returned}}</b>
                  </td>
                  <td>
                    <b>{{($debt->amount_returned * 100) / $debt->amount }}%</b>
                  </td>
                  <td>
                    <b>
                      Pendiente
                    </b>
                  </td>
                  <td>
                    @include('debt.add', ['id' => $debt->id, 'amount' => $debt->amount - $debt->amount_returned])
                  </td>
                  <td>
                    @include('debt.edit', ['id' => $debt->id, 'debt' => $debt])
                  </td>
                  <td>
                      @include('helper.delete', ['id' => $debt->id, 'route' => url('debt', $debt->id)])
                  </td>
                </tr>
                @endforeach
              </tbody>
              <tfoot>
                <tr class="text-center">
                  <td>
                    <b>Total</b>
                  </td>
                  <td>
                    <b>$ {{Auth::user()->debts->where('status', 'enabled')->sum('amount')}}</b>
                  </td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>

        <div id="disabled" class="tabcontent">
          <div class="table table-responsive">
            <table class="table table-striped">
              <thead>
                  <tr class="text-center">
                    <th>A quien?</th>
                    <th>Monto prestado</th>
                    <th>Estado</th>
                    <th>Eliminar</th>
                  </tr>
              </thead>
              <tbody>
               @foreach(Auth::user()->debts->where('status', 'disabled') as $debt)
                <tr class="text-center">
                  <td>
                    <b>{{$debt->who}}</b>      
                  </td>
                  <td>
                    <b>$ {{$debt->amount}}</b>
                  </td>
                  <td>
                    <b>
                      Pága
                    </b>
                  </td>
                  <td>
                      @include('helper.delete', ['id' => $debt->id, 'route' => url('debt', $debt->id)])
                  </td>
                </tr>
                @endforeach
              </tbody>
              <tfoot>
                <tr class="text-center">
                  <td>
                    <b>Total</b>
                  </td>
                  <td>
                    <b>$ {{Auth::user()->debts->where('status', 'disabled')->sum('amount')}}</b>
                  </td>
                  <td></td>
                  <td></td>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
	<script>
    $(document).ready(function()
    {
      $('#addModal').on('shown.bs.modal', function () {
        $('#who').trigger('focus')
      })
    });

    function openCity(evt, cityName) {
      var i, tabcontent, tablinks;
      tabcontent = document.getElementsByClassName("tabcontent");
      for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
      }
      tablinks = document.getElementsByClassName("tablinks");
      for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
      }
      document.getElementById(cityName).style.display = "block";
      evt.currentTarget.className += " active";
    }
</script>
@endsection