<!-- Button trigger modal -->
  <button type="button" class="btn btn-success" data-toggle="modal" data-target="#addModal_{{$id}}">
    <i class="fa fa-plus"></i>
  </button>

  <!-- Modal -->
  <div class="modal fade" id="addModal_{{$id}}" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true" >
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="addModalLabel">Cantidad devuelta</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body text-left">
          <form class="form-horizontal" action="{{url('debt/add')}}" method="post">
            @csrf
            <input type="hidden" name="debt" value="{{$id}}">
            <input type="hidden" name="type" id="type" value="more">
            <div class="col-sm-12">
              <div class="form-group">
                <div class="input-group mb-3">
      						<div class="input-group-prepend">
      							<span class="input-group-text" id="basic-addon1_{{$id}}">
      								<i class="fa fa-plus"></i>
      							</span>
      						</div>
      						<input type="number" class="form-control" aria-describedby="basic-addon1_{{$id}}" name="number" id="number_{{$id}}" value="{{$amount}}" required style="text-align: right; font-size: 1.5em;">
      					</div>
              </div>
              <hr>
              <div class="row">
                <div class="form-group col">
                  <label for="">¿Desea crear el registro en la cuenta?</label>
                  <div class="col">
                    @include('helper.switch', ['name' => 'registry_check', 'active' => ''])
                  </div>
                </div>
                <div class="form-group col">
                  <div class="label">¿En que cuenta?</div>
                  <select name="account_id" id="account_id" class="form-control">
                    @foreach(Auth::user()->accounts as $account)
                      <option value="{{$account->id}}">{{$account->title}}</option>
                    @endforeach
                  </select>
                </div>  
              </div>
             
            </div>
            <hr>
            <div class="row">
              <div class="col">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                  <i class="fa fa-chevron-left"></i>
                  Atras
                </button>
              </div>
              <div class="col text-right">
                <button class="btn btn-primary">
                  <i class="fa fa-save"></i>
                  Guardar
                </button>
              </div>  
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
