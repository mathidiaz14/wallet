<!-- Button trigger modal -->
  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addModal">
    <i class="fa fa-plus"></i>
    Agregar deuda
  </button>

  <!-- Modal -->
  <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true" >
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="addModalLabel">Agregar deuda</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body text-left">
          <form class="form-horizontal" action="{{url('debt')}}" method="post">
            @csrf
            <div class="form-group">
                <label>¿A quien?</label>
                <input type="text" class="form-control" placeholder="¿a quien le debes?" name="who" id="who" required="">
            </div>
            <div class="form-group">
                <label>Monto</label>
                <input type="number" class="form-control" value="0" name="amount" required="">
            </div>
            
            <div class="form-group">
              <label for="">¿Desea crear el registro en la cuenta?</label>
              <div class="form-group">
                <label class="switch">
                  <input type="checkbox" name="registry_check" id="registry_check">
                  <span class="slider round"></span>
                </label>
              </div>
            </div>
            <div class="form-group">
              <div class="label">¿En que cuenta?</div>
              <select name="account_id" id="account_id" class="form-control" disabled="">
                @foreach(Auth::user()->accounts as $account)
                  <option value="{{$account->id}}">{{$account->title}}</option>
                @endforeach
              </select>
            </div>
          
            <hr>
            <div class="row">
              <div class="col">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                  <i class="fa fa-chevron-left"></i>
                  Atras
                </button>
              </div>
              <div class="col text-right">
                <button class="btn btn-primary">
                  <i class="fa fa-save"></i>
                  Guardar
                </button>
              </div>  
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <script>
    $(document).ready(function()
    {
      $('#registry_check').click(function()
      {
        $('#account_id').attr('disabled', false);
      });
    });
  </script>