@extends('layouts.dashboard', ['menu' => 'dashboard'])

@section('content')
	<!-- Cuentas -->
  <div class="row">
    @foreach(Auth::user()->accounts->where('exclude', null) as $account)
      <div class="col-xs-12 col-md-6 col-lg-3 mb-4">
        <a href="{{url('dashboard/card', $account->id)}}" style="text-decoration: none;">
          <div class="card shadow h-100 py-2" style="border-left: 0.25rem solid {{$account->color}} !important;">
            <div class="card-body" attr-account="{{$account->id}}" style="cursor: pointer;">
              <div class="row no-gutters align-items-center">
                <div class="col">
                  <div class="text font-weight-bold text-primary text-uppercase mb-1">
                    {{$account->title}}
                  </div>
                  <div class="h5 mb-0 font-weight-bold text-gray-800">
                    $ {{$account->amount}} 
                  </div>
                </div>
              </div>
            </div>
            <div class="card-footer p-1 text-center">
              <a href="{{url('account', $account->id)}}">
                <i class="fas fa-fw fa-receipt"></i>
                Ver los registros
              </a>
            </div>
          </div>
        </a>
      </div>
    @endforeach
    
    
    @if(Setting('total_account') == "on")
      <div class="col-xs-12 col-md-6 col-lg-3 mb-4">
        <a href="{{url('home')}}" style="text-decoration: none;">
          <div class="card shadow h-100 py-2" style="border-left: 0.25rem solid  !important;">
            <div class="card-body" attr-account="total" style="cursor: pointer;">
              <div class="row no-gutters align-items-center">
                <div class="col">
                  <div class="text font-weight-bold text-primary text-uppercase mb-1">
                    Total
                  </div>
                  <div class="h5 mb-0 font-weight-bold text-gray-800">
                    @php $total = 0; @endphp
                    @foreach(Auth::user()->accounts->where('exclude', null) as $account)
                      @php $total += $account->amount; @endphp
                    @endforeach
                    $ {{$total}} <small>uyu</small>
                  </div>
                </div>
              </div>
            </div>
            <div class="card-footer p-1 text-center">
              <a href="{{url('registry')}}">
                <i class="fas fa-fw fa-receipt"></i>
                Ver los registros
              </a>
            </div>
          </div>
        </a>
      </div>
    @endif
  </div>

  <div class="row">
    @foreach(Auth::user()->accounts->where('exclude', 'on') as $account)
      <div class="col-xs-12 col-md-6 col-lg-3 mb-4">
        <a href="{{url('dashboard/card', $account->id)}}" style="text-decoration: none;">
          <div class="card shadow h-100 py-2" style="border-left: 0.25rem solid {{$account->color}} !important;">
            <div class="card-body">
              <div class="row no-gutters align-items-center">
                <div class="col">
                  <div class="text font-weight-bold text-primary text-uppercase mb-1">
                    {{$account->title}}
                  </div>
                  <div class="h5 mb-0 font-weight-bold text-gray-800">
                    $ {{$account->amount}} 
                    <small style="font-size: .6em;">Excluido del total</small>
                  </div>
                </div>
              </div>
            </div>
            <div class="card-footer p-1 text-center">
              <a href="{{url('account', $account->id)}}">
                <i class="fas fa-fw fa-receipt"></i>
                Ver los registros
              </a>
            </div>
          </div>
        </a>
      </div>
    @endforeach

  </div>
  <hr>
  <div class="row">
    <div class="col-xs-12 col-md-12">
      <div class="card shadow mb-4">
        <div class="card-body">
          <form action="{{url('filter')}}" class="form-horizontal" method="post">
            @csrf
            <input type="hidden" name="account_select" value="{{$account_select}}">
            <div class="row">
              <div class="col-xs-12 col-md-6">
                <div class="form-group">
                  <label for="" class="">Desde</label>
                  <input type="date" class="form-control " name="start" value="{{$start->format('Y-m-d')}}" max="{{$end->format('Y-m-d')}}">
                </div>
              </div>
              <div class="col-xs-12 col-md-6">
                <div class="form-group">
                    <label for="" class="">Hasta</label>
                    <input type="date" class="form-control " name="end" value="{{$end->format('Y-m-d')}}" max="{{$end->format('Y-m-d')}}">
                </div>
              </div>
              <div class="col text-right">
                <button class="btn btn-info">
                  <i class="fa fa-endevor"></i>
                  Enviar
                </button>
              </div>
            </div>  
          </form>
        </div>
      </div>
    </div>

    @if(Auth::user()->dashboard->one == null)
      <div class="col-xs-12 col-md-6 mb-4">
        <div class="col dashboard_card" attr-id="one">
          <i class="fa fa-plus-circle fa-3x"></i>
          <p>Añadir tarjeta</p>
        </div>
      </div>
    @else
      <div class="col-xs-12 col-md-6 mb-4" id="one" attr-card="{{Auth::user()->dashboard->one}}">
        @include('dashboard.'.Auth::user()->dashboard->one, ['number' => 'one', 'account_select' => $account_select])
      </div>
    @endif

    
    @if(Auth::user()->dashboard->two == null)
      <div class="col-xs-12 col-md-6 mb-4">
          <div class="col dashboard_card" attr-id="two">
            <i class="fa fa-plus-circle fa-3x"></i>
            <p>Añadir tarjeta</p>
          </div>
      </div>        
    @else
      <div class="col-xs-12 col-md-6 mb-4" id="two" attr-card="{{Auth::user()->dashboard->two}}">
        @include('dashboard.'.Auth::user()->dashboard->two, ['number' => 'two', 'account_select' => $account_select])
      </div>
    @endif
        
    @if(Auth::user()->dashboard->tree == null)
      <div class="col-xs-12 col-md-6 mb-4">
        <div class="col dashboard_card" attr-id="tree">
          <i class="fa fa-plus-circle fa-3x"></i>
          <p>Añadir tarjeta</p>
        </div>
      </div>
    @else
      <div class="col-xs-12 col-md-6 mb-4" id="tree" attr-card="{{Auth::user()->dashboard->tree}}">
        @include('dashboard.'.Auth::user()->dashboard->tree, ['number' => 'tree', 'account_select' => $account_select])
      </div>
    @endif
        
    
    @if(Auth::user()->dashboard->four == null)
      <div class="col-xs-12 col-md-6 mb-4">
          <div class="col dashboard_card" attr-id="four">
            <i class="fa fa-plus-circle fa-3x"></i>
            <p>Añadir tarjeta</p>
          </div>
      </div>    
    @else
      <div class="col-xs-12 col-md-6 mb-4" id="four" attr-card="{{Auth::user()->dashboard->four}}">
        @include('dashboard.'.Auth::user()->dashboard->four, ['number' => 'four', 'account_select' => $account_select])
      </div>
    @endif
    
    @if(Auth::user()->dashboard->five == null)
      <div class="col-xs-12 col-md-6 mb-4">
          <div class="col dashboard_card" attr-id="five">
            <i class="fa fa-plus-circle fa-3x"></i>
            <p>Añadir tarjeta</p>
          </div>
      </div>    
    @else
      <div class="col-xs-12 col-md-6 mb-4" id="five" attr-card="{{Auth::user()->dashboard->five}}">
        @include('dashboard.'.Auth::user()->dashboard->five, ['number' => 'five', 'account_select' => $account_select])
      </div>
    @endif
    
    @if(Auth::user()->dashboard->six == null)
      <div class="col-xs-12 col-md-6 mb-4">
          <div class="col dashboard_card" attr-id="six">
            <i class="fa fa-plus-circle fa-3x"></i>
            <p>Añadir tarjeta</p>
          </div>
      </div>    
    @else
      <div class="col-xs-12 col-md-6 mb-4" id="six" attr-card="{{Auth::user()->dashboard->six}}">
        @include('dashboard.'.Auth::user()->dashboard->six, ['number' => 'six', 'account_select' => $account_select])
      </div>
    @endif
  
    @if(Auth::user()->dashboard->seven == null)
      <div class="col-xs-12 col-md-6 mb-4">
          <div class="col dashboard_card" attr-id="seven">
            <i class="fa fa-plus-circle fa-3x"></i>
            <p>Añadir tarjeta</p>
          </div>
      </div>    
    @else
      <div class="col-xs-12 col-md-6 mb-4" id="seven" attr-card="{{Auth::user()->dashboard->seven}}">
        @include('dashboard.'.Auth::user()->dashboard->seven, ['number' => 'seven', 'account_select' => $account_select])
      </div>
    @endif
    
    @if(Auth::user()->dashboard->eight == null)
      <div class="col-xs-12 col-md-6 mb-4">
          <div class="dashboard_card" attr-id="eight">
            <i class="fa fa-plus-circle fa-3x"></i>
            <p>Añadir tarjeta</p>
          </div>
      </div>  
    @else
      <div class="col-xs-12 col-md-6 mb-4" id="eight" attr-card="{{Auth::user()->dashboard->eight}}">
        @include('dashboard.'.Auth::user()->dashboard->eight, ['number' => 'eight', 'account_select' => $account_select])
      </div>
    @endif
  </div>
  
  <div class="modal fade" id="addCard" tabindex="-1" role="dialog" aria-labelledby="addCard" aria-hidden="true" >
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="addCard">Agregar tarjeta</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body text-left">
         <form action="{{url('dashboard/card')}}" method="post" id="form_dashboard">
          @csrf
           <input type="hidden" name="number" id="number">
           <input type="hidden" name="card" id="card">

           <div class="row">
             <div class="col">
              @if(dashboard_check('expense_structure'))
                <p attr-card="expense_structure" class="btn btn-primary btn-block btn-card">
                  Estructura de gastos
                </p>
              @else
                <p class="btn btn-primary btn-block disabled">
                  Estructura de gastos
                </p>
              @endif
            </div>
            <div class="col">
              @if(dashboard_check('income_structure'))
                <p attr-card="income_structure" class="btn btn-primary btn-block btn-card">
                  Estructura de ingresos
                </p>
              @else
                <p class="btn btn-primary btn-block disabled">
                  Estructura de ingresos
                </p>
              @endif
            </div>
            <div class="col">
              @if(dashboard_check('cash_flow'))
                <p attr-card="cash_flow" class="btn btn-primary btn-block btn-card">
                  Flujo de caja
                </p>
              @else
                <p class="btn btn-primary btn-block disabled">
                  Flujo de caja
                </p>
              @endif
            </div>
            <div class="col">
              @if(dashboard_check('last_registry'))
                <p attr-card="last_registry" class="btn btn-primary btn-block btn-card">
                  Ultimos registros
                </p>
              @else
                <p class="btn btn-primary btn-block disabled">
                  Ultimos registros
                </p>
              @endif
            </div>
           </div>
           <br>
           <div class="row">
             <div class="col">
              @if(dashboard_check('objectives'))
                <p attr-card="objectives" class="btn btn-primary btn-block btn-card">
                  Objetivos
                </p>
              @else
                <p class="btn btn-primary btn-block disabled">
                  Objetivos
                </p>
              @endif
            </div>
            <div class="col">
              @if(dashboard_check('debts'))
                <p attr-card="debts" class="btn btn-primary btn-block btn-card">
                  Deudas
                </p>
              @else
                <p class="btn btn-primary btn-block disabled">
                  Deudas
                </p>
              @endif
            </div>
            <div class="col">
              @if(dashboard_check('balance_trend'))
                <p attr-card="balance_trend" class="btn btn-primary btn-block btn-card">
                  Tendencia de saldo
                </p>
              @else
                <p class="btn btn-primary btn-block disabled">
                  Tendencia de saldo
                </p>
              @endif
            </div>
            <div class="col">
              @if(dashboard_check('period_comparison'))
                <p attr-card="period_comparison" class="btn btn-primary btn-block btn-card">
                  Comparación periodo a periodo
                </p>
              @else
                <p class="btn btn-primary btn-block disabled">
                  Comparación periodo a periodo
                </p>
              @endif
            </div>
           </div>
         </form>
        </div>
      </div>
    </div>
  </div>

@endsection

@section('js')
  <script>
    $(document).ready(function()
    {
      $('.dashboard_card').click(function()
      {
        $('#addCard').modal('toggle');
        $('#number').val($(this).attr('attr-id'));
         $('#card').val('');
      });

      $('.btn-card').click(function()
      {
        $('#card').val($(this).attr('attr-card'));
        $('#form_dashboard').submit();
      });

    });
  </script>
@endsection