<!-- Button trigger modal -->
<button type="button" class="btn btn-info" data-toggle="modal" data-target="#editModal_{{$category->id}}">
	<i class="fa fa-edit"></i>
</button>

<!-- Modal -->
<div class="modal fade" id="editModal_{{$category->id}}" tabindex="-1" role="dialog" aria-labelledby="editModalLabel_{{$category->id}}" aria-hidden="true" >
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="editModalLabel_{{$category->id}}">Editar categoria</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body text-left">
				<form class="form-horizontal" action="{{url('category', $category->id)}}" method="post">
					@csrf
					@method('PATCH')
					<div class="col-12">
						<div class="form-group">
							<label>Nombre</label>
							<input type="text" class="form-control" value="{{$category->title}}" name="title" id="title" required="">
						</div>
					</div>
					<div class="col-12">
						<div class="form-group">
							<label>Descripción</label>
							<textarea name="description" id="" class="form-control">{{$category->description}}</textarea>
						</div>
					</div>
					<div class="col-12">
		                <div class="form-group">
		                    <label>Tipo</label>
		                    <select name="type" id="" class="form-control">
		                    	@switch($category->type)
		                      		@case('more')
										<option value="more" selected>Ingreso</option>
		                      			<option value="less">Gasto</option>
		                      		@break
		                      		@case('less')
										<option value="more">Ingreso</option>
		                      			<option value="less" selected>Gasto</option>
		                      		@break
		                      	@endswitch
		                    </select>
		                </div>
		            </div>
					<div class="col-12">
						<div class="form-group">
							<label>Color</label>
							<input type="color" class="form-control" name="color" value="{{$category->color}}">
						</div>
					</div>
					<hr>
					<div class="row">
						<div class="col">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">
								<i class="fa fa-chevron-left"></i>
								Atras
							</button>
						</div>
						<div class="col text-right">
							<button class="btn btn-primary">
								<i class="fa fa-save"></i>
								Guardar
							</button>
						</div>  
					</div>
				</form>
			</div>	
		</div>
	</div>
</div>