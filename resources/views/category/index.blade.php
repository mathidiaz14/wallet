@extends('layouts.dashboard', ['menu' => 'category'])

@section('content')
<div class="row">
  	<div class="col-sm">
    	<div class="card shadow">
      		<div class="card-header py-3">
        		<div class="row">
          			<div class="col">
            			<h6 class="m-0 font-weight-bold text-primary">Categoria</h6>
          			</div>
          			<div class="col text-right">
            			@include('category.create')  
          			</div>
        		</div>
      		</div>
      		<div class="card-body">
		        <div class="table table-responsive">
		        	<table class="table table-striped">
			          	<thead>
			              	<tr class="text-center">
				                <th>Nombre</th>
				                <th>Descripción</th>
				                <th>Tipo</th>
				                <th>Color</th>
				                <th>Opciones</th>
			              	</tr>
			          	</thead>
			          	<tbody>
			              	@foreach(Auth::user()->categories as $category)
			                  	<tr class="text-center">
			                      	<td>{{$category->title}}</td>
			                      	<td>{{$category->description}}</td>
			                      	<td>
			                      		@if($category->type == "less")
			                      			Gasto
			                      		@else
											Ingreso
			                      		@endif
			                      	</td>
			                      	<td>
				                        <span style="background-color: {{$category->color}}; padding: 5px 30px; border-radius: 5px;">
				                        </span>
			                      	</td>
			                     	<td>
										<a href="{{url('category', $category->id)}}" class="btn btn-success btn-fill">
										  	<i class="fa fa-eye"></i>
										</a>
										@include('category.edit', ['id' => $category->id, 'category' => $category])
			                        	@include('helper.delete_category', ['id' => $category->id])
			                      	</td>
			                  </tr>
			              @endforeach
			          	</tbody>
			        </table>
		        </div>
      		</div>
    	</div>
  	</div>
</div>
@endsection