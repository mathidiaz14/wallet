<!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addModal">
	<i class="fa fa-plus"></i>
	Agregar Categoria
</button>

<!-- Modal -->
<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true" >
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="addModalLabel">Agregar categoria</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body text-left">
				<form class="form-horizontal" action="{{url('category')}}" method="post">
					@csrf
					<div class="col-12">
						<div class="form-group">
							<label>Nombre</label>
							<input type="text" class="form-control" placeholder="Nombre..." name="title" id="title" required="">
						</div>
					</div>
					<div class="col-12">
						<div class="form-group">
							<label>Descripción</label>
							<textarea name="description" id="" class="form-control"></textarea>
						</div>
					</div>
					<div class="col-12">
		                <div class="form-group">
		                    <label>Tipo</label>
		                    <select name="type" id="" class="form-control">
		                      <option value="more">Ingreso</option>
		                      <option value="less">Gasto</option>
		                    </select>
		                </div>
		            </div>
					<div class="col-12">
						<div class="form-group">
							<label>Color</label>
							<input type="color" class="form-control" name="color">
						</div>
					</div>
					<hr>
					<div class="row">
						<div class="col">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">
								<i class="fa fa-chevron-left"></i>
								Atras
							</button>
						</div>
						<div class="col text-right">
							<button class="btn btn-primary">
								<i class="fa fa-save"></i>
								Guardar
							</button>
						</div>  
					</div>
				</form>
			</div>	
		</div>
	</div>
</div>