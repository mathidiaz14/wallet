@extends('layouts.dashboard', ['menu' => 'category'])

@section('content')
<div class="row">
  <div class="col-sm">
    <div class="card shadow">
      <div class="card-header py-3">
        <div class="row">
          <div class="col">
            <h6 class="m-0 font-weight-bold text-primary">Subcategoria</h6>
          </div>
          <div class="col text-right">
            @include('subcategory.create', ['category' => $category])  
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="table table-responsive">
          <table class="table table-striped">
            <thead>
                <tr class="text-center">
                  <th>Nombre</th>
                  <th>Descripción</th>
                  <th>Tipo</th>
                  <th>Color</th>
                  <th>Opciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach($category->subcategories as $subcategory)
                    <tr class="text-center">
                        <td>{{$subcategory->title}}</td>
                        <td>{{$subcategory->description}}</td>
                        <td>
                            @if($subcategory->category->type == "less")
                              Gasto
                            @else
                              Ingreso
                            @endif
                        </td>
                        <td>
                            @if($subcategory->color == null)
                              <span style="background-color: {{$subcategory->category->color}}; padding: 5px 30px; border-radius: 5px;">
                              </span>
                            @else
                              <span style="background-color: {{$subcategory->color}}; padding: 5px 30px; border-radius: 5px;">
                              </span>
                            @endif
                        </td>
                        <td>
                          @include('subcategory.edit', ['id' => $subcategory->id, 'subcategory' => $subcategory])
                          @include('helper.delete_subcategory', ['id' => $subcategory->id])
                        </td>
                    </tr>
                @endforeach
            </tbody>
          </table>
        </div>
      </div>
      <div class="card-footer">
      	<a href="{{url('category')}}" class="btn btn-secondary">
			<i class="fa fa-chevron-left"></i>
			Atras
		</a>
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
  <script>
    $(document).ready(function()
    {
      $('#addModal').on('shown.bs.modal', function () {
        $('#title').trigger('focus')
      })
    });
  </script>
@endsection