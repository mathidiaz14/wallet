@extends('layouts.dashboard', ['menu' => 'root'])

@section('content')
  <div class="row">
  	<div class="col-xs-12 col-md-6 col-lg-3 mb-4">
        <a style="text-decoration: none;">
          <div class="card shadow h-100 py-2" style="border-left: 0.25rem solid #006eff !important;">
            <div class="card-body">
              <div class="row no-gutters align-items-center">
                <div class="col">
                  <div class="text font-weight-bold text-primary text-uppercase mb-1">
                    Usuarios
                  </div>
                  <div class="h5 mb-0 font-weight-bold text-gray-800">
                    {{App\Models\User::all()->count()}}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </a>
      </div>
      <div class="col-xs-12 col-md-6 col-lg-3 mb-4">
        <a style="text-decoration: none;">
          <div class="card shadow h-100 py-2" style="border-left: 0.25rem solid #006eff !important;">
            <div class="card-body">
              <div class="row no-gutters align-items-center">
                <div class="col">
                  <div class="text font-weight-bold text-primary text-uppercase mb-1">
                    Cuentas
                  </div>
                  <div class="h5 mb-0 font-weight-bold text-gray-800">
                    {{App\Account::all()->count()}}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </a>
      </div>
      <div class="col-xs-12 col-md-6 col-lg-3 mb-4">
        <a style="text-decoration: none;">
          <div class="card shadow h-100 py-2" style="border-left: 0.25rem solid #006eff !important;">
            <div class="card-body">
              <div class="row no-gutters align-items-center">
                <div class="col">
                  <div class="text font-weight-bold text-primary text-uppercase mb-1">
                    Registros
                  </div>
                  <div class="h5 mb-0 font-weight-bold text-gray-800">
                    {{App\Registry::all()->count()}}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </a>
      </div>
      <div class="col-xs-12 col-md-6 col-lg-3 mb-4">
        <a style="text-decoration: none;">
          <div class="card shadow h-100 py-2" style="border-left: 0.25rem solid #006eff !important;">
            <div class="card-body">
              <div class="row no-gutters align-items-center">
                <div class="col">
                  <div class="text font-weight-bold text-primary text-uppercase mb-1">
                    Recordatorios
                  </div>
                  <div class="h5 mb-0 font-weight-bold text-gray-800">
                    {{App\Reminder::all()->count()}}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </a>
      </div>
  </div>
	<div class="row">
		<div class="col-xs-12 col-md-6">
		  	<div class="card shadow mb-4">
			   <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
			    <h6 class="m-0 font-weight-bold text-primary">Usuario en los ultimos 6 meses</h6>
			    <div class="dropdown no-arrow">
			      <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			        <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
			      </a>
			    </div>
			  </div>
			  <div class="card-body">
			      <div class="row">
			        <canvas id="balanceTrend"></canvas>
			      </div>
			      
			      <script>      
			        var configBalanceTrend = {
			          type: 'line',
			          data: {
			            labels: [
			              	@for($i=5; $i>=0; $i--)
			              		"{{\Carbon\Carbon::now()->subMonth($i)->format('M')}}", 
			              	@endfor
			              ],
			            datasets: [{
			              label: 'Usuarios',
			              borderColor: "#0092db",
			              backgroundColor: "#0092dbaa",
			              data: [
			              	@for($i=5; $i>=0; $i--)
			              		{{App\Models\User::whereBetween('created_at', [\Carbon\Carbon::now()->subMonth($i)->firstOfMonth(), \Carbon\Carbon::now()->subMonth($i)->lastOfMonth()])->count()}}, 
			              	@endfor
			              ],
			            }]
			          },
			          options: {
			            responsive: true,
			            tooltips: {
			              mode: 'index',
			            },
			            hover: {
			              mode: 'index'
			            },
			            scales: {
			              xAxes: [{
			                scaleLabel: {
			                  display: true,
			                  labelString: 'Mes'
			                }
			              }],
			              yAxes: [{
			                stacked: true,
			                scaleLabel: {
			                  display: true,
			                  labelString: 'Usuarios'
			                }
			              }]
			            }
			          }
			        };

			        $(document).ready(function()
			        {
			          var balanceTrend = document.getElementById('balanceTrend').getContext('2d');
			          window.myLine = new Chart(balanceTrend, configBalanceTrend);
			        });
			      </script>
			  </div>
		  </div>
		</div>

		<div class="col-xs-12 col-md-6">
		  	<div class="card shadow mb-4">
			   <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
			    <h6 class="m-0 font-weight-bold text-primary">Registros en los ultimos 6 meses</h6>
			    <div class="dropdown no-arrow">
			      <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			        <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
			      </a>
			    </div>
			  </div>
			  <div class="card-body">
			      <div class="row">
			        <canvas id="registry"></canvas>
			      </div>
			      
			      <script>      
			        var configregistry = {
			          type: 'line',
			          data: {
			            labels: [
			              	@for($i=5; $i>=0; $i--)
			              		"{{\Carbon\Carbon::now()->subMonth($i)->format('M')}}", 
			              	@endfor
			              ],
			            datasets: [{
			              label: 'Registros',
			              borderColor: "#2dd31b",
			              backgroundColor: "#2dd31baa",
			              data: [
			               	@for($i=5; $i>=0; $i--)
			              		{{App\Registry::whereBetween('created_at', [\Carbon\Carbon::now()->subMonth($i)->firstOfMonth(), \Carbon\Carbon::now()->subMonth($i)->lastOfMonth()])->count()}}, 
			              	@endfor
			              ],
			            }]
			          },
			          options: {
			            responsive: true,
			            tooltips: {
			              mode: 'index',
			            },
			            hover: {
			              mode: 'index'
			            },
			            scales: {
			              xAxes: [{
			                scaleLabel: {
			                  display: true,
			                  labelString: 'Mes'
			                }
			              }],
			              yAxes: [{
			                stacked: true,
			                scaleLabel: {
			                  display: true,
			                  labelString: 'Registros'
			                }
			              }]
			            }
			          }
			        };

			        $(document).ready(function()
			        {
			          var registry = document.getElementById('registry').getContext('2d');
			          window.myLine = new Chart(registry, configregistry);
			        });
			      </script>
			  </div>
		  </div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
		  	<div class="card shadow mb-4">
			   	<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
			    	<h6 class="m-0 font-weight-bold text-primary">Usuarios</h6>
			  	</div>
				<div class="card-body">
					<div class="row">
						<div class="table table-responsive">
							<table class="table table-striped">
								<tr>
									<th>Nombre</th>
									<th>Email</th>
									<th>Ultimo inicio</th>
									<th>Registros</th>
									<th>Cambiar contraseña</th>
									<th>Eliminar</th>
								</tr>
								@foreach($users as $user)
									<tr>
										<td>{{$user->name}}</td>
										<td>{{$user->email}}</td>
										<td>
											@if($user->last_login == null)
												NULL
											@else
												{{$user->last_login->format('d/m/Y H:i')}}
											@endif
										</td>
										<td>
											{{$user->registries->count()}}
										</td>
										<td>
											<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#changePassword{{$user->id}}">
												<i class="fa fa-key"></i>
											</button>

											<!-- Modal -->
											<div class="modal fade" id="changePassword{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true" >
											<div class="modal-dialog" role="document">
											  <div class="modal-content">
											    <div class="modal-header">
											      <h5 class="modal-title" id="addModalLabel">Cambiar contraseña</h5>
											      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
											        <span aria-hidden="true">&times;</span>
											      </button>
											    </div>
											    <div class="modal-body text-left">
											      <form class="form-horizontal" action="{{url('root/password', $user->id)}}" method="post">
											        @csrf
											        <div class="row">
											        	<div class="col-sm-10">
												            <div class="form-group">
												                <input type="text" class="form-control" placeholder="Nueva contraseña..." name="password" id="password" required="">
												            </div>
												        </div>
												        <div class="col-sm-2">
												        	<a class="btn btn-info btn-block btn-pass">
												        		<i class="fa fa-dice"></i>
												        	</a>
												        </div>
											        </div>
											        <hr>
											        <div class="row">
											          <div class="col">
											            <button type="button" class="btn btn-secondary" data-dismiss="modal">
											              <i class="fa fa-chevron-left"></i>
											              Atras
											            </button>
											          </div>
											          <div class="col text-right">
											            <button class="btn btn-primary">
											              <i class="fa fa-save"></i>
											              Cambiar
											            </button>
											          </div>  
											        </div>
											      </form>
											    </div>
											  </div>
											</div>
											</div>
										</td>
										<td>
											@include('helper.delete_user_root', ['id' => $user->id, 'route' => url('root/user/destroy', $user->id)])
										</td>		
									</tr>
								@endforeach
							</table>
						</div>
						<div class="col-sm-12 text-center">
							{{ $users->links() }}
						</div>
					</div>
				</div>
		  	</div>
		</div>
	</div>
@endsection

@section('js')
	<script>
		$(document).ready(function()
		{
			$('.btn-pass').click(function()
			{
				$('#password').val(Math.random().toString(36).substring(2));
			});
		});
	</script>
@endsection