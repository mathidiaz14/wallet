<!-- Button trigger modal -->
<button type="button" class="btn btn-success" data-toggle="modal" data-target="#editReminderModal_{{$reminder->id}}">
	<i class="fa fa-check"></i>				
</button>

  <!-- Modal -->
<div class="modal fade" id="editReminderModal_{{$reminder->id}}" tabindex="-1" role="dialog" aria-labelledby="editreminderModalLabel_{{$reminder->id}}" aria-hidden="true" >
    <div class="modal-dialog" role="document">
      	<div class="modal-content">
	        <div class="modal-header">
	          	<h5 class="modal-title">
	          		Cerrar recordatorio
	          	</h5>
	         	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	            	<span aria-hidden="true">&times;</span>
	          	</button>
	        </div>
	        <div class="modal-body text-left">
        	  	<form class="form-horizontal" action="{{url('reminder/check', $reminder->id)}}" method="post">
	            	@csrf
		            <div class="row">
			            <div class="col-6">
							<label>Monto</label>
							<div class="input-group mb-3">
								<div class="input-group-prepend">
									<span class="input-group-text" id="basic-addon1">
										<i class="fa fa-minus"></i>
									</span>
								</div>
								<input type="number" class="form-control" aria-describedby="basic-addon1" name="amount" id="amount_reminder" value="{{$reminder->amount}}" required>
							</div>
			            </div>
		            	<div class="col-6">
			                <div class="form-group">
			                    <label>Cuenta</label>
			                    <select name="account" id="" class="form-control">
			                      	@foreach(Auth::user()->accounts as $account)
										@if($account->id == $reminder->account_id)
											<option value="{{$account->id}}" selected="">{{$account->title}}</option>
										@else
											<option value="{{$account->id}}">{{$account->title}}</option>
										@endif
			                      	@endforeach
			                    </select>
			                </div>
			            </div>
			            <div class="col-12">
			            	<hr>
			            </div>
			            <div class="col-6">
			                <div class="form-group">
			                    <label>Categoria</label>
			                    <select name="category" id="" class="form-control">
									<option value="null">Sin categoria</option>
			                    	@foreach(Auth::user()->categories as $category)
										<optgroup label="{{$category->title}}">
											@foreach($category->subcategories as $subcategory)
												@if($subcategory->id == $reminder->subcategory_id)
													<option value="{{$subcategory->id}}" selected="">{{$subcategory->title}}</option>
												@else
													<option value="{{$subcategory->id}}">{{$subcategory->title}}</option>
												@endif
											@endforeach
										</optgroup>
			                    	@endforeach
			                    </select>
			                </div>
			            </div>
			            <div class="col-6">
			                <div class="form-group">
			                    <label>Forma de pago</label>
			                    <select name="way" id="" class="form-control">
									<option value="debt">Debito</option>
									<option value="cash">Efectivo</option>
									<option value="credit">Credito</option>
									<option value="check">Cheque</option>
			                    </select>
			                </div>
			            </div>
			            <div class="col-12">
			            	<hr>
			            </div>
			            <div class="col-12">
			                <div class="form-group">
			                    <label>Observación</label>
			                    <textarea name="description" id="" class="form-control">{{$reminder->description}}</textarea>
			                </div>
			            </div>
			             <div class="col-6">
			                <div class="form-group">
			                    <label>Fecha</label>
			                    <input type="date" name="date" id="date" value="{{Carbon\Carbon::now()->format('Y-m-d')}}" class="form-control">
			                </div>
			            </div>
			            <div class="col-6">
			                <div class="form-group">
			                    <label>Hora</label>
			                    <input type="time" name="hour" id="hour" value="{{Carbon\Carbon::now()->format('H:i')}}" class="form-control">
			                </div>
			            </div>
		            </div>
		            <hr>
		            <div class="row">
		              	<div class="col">
			                <button type="button" class="btn btn-secondary" data-dismiss="modal">
								<i class="fa fa-times"></i>
								Cerrar
			                </button>
		              	</div>
		              	<div class="col text-right">
			                <button class="btn btn-primary">
								<i class="fa fa-save"></i>
								Guardar
			                </button>
		              	</div>  
		            </div>
	          	</form>
	        </div>
      </div>
    </div>
</div>

<script>	
    $(document).ready(function()
    {
		$('#editReminderModal_{{$reminder->id}}').on('shown.bs.modal', function () {
			$('#amount_reminder').trigger('focus')
		});
    });
</script>