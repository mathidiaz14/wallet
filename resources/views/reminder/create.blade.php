<!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addModal">
	<i class="fa fa-plus"></i>			
	Agregar recordatorio
</button>

  <!-- Modal -->
<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addLabel" aria-hidden="true" >
    <div class="modal-dialog" role="document">
      	<div class="modal-content">
	        <div class="modal-header">
	          	<h5 class="modal-title">
	          		Agregar recordatorio
	          	</h5>
	         	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	            	<span aria-hidden="true">&times;</span>
	          	</button>
	        </div>
	        <div class="modal-body text-left">
	        	@if(Auth::user()->accounts->count() > 0)
		          	<form class="form-horizontal" action="{{url('reminder')}}" method="post">
		            @csrf
			            <div class="row">
				            <div class="col-6">
								<label>Monto</label>
								<div class="input-group mb-3">
									<div class="input-group-prepend">
										<span class="input-group-text" id="basic-addon1">
											<i class="fa fa-minus"></i>
										</span>
									</div>
									<input type="number" class="form-control" aria-describedby="basic-addon1" name="amount" id="amount_registry_reminder" required>
								</div>
				            </div>
			            	<div class="col-6">
				                <div class="form-group">
				                    <label>Cuenta</label>
				                    <select name="account" id="account_reminder" class="form-control">
				                      	@foreach(Auth::user()->accounts as $account)
											<option value="{{$account->id}}">{{$account->title}}</option>
				                      	@endforeach
				                    </select>
				                </div>
				            </div>
				            <div class="col-12">
				            	<hr>
				            </div>
				            <div class="col-6">
				                <div class="form-group">
				                    <label>Categoria</label>
				                    <input type="hidden" name="subcategory" id="subcategory_reminder">
									<br>
									<button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#categoryModalReminder" id="category_select_reminder">
										<i class="fa fa-external-link-alt"></i>
										Seleccionar Categoria
									</button>
				                </div>
				            </div>
				            <div class="col-6">
				                <div class="form-group">
				                    <label>Fecha del recordatorio</label>
				                    <input type="date" name="reminder_date" id="date" value="" min="{{Carbon\Carbon::now()->format('Y-m-d')}}" class="form-control" required="">
				                </div>
				            </div>
				            <div class="col-6">
				                <div class="form-group">
				                    <label>Repetir recordatorio</label>
				                    <select name="reminder_type" id="" class="form-control">
				                    	<option value="">No repetir</option>
				                    	<option value="day">Diario</option>
				                    	<option value="week">Semanal</option>
				                    	<option value="month">Mensual</option>
				                    	<option value="year">Anual</option>
				                    	<optgroup label="Primer dia del mes">
											<option value="first_monday">Lunes</option>
											<option value="first_thuesday">Martes</option>
											<option value="first_wednesday">Miercoles</option>
											<option value="first_thursday">Jueves</option>
											<option value="first_friday">Viernes</option>
											<option value="first_saturday">Sabado</option>
											<option value="first_sunday">Domingo</option>
				                    	</optgroup>
				                    </select>
				                </div>
				            </div>
				            <div class="col-6">
				                <div class="form-group">
				                    <label>Repetir Hasta</label>
				                    <input type="date" name="reminder_until" id="" value="" min="{{Carbon\Carbon::now()->format('Y-m-d')}}" class="form-control">
				                </div>
				            </div>
				            <div class="col-12">
				                <div class="form-group">
				                    <label>Observación</label>
				                    <textarea name="description" id="description_reminder" class="form-control"></textarea>
				                </div>
				            </div>
			            </div>
			            <hr>
			            <div class="row">
			              	<div class="col">
				                <button type="button" class="btn btn-secondary" data-dismiss="modal">
									<i class="fa fa-times"></i>
									Cerrar
				                </button>
			              	</div>
			              	<div class="col text-right">
				                <button class="btn btn-primary">
									<i class="fa fa-save"></i>
									Guardar
				                </button>
			              	</div>  
			            </div>
		          	</form>
	            @else
		            <div class="col text-center">
		            	<h4 style="color:red;">No tiene ninguna cuenta, <a href="{{url('account')}}">ingrese aqui</a> para crear una.</h4>
		            </div>
	            @endif
	        </div>
      </div>
    </div>
</div>


 <!-- Modal -->
<div class="modal fade" id="categoryModalReminder" tabindex="-1" role="dialog" aria-labelledby="categoryModalReminderLabel" aria-hidden="true" >
    <div class="modal-dialog" role="document">
      	<div class="modal-content">
        	<div class="modal-header">
          		<h5 class="modal-title">Categoria</h5>
	         	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	            	<span aria-hidden="true">&times;</span>
	          	</button>
        	</div>
        	<div class="modal-body ">
	          	<div class="row row_category_reminder">
	          		@foreach(Auth::user()->categories as $category)
		              	<div class="col-6">
		              		<a class="btn btn_category_reminder btn-block" attr-id="{{$category->id}}" style="background-color: {{$category->color}}; color:white; margin-bottom: 10px; cursor:pointer;">
			              		{{$category->title}}
			              	</a>
		              	</div>
		          	@endforeach
	          	</div>

	          	@foreach(Auth::user()->categories as $category)
					<div class="row row_subcategory_reminder" id="category_reminder_{{$category->id}}" style="display: none;">
		              	@foreach($category->subcategories as $subcategory)
							<div class="col-6">
								<a class="btn btn_subcategory_reminder btn-block" 
								attr-color="@if($subcategory->color == null){{$category->color}}@else{{$subcategory->color}}@endif" 
								attr-name="{{$subcategory->title}}"
								attr-id="{{$subcategory->id}}" 
								attr-type="{{$subcategory->category->type}}"
								style="background-color:@if($subcategory->color == null){{$category->color}}@else{{$subcategory->color}}@endif; color:white;  margin-bottom: 10px; cursor:pointer;">
				              		{{$subcategory->title}}
				              	</a>	
							</div>
		              	@endforeach

		              	<div class="col-6">
		              		<a class="btn btn-block btn-secondary btn_back_reminder" style= "margin-bottom: 10px; cursor:pointer; color:white;">
		              			<i class="fa fa-chevron-left"></i>
		              			Atras
		              		</a>
		              	</div> 
	              	</div>
	          	@endforeach

          		<hr>
	            <div class="row">
	              	<div class="col">
		                <button type="button" class="btn btn-secondary" data-dismiss="modal">
							<i class="fa fa-times"></i>
							Cerrar
		                </button>
	              	</div>
	         	</div>
        	</div>
      	</div>
    </div>
</div>

<script>	
    $(document).ready(function()
    {
		$('#addModal').on('shown.bs.modal', function () 
		{
			//Normalizo los campos antes de abrir, para cuando cierro y abro el modal
			$('#amount_registry_reminder').val("");
			$('#description_reminder').val("");
			$("#account_reminder").val($("#account_reminder option:first").val());
			$('#category_select_reminder').css('background-color' , '#2E59D9');
			$('#category_select_reminder').html('<i class="fa fa-external-link-alt"></i> Seleccionar Categoria');
			$('#subcategory_reminder').val(null);
			$('#amount_registry_reminder').trigger('focus');
		});

		$('.btn_subcategory_reminder').click(function()
		{
			$('#categoryModalReminder').modal('hide');
			$('#category_select_reminder').removeClass('btn-primary');
			$('#category_select_reminder').css('background-color' , $(this).attr('attr-color'));
			$('#category_select_reminder').css('color' , 'white');
			$('#category_select_reminder').html($(this).attr('attr-name'));
			$('#subcategory_reminder').val($(this).attr('attr-id'));

			if ($(this).attr('attr-type') == 'less') 
				$('#basic-addon1').html('<i class="fa fa-minus"></i>');
			else
				$('#basic-addon1').html('<i class="fa fa-plus"></i>');

		});

		$('.btn_category_reminder').click(function()
		{
			$('.row_subcategory_reminder').hide();
			$('.row_category_reminder').hide();
			$('#category_reminder_'+$(this).attr('attr-id')).show();
		});

		$('.btn_back_reminder').click(function()
		{
			$('.row_subcategory_reminder').hide();
			$('.row_category_reminder').show();
		});
    });
</script>