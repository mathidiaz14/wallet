<!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editModal_{{$reminder->id}}">	
	<i class="fa fa-edit"></i>			
</button>

  <!-- Modal -->
<div class="modal fade" id="editModal_{{$reminder->id}}" tabindex="-1" role="dialog" aria-labelledby="addLabel" aria-hidden="true" >
    <div class="modal-dialog" role="document">
      	<div class="modal-content">
	        <div class="modal-header">
	          	<h5 class="modal-title">
	          		Editar recordatorio
	          	</h5>
	         	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	            	<span aria-hidden="true">&times;</span>
	          	</button>
	        </div>
	        <div class="modal-body text-left">
	          	<form class="form-horizontal" action="{{url('reminder', $reminder->id)}}" method="post">
	            @csrf
	            @method('PUT')
		            <div class="row">
			            <div class="col-6">
							<label>Monto</label>
							<div class="input-group mb-3">
								<div class="input-group-prepend">
									<span class="input-group-text" id="basic-addon1">
										<i class="fa fa-minus"></i>
									</span>
								</div>
								<input type="number" class="form-control" aria-describedby="basic-addon1" name="amount" id="amount_registry_reminder" value="{{$reminder->amount}}" required>
							</div>
			            </div>
		            	<div class="col-6">
			                <div class="form-group">
			                    <label>Cuenta</label>
			                    <select name="account" id="account_reminder" class="form-control">
			                      	@foreach(Auth::user()->accounts as $account)
										@if($account->id == $reminder->account_id)
											<option value="{{$account->id}}" selected>{{$account->title}}</option>
										@else
											<option value="{{$account->id}}">{{$account->title}}</option>
										@endif
			                      	@endforeach
			                    </select>
			                </div>
			            </div>
			            <div class="col-12">
			            	<hr>
			            </div>
			            <div class="col-6">
			                <div class="form-group">
			                    <label>Categoria</label>
								<select name="subcategory" id="subcategory_reminder" class="form-control">
									<option value="">Ninguna</option>
									@foreach(Auth::user()->categories as $category)
										<optgroup label="{{$category->title}}">
							              	@foreach($category->subcategories as $subcategory)
												@if($subcategory->id == $reminder->subcategory_id)
													<option value="{{$subcategory->id}}" selected="">{{$subcategory->title}}</option>
												@else
													<option value="{{$subcategory->id}}">{{$subcategory->title}}</option>
												@endif
							              	@endforeach
						              	</optgroup>
						          	@endforeach
								</select>
			                </div>
			            </div>
			            <div class="col-6">
			                <div class="form-group">
			                    <label>Fecha del recordatorio</label>
			                    <input type="date" name="reminder_date" id="date" min="{{Carbon\Carbon::now()->format('Y-m-d')}}" value="{{$reminder->reminder_date->format('Y-m-d')}}" class="form-control" required="">
			                </div>
			            </div>
			            <div class="col-6">
			                <div class="form-group">
			                    <label>Repetir recordatorio</label>
			                    <select name="reminder_type" id="" class="form-control">
			                    	
			                    	@if($reminder->reminder_type == null)
			                    		<option value="" selected="">No repetir</option>
			                    	@else
			                    		<option value="">No repetir</option>
			                    	@endif

			                    	@if($reminder->reminder_type == 'day')
			                    		<option value="day" selected>Diario</option>
			                    	@else
			                    		<option value="day">Diario</option>
			                    	@endif

			                    	@if($reminder->reminder_type == 'week')
			                    		<option value="week" selected>Semanal</option>
			                    	@else
			                    		<option value="week">Semanal</option>
			                    	@endif

			                    	@if($reminder->reminder_type == 'month')
			                    		<option value="month" selected>Mensual</option>
			                    	@else
			                    		<option value="month">Mensual</option>
			                    	@endif

			                    	@if($reminder->reminder_type == 'year')
			                    		<option value="year" selected>Anual</option>
			                    	@else
			                    		<option value="year">Anual</option>
			                    	@endif

			                    	<optgroup label="Primer dia del mes">

				                    	@if($reminder->reminder_type == 'first_monday')
				                    		<option value="first_monday" selected="">Lunes</option>
				                    	@else
				                    		<option value="first_monday">Lunes</option>
				                    	@endif

				                    	@if($reminder->reminder_type == 'first_thuesday')
				                    		<option value="first_thuesday" selected="">Martes</option>
				                    	@else
				                    		<option value="first_thuesday">Martes</option>
				                    	@endif

				                    	@if($reminder->reminder_type == 'first_wednesday')
				                    		<option value="first_wednesday" selected="">Miercoles</option>
				                    	@else
				                    		<option value="first_wednesday">Miercoles</option>
				                    	@endif

				                    	@if($reminder->reminder_type == 'first_thursday')
				                    		<option value="first_thursday" selected="">Jueves</option>
				                    	@else
				                    		<option value="first_thursday">Jueves</option>
				                    	@endif

				                    	@if($reminder->reminder_type == 'first_friday')
				                    		<option value="first_friday" selected="">Viernes</option>
				                    	@else
				                    		<option value="first_friday">Viernes</option>
				                    	@endif

				                    	@if($reminder->reminder_type == 'first_saturday')
				                    		<option value="first_saturday" selected="">Sabado</option>
				                    	@else
				                    		<option value="first_saturday">Sabado</option>
				                    	@endif

				                    	@if($reminder->reminder_type == 'first_sunday')
				                    		<option value="first_sunday" selected="">Domingo</option>
				                    	@else
				                    		<option value="first_sunday">Domingo</option>
				                    	@endif
			                    	</optgroup>

			                    </select>
			                </div>
			            </div>
			            <div class="col-6">
			                <div class="form-group">
			                    <label>Repetir Hasta</label>
			                    <input type="date" name="reminder_until" id="" value="@if($reminder->reminder_until != null) {{$reminder->reminder_until->format('Y-m-d')}} @endif" min="{{Carbon\Carbon::now()->format('Y-m-d')}}" class="form-control">
			                </div>
			            </div>
			            <div class="col-12">
			                <div class="form-group">
			                    <label>Observación</label>
			                    <textarea name="description" id="description_reminder" class="form-control">{{$reminder->observation}}</textarea>
			                </div>
			            </div>
		            </div>
		            <hr>
		            <div class="row">
		              	<div class="col">
			                <button type="button" class="btn btn-secondary" data-dismiss="modal">
								<i class="fa fa-times"></i>
								Cerrar
			                </button>
		              	</div>
		              	<div class="col text-right">
			                <button class="btn btn-primary">
								<i class="fa fa-save"></i>
								Guardar
			                </button>
		              	</div>  
		            </div>
	          	</form>
	        </div>
      </div>
    </div>
</div>

<script>	
    $(document).ready(function()
    {
		$('#editModal_{{$reminder->id}}').on('shown.bs.modal', function () 
		{
			//Normalizo los campos antes de abrir, para cuando cierro y abro el modal
			$('#amount_registry_reminder').val("");
			$('#description_reminder').val("");
			$("#account_reminder").val($("#account_reminder option:first").val());
			$('#category_select_reminder').css('background-color' , '#2E59D9');
			$('#category_select_reminder').html('<i class="fa fa-external-link-alt"></i> Seleccionar Categoria');
			$('#subcategory_reminder').val(null);
			$('#amount_registry_reminder').trigger('focus');
		});

		$('.btn_subcategory_reminder').click(function()
		{
			$('#categoryModalReminder').modal('hide');
			$('#subcategory_reminder').val($(this).attr('attr-id'));

			if ($(this).attr('attr-type') == 'less') 
				$('#basic-addon1').html('<i class="fa fa-minus"></i>');
			else
				$('#basic-addon1').html('<i class="fa fa-plus"></i>');

		});

		$('.btn_category_reminder').click(function()
		{
			$('.row_subcategory_reminder').hide();
			$('.row_category_reminder').hide();
			$('#category_reminder_'+$(this).attr('attr-id')).show();
		});

		$('.btn_back_reminder').click(function()
		{
			$('.row_subcategory_reminder').hide();
			$('.row_category_reminder').show();
		});
    });
</script>