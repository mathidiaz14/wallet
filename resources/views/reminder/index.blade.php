@extends('layouts.dashboard', ['menu' => 'reminder'])

@section('content')
<div class="row">
  <div class="col-sm">
    <div class="card shadow">
      <div class="card-header py-3">
        <div class="row">
          <div class="col">
            <h6 class="m-0 font-weight-bold text-primary">Recordatorios</h6>
          </div>
          <div class="col text-right">
            @include('reminder.create')  
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="table table-responsive">
          <table class="table table-striped">
            <tbody>
              <tr class="text-center">
                <th>Monto</th>
                <th>Categoria</th>
                <th>Cuenta</th>
                <th>Fecha recordatorio</th>
                <th>Tipo recordatorio</th>
                <th>Hasta cuando?</th>
                <th>Estado</th>
                <th></th>
                <th></th>
                <th></th>
              </tr>
              @foreach(Auth::user()->reminders as $reminder)
                <tr class="text-center">
                  <td>
                    @if($reminder->type == "less")
                      <i class="fa fa-minus" style="color:red;"></i>
                    @else
                      <i class="fa fa-plus" style="color:green;"></i>
                    @endif
                    <b>{{$reminder->amount}}</b>
                  </td>
                  <td>
                    @if($reminder->subcategory == null)
                      Sin categoria
                    @else
                      <span style="background-color:
                      @if($reminder->subcategory->color == null)
                      {{$reminder->subcategory->category->color}}
                      @else
                      {{$reminder->subcategory->color}} 
                      @endif; 
                      color:white;
                      padding: 5px 30px; 
                      border-radius: 5px;">
                        {{$reminder->subcategory->title}}
                      </span>
                    @endif
                  </td>
                  <td>
                    <a href="{{url('account', $reminder->account->id)}}" style="
                      background-color: {{$reminder->account->color}}; 
                      color:white;
                      padding: 5px 30px; 
                      border-radius: 5px;">
                      {{$reminder->account->title}}
                    </a>
                  </td>
                  <td>
					{{$reminder->reminder_date->format('d/m/Y')}}
                  </td>
				  <td>
				  	@if($reminder->reminder_type != null) 
				  		@if($reminder->reminder_type == "day")
	                        Por dia
	                    @elseif($reminder->reminder_type == "week")                        
	                        Por semana
	                    @elseif($reminder->reminder_type == "month")
	                        Por mes
	                    @elseif($reminder->reminder_type == "year")
	                        Por año
	                    @elseif($reminder->reminder_type == "first_monday")                    
	                        Primer lunes
	                    @elseif($reminder->reminder_type == "first_thuesday")                    
	                        Primer martes
	                    @elseif($reminder->reminder_type == "first_wednesday")                    
	                        Primer miercoels
	                    @elseif($reminder->reminder_type == "first_thursday")                    
	                        Primer jueves
	                    @elseif($reminder->reminder_type == "first_friday")                    
	                        Primer viernes
	                    @elseif($reminder->reminder_type == "first_saturday")                    
	                        Primer sabado
	                    @elseif($reminder->reminder_type == "first_sunday")                    
	                        Primer domingo
	                    @endif
				  	@endif
				  </td>
                  <td>
                  	@if($reminder->reminder_until != null)
						{{$reminder->reminder_until->format('d/m/Y')}}
                  	@else
                  		Siempre
                  	@endif
                  </td>
                  <td>
                  	@if($reminder->status == "disabled")
                  		<span style="
	                      background-color: red; 
	                      color:white;
	                      padding: 5px 30px; 
	                      border-radius: 5px;">
	                      Vencido
	                    </span>
                  	@else
                  		Esperando
                  	@endif
                  </td>
                  <td>
                    @include('reminder.check', ['reminder' => $reminder])
                  </td>
                  <td>  
                    @include('reminder.edit', ['reminder' => $reminder])
                  </td>
                  <td>
                    @include('helper.delete', ['id' => $reminder->id, 'route' => url('reminder', $reminder->id)])
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>  
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
  
@endsection