@extends('layouts.dashboard', ['menu' => 'setting'])

@section('content')
<div class="row">
  <div class="col-sm">
    <div class="card shadow">
      <div class="card-header py-3">
        <div class="row">
          <div class="col">
            <h6 class="m-0 font-weight-bold text-primary">Configuración</h6>
          </div>
          <div class="col text-right">
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="col-xs-12 col-md-10 offset-md-1 col-lg-8 offset-lg-2">
          @include('helper.alert')
          <form action="{{url('setting')}}" method="POST" class="form-horizontal">
            @csrf
            <div class="form-group">
              <label for="">Cuenta predeterminada</label>
              <select name="default_account" id="" class="form-control">
                
                @if(Auth::user()->setting->default_account == null)
                  <option value="null" selected>Ninguna</option>
                @else
                  <option value="null">Ninguna</option>
                @endif
                
                @foreach(Auth::user()->accounts as $account)
                  @if(Auth::user()->setting->default_account == $account->id) 
                    <option value="{{$account->id}}" selected="">{{$account->title}}</option>
                  @else
                    <option value="{{$account->id}}">{{$account->title}}</option>
                  @endif
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <label for="">¿Desea ver la suma total de las cuentas?</label>
              <div class="col">
                @include('helper.switch', ['name' => 'total_account', 'active' => Auth::user()->setting->total_account])
              </div>
            </div>
            <div class="form-group">
              <label for="">¿Desea que se recuerde la categoria del ultimo registro?</label>
              <div class="col">
                @include('helper.switch', ['name' => 'remember_subcategory', 'active' => Auth::user()->setting->remember_subcategory])
              </div>
            </div>
            <div class="form-group text-right">
              <button class="btn btn-primary">
                <i class="fa fa-save"></i>
                Guardar
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
  
@endsection