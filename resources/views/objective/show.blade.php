@extends('layouts.dashboard', ['menu' => 'objective'])

@section('content')
<div class="row">
  <div class="col-sm">
    <div class="card shadow">
      <div class="card-header py-3">
        <div class="row">
          <div class="col">
            <h6 class="m-0 font-weight-bold text-primary">{{$objective->title}}</h6>
          </div>
          <div class="col text-right">
            @include('objective.create')  
          </div>
        </div>
      </div>
      <div class="card-body">
        <table class="table table-striped">
          <thead>
              <tr class="text-center">
                <th>Nombre</th>
                <th>Descripción</th>
                <th>Progreso</th>
                <th>Color</th>
                <th></th>
                <th></th>
                <th></th>
              </tr>
          </thead>
          <tbody>
              @foreach(Auth::user()->objectives as $objective)
                  <tr class="text-center">
                      <td>{{$objective->title}}</td>
                      <td>{{$objective->description}}</td>
                      <td>
                      	<b>{{($objective->amount_initial * 100) / $objective->amount_final }}%</b>
                      </td>
                      <td>
                        <span style="background-color: {{$objective->color}}; padding: 5px 30px; border-radius: 5px;">
                        </span>
                      </td>
                      <td>
                        <a href="{{url('objective', $objective->id)}}" class="btn btn-success btn-fill">
                            <i class="fa fa-eye"></i>
                        </a>
                      </td>
                      <td>
                        @include('objective.edit', ['id' => $objective->id, 'objective' => $objective])
                      </td>
                      <td>
                          @include('helper.delete', ['id' => $objective->id, 'route' => url('objective', $objective->id)])
                      </td>
                  </tr>
              @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
  
@endsection