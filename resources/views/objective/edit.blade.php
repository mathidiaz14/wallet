<!-- Button trigger modal -->
  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editModal_{{$objective->id}}">
    <i class="fa fa-edit"></i>
  </button>

  <!-- Modal -->
  <div class="modal fade" id="editModal_{{$objective->id}}" tabindex="-1" role="dialog" aria-labelledby="editModal_{{$objective->id}}Label" aria-hidden="true" >
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="editModal_{{$objective->id}}Label">Editar objetivo</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body text-left">
          <form class="form-horizontal" action="{{url('objective', $objective->id)}}" method="post">
            @csrf
            @method('PATCH')
            <div class="col-sm-12">
                <div class="form-group">
                    <label>Titulo</label>
                    <input type="text" class="form-control" value="{{$objective->title}}" name="title" id="title" required="">
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                    <label>Monto inicial</label>
                    <input type="number" class="form-control" value="{{$objective->amount}}" name="amount" required="">
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                    <label>Objetivo</label>
                    <input type="number" class="form-control" value="{{$objective->objective}}" name="objective" required="">
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                    <label>Color</label>
                    <input type="color" class="form-control" name="color" value="{{$objective->color}}">
                </div>
            </div>
             <div class="col-sm-12">
                <div class="form-group">
                    <label>Fecha objetivo</label>
                    <input type="date" class="form-control" name="date" value="@if($objective->date != null){{$objective->date->format('Y-m-d')}} @endif">
                </div>
            </div>
            <hr>
            <div class="row">
              <div class="col">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                  <i class="fa fa-chevron-left"></i>
                  Atras
                </button>
              </div>
              <div class="col text-right">
                <button class="btn btn-primary">
                  <i class="fa fa-save"></i>
                  Guardar
                </button>
              </div>  
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>