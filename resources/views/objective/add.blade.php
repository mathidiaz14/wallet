<!-- Button trigger modal -->
  <button type="button" class="btn btn-success" data-toggle="modal" data-target="#addModal_{{$id}}">
    <i class="fa fa-plus"></i>
  </button>

  <!-- Modal -->
  <div class="modal fade" id="addModal_{{$id}}" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true" >
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="addModalLabel">Agregar monto</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body text-left">
          <form class="form-horizontal" action="{{url('objective/add')}}" method="post">
            @csrf
            <input type="hidden" name="objective" value="{{$id}}">
			<input type="hidden" name="type" id="type" value="more">
            
            <div class="col-sm-12">
                <div class="form-group">
                    <div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text" id="basic-addon1_{{$id}}">
								<i class="fa fa-plus"></i>
							</span>
						</div>
						<input type="number" class="form-control" aria-describedby="basic-addon1_{{$id}}" name="number" id="number_{{$id}}" required style="text-align: right; font-size: 1.5em;">
					</div>
                </div>
                <div class="form-group">
                	<div class="row">
                		<div class="col">
                			<a class="btn btn-info btn-block text-white btn-calc" attr-value="7">
                				<b>7</b>
                			</a>
                		</div>
                		<div class="col">
                			<a class="btn btn-info btn-block text-white btn-calc" attr-value="8">
                				<b>8</b>
                			</a>
                		</div>
                		<div class="col">
                			<a class="btn btn-info btn-block text-white btn-calc" attr-value="9">
                				<b>9</b>
                			</a>
                		</div>
                	</div>
                	<br>
                	<div class="row">
                		<div class="col">
                			<a class="btn btn-info btn-block text-white btn-calc" attr-value="4">
                				<b>4</b>
                			</a>
                		</div>
                		<div class="col">
                			<a class="btn btn-info btn-block text-white btn-calc" attr-value="5">
                				<b>5</b>
                			</a>
                		</div>
                		<div class="col">
                			<a class="btn btn-info btn-block text-white btn-calc" attr-value="6">
                				<b>6</b>
                			</a>
                		</div>
                	</div>
                	<br>
                	<div class="row">
                		<div class="col">
                			<a class="btn btn-info btn-block text-white btn-calc" attr-value="1">
                				<b>1</b>
                			</a>
                		</div>
                		<div class="col">
                			<a class="btn btn-info btn-block text-white btn-calc" attr-value="2">
                				<b>2</b>
                			</a>
                		</div>
                		<div class="col">
                			<a class="btn btn-info btn-block text-white btn-calc" attr-value="3">
                				<b>3</b>
                			</a>
                		</div>
                	</div>
                	<br>
                	<div class="row">
                		<div class="col">
                			<a class="btn btn-info btn-block text-white btn-calc" attr-value="0">
                				<b>0</b>
                			</a>
                		</div>
                		<div class="col">
                			<a class="btn btn-info btn-block text-white btn-change">
                				<b>+ / -</b>
                			</a>
                		</div>
                		<div class="col">
                			<a class="btn btn-warning btn-block text-white btn-erase">
            					<b>C</b>
            				</a>
            			</div>
                	</div>
                </div>
            </div>
            <hr>
            <div class="row">
              <div class="col">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                  <i class="fa fa-chevron-left"></i>
                  Atras
                </button>
              </div>
              <div class="col text-right">
                <button class="btn btn-primary">
                  <i class="fa fa-save"></i>
                  Guardar
                </button>
              </div>  
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  	<script>
	  	$(document).ready(function()
	  	{
	  		$('#addModal_{{$id}}').on('shown.bs.modal', function () 
			{
				$('#number_{{$id}}').trigger('focus');
			});
	  		$('.btn-calc').click(function()
	  		{
	  			$('#number_{{$id}}').val($('#number_{{$id}}').val() + $(this).attr('attr-value'));
	  		});

	  		$('.btn-change').click(function()
	  		{
	  			if($('#type').val() == "more")
	  			{
	  				$('#type').val('less');
	  				$('#basic-addon1_{{$id}}').html('<i class="fa fa-minus"></i>');
	  			}else
	  			{
	  				$('#type').val('more');
	  				$('#basic-addon1_{{$id}}').html('<i class="fa fa-plus"></i>');
	  			}
	  		});

	  		$('.btn-erase').click(function()
	  		{
	  			$('#number_{{$id}}').val("");
	  		});
	  	});
 	</script>