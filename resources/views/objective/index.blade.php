@extends('layouts.dashboard', ['menu' => 'objective'])

@section('content')
<div class="row">
  <div class="col-sm">
    <div class="card shadow">
      <div class="card-header py-3">
        <div class="row">
          <div class="col">
            <h6 class="m-0 font-weight-bold text-primary">Objetivos</h6>
          </div>
          <div class="col text-right">
            @include('objective.create')  
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="table table-responsive">
          <table class="table table-striped">
            <thead>
                <tr class="text-center">
                  <th>Nombre</th>
                  <th>Color</th>
                  <th>Progreso</th>
                  <th>Monto</th>
                  <th>Objetivo</th>
                  <th></th>
                  <th></th>
                  <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach(Auth::user()->objectives as $objective)
                    <tr class="text-center">
                        <td>{{$objective->title}}</td>
                        <td>
                          <span style="background-color: {{$objective->color}}; padding: 5px 30px; border-radius: 5px;">
                          </span>
                        </td>
                        <td>
                        	@if($objective->objective > 0)
                            <b>{{($objective->amount * 100) / $objective->objective }}%</b>
                          @else
                            <b>0%</b>
                          @endif
                        </td>
                        <td>${{$objective->amount}}</td>
                        <td>${{$objective->objective}}</td>
                        <td>
                          @include('objective.add', ['id' => $objective->id])
                        </td>
                        <td>
                          @include('objective.edit', ['id' => $objective->id, 'objective' => $objective])
                        </td>
                        <td>
                            @include('helper.delete', ['id' => $objective->id, 'route' => url('objective', $objective->id)])
                        </td>
                    </tr>
                @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
  
@endsection