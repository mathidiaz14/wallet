@php
  if($account_select == null)
  {
    $registries = \Auth::user()->registries->where('subcategory_id', '!=', 'transfer')->whereBetween('date', [$start, $end]);
  }
  elseif($account_select == 'total')
  {
    $registries = collect();
    foreach(Auth::user()->accounts->where('exclude', '!=', 'on') as $account)
    {
      foreach($account->registries->where('subcategory_id', '!=', 'transfer')->whereBetween('date', [$start, $end]) as $registry)
      {
        $registries->add($registry);
      }
    }
  }else
  {
    $registries = App\Account::find($account_select)->registries->where('subcategory_id', '!=', 'transfer')->whereBetween('date', [$start, $end]);
  }
@endphp
  <div class="card shadow mb-4">
     <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
      <h6 class="m-0 font-weight-bold text-primary">Flujo de efectivo</h6>
      <div class="dropdown no-arrow">
        <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
          <div class="dropdown-header">Opciones</div>
          <form action="{{url('dashboard/card/delete')}}" method="post">
            @csrf
            <input type="hidden" name="number" value="{{$number}}">
            <input type="submit" class="dropdown-item" value="Eliminar">
          </form>
        </div>
      </div>
    </div>
    <div class="card-body">
      <div class="row">
        @if($registries->count() == 0)
        <div class="col text-center">
          <br>
          <i class="fa fa-receipt fa-4x"></i>
          <br><br>
          <p>No hay ningun registro</p>
        </div>
      @else
      
        @php 
          $registryMore = 0;
          $registryLess = 0;
        @endphp
        
        @foreach($registries as $registry)
          @if($registry->type == "more")
            @php
                $registryMore += $registry->amount;
            @endphp
          
          @else
            
            @php
              $registryLess += $registry->amount;
            @endphp

          @endif
        @endforeach
        
        <div class="col">
          <h3>Diferencia <span class="float-right">${{number_format($registryMore - $registryLess)}}</span></h3>
        </div>
        <hr>
        <div class="col">
          @if ($registryMore >= $registryLess)
            <h4 class="small font-weight-bold">Ingresos <span class="float-right">${{number_format($registryMore)}}</span></h4>
            <div class="progress mb-4">
              <div class="progress-bar bg-success" role="progressbar" style="width: 100%" aria-valuenow="$registryMore" aria-valuemin="0" aria-valuemax="$registryMore"></div>
            </div>

            <h4 class="small font-weight-bold">Egresos <span class="float-right">${{number_format($registryLess)}}</span></h4>
            <div class="progress mb-4">
              @php
                if($registryMore > 0)
                  $perc = ($registryLess * 100) / $registryMore;
                else
                  $perc = 100;
              @endphp
              <div class="progress-bar bg-danger" role="progressbar" style="width: {{$perc}}%" aria-valuenow="$registryLess" aria-valuemin="0" aria-valuemax="$registryLess"></div>
            </div>
          @endif

          @if ($registryMore < $registryLess)
            <h4 class="small font-weight-bold">Ingresos <span class="float-right">${{$registryMore}}</span></h4>
            <div class="progress mb-4">
              @php
                if($registryLess > 0)
                  $perc = ($registryMore * 100) / $registryLess;
                else
                  $perc = 100;
              @endphp
              <div class="progress-bar bg-success" role="progressbar" style="width: {{$perc}}%" aria-valuenow="$registryMore" aria-valuemin="0" aria-valuemax="$registryMore"></div>
            </div>

            <h4 class="small font-weight-bold">Egresos <span class="float-right">${{$registryLess}}</span></h4>
            <div class="progress mb-4">
              
              <div class="progress-bar bg-danger" role="progressbar" style="width: 100%" aria-valuenow="$registryLess" aria-valuemin="0" aria-valuemax="$registryLess"></div>
            </div>
          @endif
        </div>
      @endif
      </div>
    </div>
  </div>