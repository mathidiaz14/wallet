@php
  if(\Carbon\Carbon::now()->diffInDays($start, true) < 20)
    $start2   = \Carbon\Carbon::create($start)->subDays(20);
  else
    $start2   = \Carbon\Carbon::create($start);

  $end2       = \Carbon\Carbon::create($end);
  $end3       = \Carbon\Carbon::create($end);
  $histories  = collect();

  if($account_select == null)
  {
    $accounts = Auth::user()->accounts;

    foreach($accounts as $account)
    {
      foreach($account->histories as $history)
        $histories->add($history);
    }
  }
  elseif($account_select == 'total')
  {
    $accounts = Auth::user()->accounts->where('exclude', '!=', 'on');
    
    foreach($accounts as $account)
    {
      foreach($account->histories as $history)
        $histories->add($history);
    }
  }
  else{
    $histories = \App\Account::find($account_select)->histories;
  }
    
  
@endphp
  <div class="card shadow mb-4">
   <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
    <h6 class="m-0 font-weight-bold text-primary">Tendencia de saldo</h6>
    <div class="dropdown no-arrow">
      <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
      </a>
      <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
        <div class="dropdown-header">Opciones</div>
        <form action="{{url('dashboard/card/delete')}}" method="post">
          @csrf
          <input type="hidden" name="number" value="{{$number}}">
          <input type="submit" class="dropdown-item" value="Eliminar">
        </form>
      </div>
    </div>
  </div>
  <div class="card-body">
    @if(($histories->count() == 0) or ($histories->sum('amount') == 0))
      <div class="col text-center">
        <br>
        <i class="fa fa-receipt fa-4x"></i>
        <br><br>
        <p>No hay ningun registro</p>
      </div>
    @else
      <div class="row">
        <div class="col">
          <p>Tendencia desde {{$start2->format('d/m/y')}} hasta {{$end->format('d/m/y')}}</p>
        </div>
      </div>
      <div class="row">
        <canvas id="balanceTrend"></canvas>
      </div>
      
      <script>      
        var configBalanceTrend = {
          type: 'line',
          data: {
            labels: [
                @while($start2->diffInDays($end2, true) > 0)
                  "{{$end2->subDay(1)->format('d/m')}}",
                @endwhile
              ],
            datasets: [{
              label: 'Saldo',
              borderColor: "#0092db",
              backgroundColor: "#0092dbaa",
              data: [
                
                @while($start2->diffInDays($end3, true) > 0)
                  "{{$histories->where('date', $end3->subDay(1)->format('d-m-y'))->sum('amount')}}",
                @endwhile
              ],
            }]
          },
          options: {
            responsive: true,
            tooltips: {
              mode: 'index',
            },
            hover: {
              mode: 'index'
            },
            scales: {
              xAxes: [{
                scaleLabel: {
                  display: true,
                  labelString: 'Dia'
                }
              }],
              yAxes: [{
                stacked: true,
                scaleLabel: {
                  display: true,
                  labelString: 'Saldo'
                }
              }]
            }
          }
        };

        $(document).ready(function()
        {
          var balanceTrend = document.getElementById('balanceTrend').getContext('2d');
          window.myLine = new Chart(balanceTrend, configBalanceTrend);
        });
      </script>
    @endif
  </div>
  </div>