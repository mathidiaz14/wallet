@php
	$registries = collect(); 

    foreach(Auth::user()->accounts as $account)
    {    
      foreach($account->registries->whereBetween('date', [$start, $end]) as $registry) 
        $registries->add($registry);
    }
@endphp

<div class="card shadow mb-4">
  	<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
    	<h6 class="m-0 font-weight-bold text-primary">Ultimos registros</h6>
	    <div class="dropdown no-arrow">
	      <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	        <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
	      </a>
	      <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
	        <div class="dropdown-header">Opciones</div>
	        <form action="{{url('dashboard/card/delete')}}" method="post">
		        @csrf
		        <input type="hidden" name="number" value="{{$number}}">
		        <input type="submit" class="dropdown-item" value="Eliminar">
		      </form>
	      </div>
	    </div>
	  </div>
	  <!-- Card Body -->
	  <div class="card-body">
	    @if($registries->count() == 0)
	      <div class="col text-center">
	        <br>
	        <i class="fa fa-receipt fa-4x"></i>
	        <br><br>
	        <p>No hay ningun registro</p>
	      </div>
	    @else
	      	<div class="table table-responsive">
          	<table class="table table-striped">
            <tbody>
              <tr class="text-center">
                <th>Fecha</th>
                <th>Monto</th>
                <th>Subcategoria</th>
                <th>Usuario</th>
                <th>Cuenta</th>
              </tr>
              @foreach($registries->sortByDesc('date')->take(5) as $registry)
                <tr class="text-center">
                  <td>{{$registry->date->format('d/m')}} {{$registry->hour}}</td>
                  <td>
                    @if($registry->type == "less")
                      <b style="color:red;">
                        -{{$registry->amount}}
                      </b>
                    @else
                      <b style="color:green;">
                        +{{$registry->amount}}
                      </b>
                    @endif
                  </td>
                  <td>
                    @include('registry.subcategory_show')
                  </td>
                  <td>{{$registry->user->name}}</td>
                  <td>
                    <a href="{{url('account', $registry->account->id)}}" class="subcategory-bg" style="
                      background-color: {{$registry->account->color}}; ">
                      {{$registry->account->title}}
                    </a>
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
	        <div class="col text-center">
	          <a href="{{url('registry')}}">Ver mas...</a>
	        </div>
	    </div>
	  @endif
	</div>
</div>