@php
  if($account_select == null)
  {
    $registries = \Auth::user()->registries->where('subcategory_id', '!=', 'transfer')->where('type', 'more')->whereBetween('date', [$start, $end]);
  
  }elseif($account_select == 'total')
  {
    $registries = collect();
    foreach(Auth::user()->accounts->where('exclude', '!=', 'on') as $account)
    {
      foreach($account->registries->where('subcategory_id', '!=', 'transfer')->where('type', 'more')->whereBetween('date', [$start, $end]) as $registry)
      {
        $registries->add($registry);
      }
    }
  }else
  {
    $registries = App\Account::find($account_select)->registries->where('subcategory_id', '!=', 'transfer')->where('type', 'more')->whereBetween('date', [$start, $end]);
  }
@endphp

<div class="card shadow mb-4">
    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
      <h6 class="m-0 font-weight-bold text-primary">Estructura de Ingresos</h6>
      <div class="dropdown no-arrow">
        <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
          <div class="dropdown-header">Opciones</div>
          <form action="{{url('dashboard/card/delete')}}" method="post">
            @csrf
            <input type="hidden" name="number" value="{{$number}}">
            <input type="submit" class="dropdown-item" value="Eliminar">
          </form>
        </div>
      </div>
    </div>
    <!-- Card Body -->
    <div class="card-body">
      @if($registries->count() == 0)
        <div class="col text-center">
          <br>
          <i class="fa fa-receipt fa-4x"></i>
          <br><br>
          <p>No hay ningun gasto</p>
        </div>
      @else
        <div class="row">
          <div class="col-12">
            <canvas id="income_structure"></canvas>
          </div>
          <div class="col-12">
            <div class="table table-responsive">
              <table class="table mt-3">
                <tbody>
                  @foreach($registries->groupBy('subcategory_id') as $registrie)
                    <tr>
                      <td>
                        <span style="border-radius: 2px; padding:0 10px; margin-right: 5px; background: 
                            @if($registrie[0]->subcategory_id == "debt")
                              #f1e66a
                            @elseif($registrie[0]->subcategory->color == null)
                              {{$registrie[0]->subcategory->category->color}}
                            @else
                              {{$registrie[0]->subcategory->color}}
                            @endif;">
                          </span> 
                      </td>
                      <td>
                        <b> 
                          @if($registrie[0]->subcategory_id == "debt")
                            Prestamo
                          @else
                            {{$registrie[0]->subcategory->title}}
                          @endif
                        </b>
                      </td>
                      <td>
                        <b>
                          ${{$registrie->sum('amount')}}
                        </b>
                      </td>
                    </tr>
                  @endforeach
                  <tfoot>
                      <tr>
                        <td></td>
                        <td>Total:</td>
                        <td>
                          <b>${{$registries->sum('amount')}}</b>
                        </td>
                      </tr>
                    </tfoot> 
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <script>
          var configIncomeStructure = {
            type: 'doughnut',
            data: {
              datasets: [{
                data: [
                  @foreach($registries->groupBy('subcategory_id') as $registrie)
                      {{$registrie->sum('amount')}},
                  @endforeach
                ],
                backgroundColor: [
                  @foreach($registries->groupBy('subcategory_id') as $registrie)
                    @if($registrie[0]->subcategory_id == "debt")
                      "#f1e66a",
                    @elseif($registrie[0]->subcategory->color == null)
                      "{{$registrie[0]->subcategory->category->color}}",
                    @else
                      "{{$registrie[0]->subcategory->color}}",
                    @endif

                  @endforeach

                ],
                label: 'Dataset 1'
              }],
              labels: [
                @foreach($registries->groupBy('subcategory_id') as $registrie)
                  @if($registrie[0]->subcategory_id == "debt")
                    "Prestamo [${{$registrie->sum('amount')}}]",
                  @else
                    "{{$registrie[0]->subcategory->title}}",
                  @endif
                @endforeach
              ]
            },
            options: {
              responsive: true,
              legend: {
                display:  false,
                position: 'right',
              },
              animation: {
                animateScale: true,
                animateRotate: true
              }
            }
          };

          $(document).ready(function()
          {
            var income_structure = document.getElementById('income_structure').getContext('2d');
            window.myDoughnut = new Chart(income_structure, configIncomeStructure);
          });
        </script>
          
      @endif
    </div>
</div>

