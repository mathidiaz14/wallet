@php
  if($account_select == null)
  {
    $registries = \Auth::user()->registries->where('subcategory_id', '!=', 'transfer')->where('subcategory_id', '!=', null)->where('type', 'less')->whereBetween('date', [$start, $end]);
  
  }elseif($account_select == 'total')
  {
    $registries = collect();
    foreach(Auth::user()->accounts->where('exclude', '!=', 'on') as $account)
    {
      foreach($account->registries->where('subcategory_id', '!=', 'transfer')->where('subcategory_id', '!=', null)->where('type', 'less')->whereBetween('date', [$start, $end]) as $registry)
      {
        $registries->add($registry);
      }
    }
  }else
  {
    $registries = App\Account::find($account_select)->registries->where('subcategory_id', '!=', 'transfer')->where('subcategory_id', '!=', null)->where('type', 'less')->whereBetween('date', [$start, $end]);
  }

  $categories = $registries->groupBy('category_id');
@endphp

<div class="card shadow mb-4">
    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
      <h6 class="m-0 font-weight-bold text-primary">Estructura de gastos</h6>
      <div class="dropdown no-arrow">
        <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
          <div class="dropdown-header">Opciones</div>
          <form action="{{url('dashboard/card/delete')}}" method="post">
            @csrf
            <input type="hidden" name="number" value="{{$number}}">
            <input type="submit" class="dropdown-item" value="Eliminar">
          </form>
        </div>
      </div>
    </div>
    <!-- Card Body -->
    <div class="card-body">
      @if($categories->count() == 0)
        <div class="col text-center">
          <br>
          <i class="fa fa-receipt fa-4x"></i>
          <br><br>
          <p>No hay ningun gasto</p>
        </div>
      @else
        <div id="expense_structure_main">
          <div class="row">
            <div class="col-12">
              <canvas id="expense_structure"></canvas>  
            </div>
            <div class="col-12">
              <div class="table table-responsive">
                <table class="table mt-3">
                  <tbody>
                    @foreach($categories as $category)
                      @if($category[0]->subcategory_id == "debt")
                        <tr>
                          <td>
                            <span style="border-radius: 2px; padding:0 10px; margin-right: 5px; background: #f1e66a;"></span>
                          </td>
                          <td>
                            <a>
                            Prestamo
                            </a>
                          </td>
                          <td>
                            ${{$category->sum('amount')}}
                          </td>
                        </tr>
                      @else
                        <tr>
                          <td>
                            <span style="border-radius: 2px; padding:0 10px; margin-right: 5px; background: {{$category[0]->category->color}};"></span>
                          </td>
                          <td>
                            <a class="btn_show_expense_category" attr-category="{{$category[0]->category_id}}">
                              <b>
                                {{$category[0]->category->title}}
                                <i class="fa fa-external-link-alt fa-xs"></i>
                              </b>
                            </a>
                          </td>
                          <td>
                            <b>${{$category->sum('amount')}}</b>
                          </td>
                        </tr>
                      @endif
                    @endforeach
                    <tfoot>
                      <tr>
                        <td></td>
                        <td>Total:</td>
                        <td>
                          @php $total = 0; @endphp
                          @foreach($categories as $category)
                            @php
                              $total += $category->sum('amount'); 
                            @endphp
                          @endforeach
                          <b>${{$total}}</b>
                        </td>
                      </tr>
                    </tfoot>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <script>
          var configExpenseStructure = {
            type: 'doughnut',
            data: {
              datasets: [{
                data: [
                  @foreach($categories as $category)
                      "{{$category->sum('amount')}}",
                  @endforeach
                ],
                backgroundColor: [
                  @foreach($categories as $category)
                    @if($category[0]->subcategory_id == "debt")
                      "#f1e66a",
                    @else
                      "{{$category[0]->category->color}}",
                    @endif
                  @endforeach
                ],
                label: 'Dataset 1'
              }],
              labels: [
                @foreach($categories as $category)
                  @if($category[0]->subcategory_id == "debt")
                    "Prestamo",
                  @else
                    "{{$category[0]->category->title}}",
                  @endif
                @endforeach
              ]
            },
            options: {
              responsive: true,
              legend: {
                display: false,
                position: 'right',
              },
              animation: {
                animateScale: true,
                animateRotate: true
              }
            }
          };


          $(document).ready(function()
          {
              var expense_structure = document.getElementById('expense_structure').getContext('2d');
              window.myDoughnut = new Chart(expense_structure, configExpenseStructure);
          });

          $('.btn_show_expense_category').click(function()
          {
            var id = $(this).attr('attr-category');

            $('#expense_structure_main').hide();
            $('.show_expense_category').hide();
            $('#show_expense_category_'+id).fadeIn();
          });
        </script>

        @foreach($categories as $category)
          @php $subcategories = $registries->where('category_id', $category[0]->category_id)->groupBy('subcategory_id'); @endphp
          <div class="col-xs-12 show_expense_category" id="show_expense_category_{{$category[0]->category_id}}" style="display: none;">
            <div class="row">
              <div class="col-12">
                <canvas id="expense_structure_{{$category[0]->category_id}}"></canvas>
              </div>
              <div class="col-12">
                <table class="table mt-3">
                  <tbody>
                    @foreach($subcategories as $subcategory)
                      @if($subcategory[0]->subcategory_id != "debt")
                      <tr>
                        <td>
                          <span style="border-radius: 2px; padding:0 10px; margin-right: 5px; background: 
                           @if($subcategory[0]->subcategory_id != "debt")
                            @if($subcategory[0]->subcategory->color == null)
                              {{$subcategory[0]->category->color}}
                            @else
                              {{$subcategory[0]->subcategory->color}}
                            @endif
                          @endif;"></span> 
                        </td>
                        <td>
                          <b>{{$subcategory[0]->subcategory->title}}</b>
                        </td>
                        <td>
                          <b>${{$subcategory->sum('amount')}}</b>
                        </td>
                      </tr>
                      @endif
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
            <a class="back_main">
              <b>
                <i class="fa fa-angle-left"></i>
                Volver
              </b>
            </a> 
            <script>
              var configExpenseStructure_{{$category[0]->category_id}} = {
                type: 'doughnut',
                data: {
                  datasets: [{
                    data: [
                      @foreach($subcategories as $subcategory)
                        @if($subcategory[0]->subcategory_id != "debt")
                          "{{$subcategory->sum('amount')}}",
                        @endif
                      @endforeach
                    ],
                    backgroundColor: [
                      @foreach($subcategories as $subcategory)
                        @if($subcategory[0]->subcategory_id != "debt")
                          @if($subcategory[0]->subcategory->color == null)
                            "{{$subcategory[0]->category->color}}",
                          @else
                            "{{$subcategory[0]->subcategory->color}}",
                          @endif
                        @endif
                      @endforeach
                    ],
                    label: 'Dataset 1'
                  }],
                  labels: [
                    @foreach($subcategories as $subcategory)
                      @if($subcategory[0]->subcategory_id != "debt")
                        "{{$subcategory[0]->subcategory->title}}",
                      @endif
                    @endforeach
                  ]
                },
                options: {
                  responsive: true,
                  legend: {
                    position: 'right',
                    display: false,
                  },
                  animation: {
                    animateScale: true,
                    animateRotate: true
                  }
                }
              };


              $(document).ready(function()
              {
                  var expense_structure_{{$category[0]->category_id}} = document.getElementById('expense_structure_{{$category[0]->category_id}}').getContext('2d');
                  window.myDoughnut = new Chart(expense_structure_{{$category[0]->category_id}}, configExpenseStructure_{{$category[0]->category_id}});
              });
            </script>
            
          </div>
        @endforeach
      @endif
      <script>
        $('.back_main').click(function()
          {
            console.log('ok');
            $('.show_expense_category').hide();
            $('#expense_structure_main').fadeIn();
          });
      </script>
    </div>
</div>