<div class="card shadow mb-4">
   <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
      <h6 class="m-0 font-weight-bold text-primary">Deudas</h6>
      <div class="dropdown no-arrow">
        <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
          <div class="dropdown-header">Opciones</div>
          <form action="{{url('dashboard/card/delete')}}" method="post">
            @csrf
            <input type="hidden" name="number" value="{{$number}}">
            <input type="submit" class="dropdown-item" value="Eliminar">
          </form>
        </div>
      </div>
    </div>
  <div class="card-body">
    @if(Auth::user()->debts->count() == 0)
      <div class="col text-center">
        <br>
        <i class="fa fa-hand-holding-usd fa-4x"></i>
        <br><br>
        <p>No hay ninguna deuda</p>
      </div>
    @else
      @foreach(Auth::user()->debts->where('status', 'enabled') as $debt)
        @php
          $perc = ($debt->amount_returned * 100) / $debt->amount;
        @endphp
        <h4 class="small font-weight-bold">
          {{$debt->who}} - ${{$debt->amount}} <span class="float-right">{{$perc}}%</span>
        </h4>
        <div class="progress mb-4">
          <div class="progress-bar bg-danger" style=" width: {{$perc}}%;" role="progressbar" aria-valuenow="{{$perc}}" aria-valuemin="0" aria-valuemax="100"></div>
        </div>
      @endforeach
    @endif
  </div>
</div>