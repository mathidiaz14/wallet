@php
  if($account_select == null)
  {
    
    $registries = \Auth::user()->registries->where('subcategory_id', '!=', 'transfer');
  
  }elseif($account_select == 'total')
  {
    $registries = collect();
    foreach(Auth::user()->accounts->where('exclude', '!=', 'on') as $account)
    {
      foreach($account->registries as $registry)
        $registries->add($registry);
    }
  
  }else
  {
    $registries = App\Account::find($account_select)->registries->where('subcategory_id', '!=', 'transfer');
  }
@endphp

<div class="card shadow mb-4">
	<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
	<h6 class="m-0 font-weight-bold text-primary">Comparación periodo a periodo</h6>
	<div class="dropdown no-arrow">
	  <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
	    <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
	  </a>
	  <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
	    <div class="dropdown-header">Opciones</div>
	    <form action="{{url('dashboard/card/delete')}}" method="post">
	        @csrf
	        <input type="hidden" name="number" value="{{$number}}">
	        <input type="submit" class="dropdown-item" value="Eliminar">
	      </form>
	  </div>
	</div>
	</div>
	<div class="card-body">
	  @if($registries->count() == 0)
	    <div class="col text-center">
	      <br>
	      <i class="fa fa-receipt fa-4x"></i>
	      <br><br>
	      <p>No hay ningun registro</p>
	    </div>
	  @else
	  	<div class="row">
	  		<div class="col">
	  			<canvas id="periodComparison"></canvas>
	  		</div>
	  	</div>
	  	<script>			
			var configPeriodComparison = {
				type: 'line',
				data: {
					labels: [
						'{{\Carbon\Carbon::now()->subMonth(6)->format("M")}}', 
						'{{\Carbon\Carbon::now()->subMonth(5)->format("M")}}', 
						'{{\Carbon\Carbon::now()->subMonth(4)->format("M")}}', 
						'{{\Carbon\Carbon::now()->subMonth(3)->format("M")}}', 
						'{{\Carbon\Carbon::now()->subMonth(2)->format("M")}}', 
						'{{\Carbon\Carbon::now()->subMonth(1)->format("M")}}', 
						'{{\Carbon\Carbon::now()->format("M")}}'],
					datasets: [{
						label: 'Ingreso',
						borderColor: "#23bc44",
						backgroundColor: "#23bc44cc",
						data: [
							{{$registries->where('type', 'more')->whereBetween('date', [\Carbon\Carbon::now()->subMonth(6)->firstOfMonth(), \Carbon\Carbon::now()->subMonth(6)->lastOfMonth()])->sum('amount')}},

							{{$registries->where('type', 'more')->whereBetween('date', [\Carbon\Carbon::now()->subMonth(5)->firstOfMonth(), \Carbon\Carbon::now()->subMonth(5)->lastOfMonth()])->sum('amount')}},

							{{$registries->where('type', 'more')->whereBetween('date', [\Carbon\Carbon::now()->subMonth(4)->firstOfMonth(), \Carbon\Carbon::now()->subMonth(4)->lastOfMonth()])->sum('amount')}},

							{{$registries->where('type', 'more')->whereBetween('date', [\Carbon\Carbon::now()->subMonth(3)->firstOfMonth(), \Carbon\Carbon::now()->subMonth(3)->lastOfMonth()])->sum('amount')}},

							{{$registries->where('type', 'more')->whereBetween('date', [\Carbon\Carbon::now()->subMonth(2)->firstOfMonth(), \Carbon\Carbon::now()->subMonth(2)->lastOfMonth()])->sum('amount')}},
							
							{{$registries->where('type', 'more')->whereBetween('date', [\Carbon\Carbon::now()->subMonth(1)->firstOfMonth(), \Carbon\Carbon::now()->subMonth(1)->lastOfMonth()])->sum('amount')}},

							{{$registries->where('type', 'more')->whereBetween('date', [\Carbon\Carbon::now()->firstOfMonth(), \Carbon\Carbon::now()->lastOfMonth()])->sum('amount')}}
						],
					}, {
						label: 'Gasto',
						borderColor: "#d62222",
						backgroundColor: "#d62222cc",
						data: [
							{{$registries->where('type', 'less')->whereBetween('date', [\Carbon\Carbon::now()->subMonth(6)->firstOfMonth(), \Carbon\Carbon::now()->subMonth(6)->lastOfMonth()])->sum('amount')}},

							{{$registries->where('type', 'less')->whereBetween('date', [\Carbon\Carbon::now()->subMonth(5)->firstOfMonth(), \Carbon\Carbon::now()->subMonth(5)->lastOfMonth()])->sum('amount')}},

							{{$registries->where('type', 'less')->whereBetween('date', [\Carbon\Carbon::now()->subMonth(4)->firstOfMonth(), \Carbon\Carbon::now()->subMonth(4)->lastOfMonth()])->sum('amount')}},

							{{$registries->where('type', 'less')->whereBetween('date', [\Carbon\Carbon::now()->subMonth(3)->firstOfMonth(), \Carbon\Carbon::now()->subMonth(3)->lastOfMonth()])->sum('amount')}},

							{{$registries->where('type', 'less')->whereBetween('date', [\Carbon\Carbon::now()->subMonth(2)->firstOfMonth(), \Carbon\Carbon::now()->subMonth(2)->lastOfMonth()])->sum('amount')}},
							
							{{$registries->where('type', 'less')->whereBetween('date', [\Carbon\Carbon::now()->subMonth(1)->firstOfMonth(), \Carbon\Carbon::now()->subMonth(1)->lastOfMonth()])->sum('amount')}},

							{{$registries->where('type', 'less')->whereBetween('date', [\Carbon\Carbon::now()->firstOfMonth(), \Carbon\Carbon::now()->lastOfMonth()])->sum('amount')}}
						],
					}]
				},
				options: {
					responsive: true,
					tooltips: {
						mode: 'index',
					},
					scales: {
						xAxes: [{
							scaleLabel: {
								display: true,
								labelString: 'Mes'
							}
						}],
						yAxes: [{
							scaleLabel: {
								display: true,
								labelString: 'Monto'
							}
						}]
					}
				}
			};

			$(document).ready(function()
			{
				var periodComparison = document.getElementById('periodComparison').getContext('2d');
				window.myLine = new Chart(periodComparison, configPeriodComparison);
			});
		</script>
	  @endif
	</div>
</div>
