<!-- Button trigger modal -->
<button type="button" class="btn btn-info" data-toggle="modal" data-target="#editModal_{{$id}}">
	<i class="fa fa-edit"></i>
</button>

<!-- Modal -->
<div class="modal fade" id="editModal_{{$id}}" tabindex="-1" role="dialog" aria-labelledby="editModalLabel_{{$id}}" aria-hidden="true" >
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="editModalLabel_{{$id}}">Editar subcategoria</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body text-left">
				<form class="form-horizontal" action="{{url('subcategory', $subcategory->id)}}" method="post">
					@csrf
					@method('PATCH')
					<div class="col-12">
						<div class="form-group">
							<label>Nombre</label>
							<input type="text" class="form-control" value="{{$subcategory->title}}" name="title" id="title" required="">
						</div>
					</div>
					<div class="col-12">
						<div class="form-group">
							<label>Descripción</label>
							<textarea name="description" id="" class="form-control">{{$subcategory->description}}</textarea>
						</div>
					</div>
					<div class="col-12">
						<div class="form-group">
							<label>Color</label>
							<input type="color" class="form-control" value="@if($subcategory->color == null){{$subcategory->category->color}}@else{{$subcategory->color}}@endif" name="color">
						</div>
					</div>
					<hr>
					<div class="row">
						<div class="col">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">
								<i class="fa fa-chevron-left"></i>
								Atras
							</button>
						</div>
						<div class="col text-right">
							<button class="btn btn-primary">
								<i class="fa fa-save"></i>
								Guardar
							</button>
						</div>  
					</div>
				</form>
			</div>	
		</div>
	</div>
</div>