@extends('layouts.auth')

@section('content')
<div class="row">
  <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
  <div class="col-lg-6">
    <div class="p-5">
        <br><br>
        <div class="text-center">
            <h1 class="h4 text-gray-900 mb-4">Bienvenido de nuevo</h1>
        </div>
        <br>
        <form class="user" method="POST" action="{{ route('login') }}">
            @csrf
            <div class="form-group">
                <input id="email" type="email" class="form-control form-control-user" name="email" value="{{ old('email') }}" placeholder="Email" required autofocus>

                @if ($errors->has('email'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <input id="password" type="password" class="form-control form-control-user" name="password" required placeholder="Contraseña">

                @if ($errors->has('password'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <div class="custom-control custom-checkbox small">
                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }} class="custom-control-input" id="customCheck">
                    <label class="custom-control-label" for="customCheck">Recuerdame</label>
                </div>
            </div>
            <button type="submit" class="btn btn-primary btn-user btn-block">
                Iniciar Sesión
            </button>
        </form>
        <hr>
        <div class="text-center">
            <a class="small" href="{{url('password.request')}}">Olvidaste tu contraseña?</a>
        </div>
        <div class="text-center">
            <a class="small" href="{{url('register')}}">Crear una cuenta</a>
        </div>
    </div>
  </div>
</div>

@endsection
