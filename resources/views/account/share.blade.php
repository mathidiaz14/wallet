<!-- Button trigger modal -->
<button type="button" class="btn btn-info" data-toggle="modal" data-target="#shareModal_{{$id}}">
    <i class="fa fa-share-alt"></i>
</button>

<!-- Modal -->
<div class="modal fade" id="shareModal_{{$id}}" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editModalLabel">Compartir cuenta</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-left">
            	<form class="form-horizontal" action="{{url('account/share')}}" method="post">
                    @csrf
                    <input type="hidden" name="account" value="{{$account->id}}">
                    <div class="form-group">
                        <label>Invitar a un amigo</label>
                        <div class="row">
	                        <div class="col-9">
	                        	<input type="email" class="form-control" placeholder="Ingrese aqui el email" name="email" id="email" required>
	                        </div>
	                        <div class="col-3">
	                        	<button class="btn btn-primary btn-block">
		                        	<i class="fa fa-paper-plane"></i>
		                        </button>
	                        </div>
                        </div>
                    </div>
                </form>
                <hr>
				<p>Miembros de la cuenta</p>
				<div class="table table-responsive">
					<table class="table table-striped">
						<tr class="text-center">
							<th>Nombre</th>
							<th>Email</th>
							<th>Estado</th>
							<th>Eliminar</th>
						</tr>
						@foreach($account->users as $user)
							@if($user->id != Auth::user()->id)
								<tr class="text-center">
									<td>{{$user->name}}</td>
									<td>{{$user->email}}</td>
									<td>
										Activo
									</td>
									<td>
										<form action="{{url('delete/user/account')}}" method="POST">
											@csrf
											<input type="hidden" name="account" value="{{$account->id}}">
											<input type="hidden" name="user" value="{{$user->id}}">
											<button class="btn btn-danger">
												<i class="fa fa-times"></i>
											</button>
										</form>
									</td>
								</tr>
							@endif
	                	@endforeach
	                	
	                	@php 
	                		$shareds = App\Shared::where('account', $account->id)->get(); 
	                	@endphp
	                	
	                	@foreach($shareds as $shared)
							<tr class="text-center">
								<td> -- </td>
								<td>{{$shared->email}}</td>
								<td>
									Esperando
								</td>
								<td>
									<form action="{{url('delete/user/shared')}}" method="POST">
										@csrf
										<input type="hidden" name="id" value="{{$shared->id}}">
										<button class="btn btn-danger">
											<i class="fa fa-times"></i>
										</button>
									</form>
								</td>
							</tr>
	                	@endforeach
					</table>
				</div>
            </div>
            <div class="modal-footer">
            	<div class="col text-left">
	            	<button type="button" class="btn btn-secondary" data-dismiss="modal">
	                	<i class="fa fa-chevron-left"></i>
	                    Atras
	                </button>
            	</div>
            </div>
        </div>
    </div>
</div>
<script>
	$(document).ready(function()
    {
      $('#shareModal_{{$id}}').on('shown.bs.modal', function () {
        $('#email').trigger('focus')
      })
    });
</script>