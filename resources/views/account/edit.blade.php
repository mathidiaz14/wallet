<!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editModal_{{$id}}">
    <i class="fa fa-edit"></i>
</button>

<!-- Modal -->
<div class="modal fade" id="editModal_{{$id}}" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editModalLabel">Editar cuenta</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-left">
                <form class="form-horizontal" action="{{route('account.update', $account->id)}}" method="post">
                    @csrf
                    <input type="hidden" name="_method" value="PATCH">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label>Titulo</label>
                            <input type="text" class="form-control" value="{{$account->title}}" name="title" required>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label>Monto</label>
                            <input type="number" class="form-control" value="{{$account->amount}}" name="amount">
                        </div>
                    </div>
                    <div class="row">
                         <div class="col">
                            <div class="form-group">
                                <label>Color</label>
                                <input type="color" class="form-control" value="{{$account->color}}" name="color">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="">¿Excluir del total?</label>
                                <div class="col">
                                    @include('helper.switch', ['name' => 'exclude', 'active' => $account->exclude])
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                <i class="fa fa-chevron-left"></i>
                                Atras
                            </button>
                        </div>
                        <div class="col text-right">
                            <button class="btn btn-primary">
                            <i class="fa fa-save"></i>
                                Guardar
                            </button>
                        </div>  
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>