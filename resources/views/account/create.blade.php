<!-- Button trigger modal -->
  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addModal">
    <i class="fa fa-plus"></i>
    Agregar cuenta
  </button>

  <!-- Modal -->
  <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true" >
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="addModalLabel">Agregar cuenta</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body text-left">
          <form class="form-horizontal" action="{{url('account')}}" method="post">
            @csrf
            <div class="col-sm-12">
                <div class="form-group">
                    <label>Titulo</label>
                    <input type="text" class="form-control" placeholder="Titulo..." name="title" id="title" required="">
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                    <label>Monto inicial</label>
                    <input type="number" class="form-control" value="0" name="amount">
                </div>
            </div>
            <div class="row">
              <div class="col">
                  <div class="form-group">
                      <label>Color</label>
                      <input type="color" class="form-control" value="" name="color">
                  </div>
              </div>
              <div class="col">
                  <div class="form-group">
                      <label for="">¿Excluir del total?</label>
                      <div class="col">
                          @include('helper.switch', ['name' => 'exclude', 'active' => null])
                      </div>
                  </div>
              </div>
            </div>
            <hr>
            <div class="row">
              <div class="col">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                  <i class="fa fa-chevron-left"></i>
                  Atras
                </button>
              </div>
              <div class="col text-right">
                <button class="btn btn-primary">
                  <i class="fa fa-save"></i>
                  Guardar
                </button>
              </div>  
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>