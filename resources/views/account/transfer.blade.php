<!-- Button trigger modal -->
<button type="button" class="btn btn-info" data-toggle="modal" data-target="#transferModal">
    <i class="fas fa-exchange-alt"></i>
    Transferir monto
</button>

<!-- Modal -->
<div class="modal fade" id="transferModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editModalLabel">Transferir monto entre cuentas</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-left">
            	<form class="form-horizontal" action="{{url('account/transfer')}}" method="post">
                    @csrf
                    <div class="form-group">
                    	<label for="">Cantidad</label>
                    	<div class="input-group mb-3">
							<div class="input-group-prepend">
								<span class="input-group-text" id="basic-addon1">
									<i class="fa fa-dollar-sign"></i>
								</span>
							</div>
							<input type="number" class="form-control" aria-describedby="basic-addon1" name="amount" id="amount" required>
						</div>
                    </div>
                    <div class="row">
                    	<div class="col-xs-12 col-lg-5">
		                    <div class="form-group">
		                    	<label for="">Desde : </label>
		                    	<select name="from" id="" class="form-control" required="">
		                    		@foreach(Auth::user()->accounts as $account)
										<option value="{{$account->id}}">
											{{$account->title}} || ${{$account->amount}}
										</option>
		                    		@endforeach
		                    	</select>
		                    </div>
	                    </div>
	                    <div class="col-xs-12 col-lg-2 text-center">
	                    	<i class="fas fa-exchange-alt fa-2x" ></i>
	                    </div>
	                    <div class="col-xs-12 col-lg-5">
		                    <div class="form-group">	
		                    	<label for="">Hasta : </label>
		                    	<select name="to" id="" class="form-control" required="">
		                    		@foreach(Auth::user()->accounts as $account)
										<option value="{{$account->id}}">
											{{$account->title}} || ${{$account->amount}}
										</option>
		                    		@endforeach
		                    	</select>
		                    </div>
		                </div>
                    </div>
                    <hr>
                    <div class="row">
                    	<div class="col text-left">
                    		<button type="button" class="btn btn-secondary" data-dismiss="modal">
			                	<i class="fa fa-chevron-left"></i>
			                    Atras
			                </button>
                    	</div>
                    	<div class="col text-right">
	                    	<button class="btn btn-primary">
	                        	<i class="fa fa-paper-plane"></i>
	                        	Enviar	
	                    	</button>
                    	</div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
	$(document).ready(function()
    {
		$('#transferModal').on('shown.bs.modal', function () {
			$('#amount').trigger('focus')
		});
    });
</script>