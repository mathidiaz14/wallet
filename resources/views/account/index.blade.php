@extends('layouts.dashboard', ['menu' => 'account'])

@section('content')
<div class="row">
  <div class="col-sm">
    <div class="card shadow">
      <div class="card-header py-3">
        <div class="row">
          <div class="col">
            <h6 class="m-0 font-weight-bold text-primary">Cuentas</h6>
          </div>
          <div class="col text-right">
            @include('account.create')  
          </div>
        </div>
      </div>
      <div class="card-body">
        @include('helper.alert')
        <div class="table table-responsive">
          <table class="table table-striped">
            <thead>
                <tr class="text-center">
                  <th>Nombre</th>
                  <th>Monto</th>
                  <th>Moneda</th>
                  <th>Color</th>
                  <th>Rol</th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach(Auth::user()->accounts as $account)
                    <tr class="text-center">
                        <td>{{$account->title}}</td>
                        <td>{{$account->amount}}</td>
                        <td>{{$account->money}}</td>
                        <td>
                          <span style="background-color: {{$account->color}}; padding: 5px 30px; border-radius: 5px;">
                          </span>
                        </td>
                        <td>
                          @if($account->pivot->role == "admin")
                            Administrador
                          @else
                            Compartida
                          @endif
                        </td>
                        <td>
                          <a href="{{url('account', $account->id)}}" class="btn btn-success btn-fill">
                              <i class="fa fa-eye"></i>
                          </a>
                        </td>
                        <td>
                          @if($account->pivot->role == "admin")
                            @include('account.edit', ['id' => $account->id, 'account' => $account])
                          @else
                            <a class="btn btn-primary disabled">
                                <i class="fa fa-edit"></i>
                            </a>
                          @endif
                        </td>
                        <td>
                          @if($account->pivot->role == "admin")
                            @include('account.share',['id' => $account->id, 'account' => $account])
                          @else
                            <a class="btn btn-info disabled">
                                <i class="fa fa-share-alt"></i>
                            </a>
                          @endif
                        </td>
                        <td>
                          @if($account->pivot->role == "admin")
                            @include('helper.delete', ['id' => $account->id, 'route' => url('account', $account->id)])
                          @else
                            <form action="{{url('account/exit', $account->id)}}" method="POST">
                              @csrf
                              <button class="btn btn-danger">
                                <i class="fa fa-sign-out-alt"></i>
                              </button>
                            </form>
                          @endif
                            
                        </td>
                    </tr>
                @endforeach
            </tbody>
          </table>
        </div>
      </div>
      <div class="card-footer text-right">
        @include('account.transfer')
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
  <script>
    $(document).ready(function()
    {
      $('#addModal').on('shown.bs.modal', function () {
        $('#title').trigger('focus')
      })
    });
  </script>
@endsection