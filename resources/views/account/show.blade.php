@extends('layouts.dashboard', ['menu' => 'account'])

@section('content')
<div class="row">
  <div class="col-sm-12">
    <div class="card shadow">
      <div class="card-header py-3">
        <div class="row">
          <div class="col-6">
            <h6 class="m-0 font-weight-bold text-primary">Saldo ultimos 10 dias</h6>
          </div>
          <div class="col-6 text-right">
            <a href="{{url('account')}}" class="btn btn-secondary">
              <i class="fa fa-chevron-left"></i>
              Atras
            </a>
          </div>
        </div>
      </div>
      <div class="card-body">
        <canvas id="myAreaChart"></canvas>
      </div>
    </div>
  </div>
  <div class="col-sm-12">
    <br>
    <div class="card shadow">
      <div class="card-header py-3">
        <div class="row">
          <div class="col">
            <h6 class="m-0 font-weight-bold text-primary">Registros de la cuenta {{$account->title}}</h6>
          </div>
          <div class="col text-right">
            <h6 class="m-0 font-weight-bold text-primary">
              Monto: ${{$account->amount}}
            </h6>
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-12">
            <div class="table table-responsive">
              <table class="table table-striped">
                <tbody>
                  <tr class="text-center">
                    <th>Fecha</th>
                    <th>Monto</th>
                    <th>Categoria</th>
                    <th>Usuario</th>
                    <th>Cuenta</th>
                    <th></th>
                    <th></th>
                    <th></th>
                  </tr>
                  @foreach($registries as $registry)
                    <tr class="text-center">
                      <td>{{$registry->date->format('d/m/Y')}}</td>
                      <td>
                        @if($registry->type == "less")
                          <b style="color:red;">
                            -{{$registry->amount}}
                          </b>
                        @else
                          <b style="color:green;">
                            +{{$registry->amount}}
                          </b>
                        @endif
                      </td>
                      <td>
                        @include('registry.subcategory_show')
                      </td>
                      <td>{{$registry->user->name}}</td>
                      <td>
                        @include('registry.show', ['registry' => $registry])
                      </td>
                      <td>
                        @include('registry.edit', ['registry' => $registry])
                      </td>
                      <td>
                        @include('helper.delete', ['id' => $registry->id, 'route' => url('registry', $registry->id)])
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>  
            </div>
          </div>
          <div class="col-12">
            @include('helper.links', ['links' => $registries]);
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
  <script>
    // Set new default font family and font color to mimic Bootstrap's default styling
    Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
    Chart.defaults.global.defaultFontColor = '#858796';

    function number_format(number, decimals, dec_point, thousands_sep) {
      // *     example: number_format(1234.56, 2, ',', ' ');
      // *     return: '1 234,56'
      number = (number + '').replace(',', '').replace(' ', '');
      var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function(n, prec) {
          var k = Math.pow(10, prec);
          return '' + Math.round(n * k) / k;
        };
      // Fix for IE parseFloat(0.55).toFixed(0) = 0;
      s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
      if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
      }
      if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
      }
      return s.join(dec);
    }

    // Area Chart Example
    var ctx = document.getElementById("myAreaChart");
    var myLineChart = new Chart(ctx, {
      type: 'line',
      data: {

        labels: [
          @for ($i = 9; $i >= 0; $i-- )
            "{{Carbon\Carbon::now()->subDay($i)->format('d/m/Y')}}",
          @endfor
          ],
        datasets: [{
          label: "Monto",
          lineTension: 0.3,
          backgroundColor: "rgba(78, 115, 223, 0.05)",
          borderColor: "rgba(78, 115, 223, 1)",
          pointRadius: 3,
          pointBackgroundColor: "rgba(78, 115, 223, 1)",
          pointBorderColor: "rgba(78, 115, 223, 1)",
          pointHoverRadius: 3,
          pointHoverBackgroundColor: "rgba(78, 115, 223, 1)",
          pointHoverBorderColor: "rgba(78, 115, 223, 1)",
          pointHitRadius: 10,
          pointBorderWidth: 2,
          data: [
            @for ($i = 9; $i >= 0; $i-- )
              @php 
                $history = $account->histories->where('date', Carbon\Carbon::now()->subDay($i)->format("d-m-y"))->first();
              @endphp
              @if($history != null)
                {{$history->amount}},
              @else
                0,
              @endif
            @endfor
          ],
        }],
      },
      options: {
        maintainAspectRatio: false,
        layout: {
          padding: {
            left: 10,
            right: 25,
            top: 25,
            bottom: 0
          }
        },
        scales: {
          xAxes: [{
            time: {
              unit: 'date'
            },
            gridLines: {
              display: false,
              drawBorder: false
            },
            ticks: {
              maxTicksLimit: 7
            }
          }],
          yAxes: [{
            ticks: {
              maxTicksLimit: 5,
              padding: 10,
              // Include a dollar sign in the ticks
              callback: function(value, index, values) {
                return '$' + number_format(value);
              }
            },
            gridLines: {
              color: "rgb(234, 236, 244)",
              zeroLineColor: "rgb(234, 236, 244)",
              drawBorder: false,
              borderDash: [2],
              zeroLineBorderDash: [2]
            }
          }],
        },
        legend: {
          display: false
        },
        tooltips: {
          backgroundColor: "rgb(255,255,255)",
          bodyFontColor: "#858796",
          titleMarginBottom: 10,
          titleFontColor: '#6e707e',
          titleFontSize: 14,
          borderColor: '#dddfeb',
          borderWidth: 1,
          xPadding: 15,
          yPadding: 15,
          displayColors: false,
          intersect: false,
          mode: 'index',
          caretPadding: 10,
          callbacks: {
            label: function(tooltipItem, chart) {
              var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
              return datasetLabel + ': $' + number_format(tooltipItem.yLabel);
            }
          }
        }
      }
    });

    $("#account option[value='{{$account->id}}']").attr("selected", true);


  </script>
@endsection